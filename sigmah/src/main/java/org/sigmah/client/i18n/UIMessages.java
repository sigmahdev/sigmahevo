package org.sigmah.client.i18n;

/**
 * Interface to represent the messages contained in resource bundle:
 * 	C:/Users/mikael/Sigmah/sigmah/src/main/java/org/sigmah/client/i18n/UIMessages.properties'.
 */
public interface UIMessages extends com.google.gwt.i18n.client.Messages {
  
  /**
   * Translated "The following test projects are already created from this model: ''{0}'' . If you shift it from \"Draft\" to ''{1}'', you can not delete these projects anymore. Are you sure to continue ?".
   * 
   * @return translated "The following test projects are already created from this model: ''{0}'' . If you shift it from \"Draft\" to ''{1}'', you can not delete these projects anymore. Are you sure to continue ?"
   */
  @DefaultMessage("The following test projects are already created from this model: ''{0}'' . If you shift it from \"Draft\" to ''{1}'', you can not delete these projects anymore. Are you sure to continue ?")
  @Key("DraftProjectModelChangeStatusDetails")
  String DraftProjectModelChangeStatusDetails(String arg0,  String arg1);

  /**
   * Translated "Activities completed after {0}".
   * 
   * @return translated "Activities completed after {0}"
   */
  @DefaultMessage("Activities completed after {0}")
  @Key("activitiesCompletedAfter")
  String activitiesCompletedAfter(String arg0);

  /**
   * Translated "Activities completed before {0}".
   * 
   * @return translated "Activities completed before {0}"
   */
  @DefaultMessage("Activities completed before {0}")
  @Key("activitiesCompletedBefore")
  String activitiesCompletedBefore(String arg0);

  /**
   * Translated "Activities completed between {0} and {1}".
   * 
   * @return translated "Activities completed between {0} and {1}"
   */
  @DefaultMessage("Activities completed between {0} and {1}")
  @Key("activitiesCompletedBetween")
  String activitiesCompletedBetween(String arg0,  String arg1);

  /**
   * Translated "{0} at {1}".
   * 
   * @return translated "{0} at {1}"
   */
  @DefaultMessage("{0} at {1}")
  @Key("activityAt")
  String activityAt(String arg0,  String arg1);

  /**
   * Translated "{0} - {1}".
   * 
   * @return translated "{0} - {1}"
   */
  @DefaultMessage("{0} - {1}")
  @Key("activityTitle")
  String activityTitle(String arg0,  String arg1);

  /**
   * Translated "Confirm deletion of {0}".
   * 
   * @return translated "Confirm deletion of {0}"
   */
  @DefaultMessage("Confirm deletion of {0}")
  @Key("adminDeleteDraftModel")
  String adminDeleteDraftModel(String arg0);

  /**
   * Translated "You are not allowed to delete compulsory fields : ''{0}''  !".
   * 
   * @return translated "You are not allowed to delete compulsory fields : ''{0}''  !"
   */
  @DefaultMessage("You are not allowed to delete compulsory fields : ''{0}''  !")
  @Key("adminErrorDeleteDefaultFlexible")
  String adminErrorDeleteDefaultFlexible(String arg0);

  /**
   * Translated "Are you sure you want to delete these fields : ''{0}'' ?".
   * 
   * @return translated "Are you sure you want to delete these fields : ''{0}'' ?"
   */
  @DefaultMessage("Are you sure you want to delete these fields : ''{0}'' ?")
  @Key("adminFlexibleConfirmDelete")
  String adminFlexibleConfirmDelete(String arg0);

  /**
   * Translated "You did not select any field! Please select one row or more.".
   * 
   * @return translated "You did not select any field! Please select one row or more."
   */
  @DefaultMessage("You did not select any field! Please select one row or more.")
  @Key("adminFlexibleDeleteNone")
  String adminFlexibleDeleteNone();

  /**
   * Translated "Pattern to identify the info to import: \n Use the key word VAR as the cell/column content to define a pattern i.e. $$VAR$$).\nExamples: ''{0}''".
   * 
   * @return translated "Pattern to identify the info to import: \n Use the key word VAR as the cell/column content to define a pattern i.e. $$VAR$$).\nExamples: ''{0}''"
   */
  @DefaultMessage("Pattern to identify the info to import: \n Use the key word VAR as the cell/column content to define a pattern i.e. $$VAR$$).\nExamples: ''{0}''")
  @Key("adminImportSchemaPattern")
  String adminImportSchemaPattern(String arg0);

  /**
   * Translated "The following models ''{0}'' are still linked to the importation scheme.  The importation scheme has not been deleted.".
   * 
   * @return translated "The following models ''{0}'' are still linked to the importation scheme.  The importation scheme has not been deleted."
   */
  @DefaultMessage("The following models ''{0}'' are still linked to the importation scheme.  The importation scheme has not been deleted.")
  @Key("adminImportationSchemesWarnModelsLinked")
  String adminImportationSchemesWarnModelsLinked(String arg0);

  /**
   * Translated "A problem has occurred while updating log frame !".
   * 
   * @return translated "A problem has occurred while updating log frame !"
   */
  @DefaultMessage("A problem has occurred while updating log frame !")
  @Key("adminLogFrameUpdateFailure")
  String adminLogFrameUpdateFailure();

  /**
   * Translated "Log frame has been successfully updated.".
   * 
   * @return translated "Log frame has been successfully updated."
   */
  @DefaultMessage("Log frame has been successfully updated.")
  @Key("adminLogFrameUpdateSuccess")
  String adminLogFrameUpdateSuccess();

  /**
   * Translated "Do you really want to change Status from '\"Draft\"' to {0}? Once confirmed, you won''t be able to update your model anymore! Confirm?".
   * 
   * @return translated "Do you really want to change Status from '\"Draft\"' to {0}? Once confirmed, you won''t be able to update your model anymore! Confirm?"
   */
  @DefaultMessage("Do you really want to change Status from '\"Draft\"' to {0}? Once confirmed, you won''t be able to update your model anymore! Confirm?")
  @Key("adminModelDraftStatusChange")
  String adminModelDraftStatusChange(String arg0);

  /**
   * Translated "Model status warning".
   * 
   * @return translated "Model status warning"
   */
  @DefaultMessage("Model status warning")
  @Key("adminModelStatusChangeBox")
  String adminModelStatusChangeBox();

  /**
   * Translated "Do you really want to delete the organization unit {0} ?".
   * 
   * @return translated "Do you really want to delete the organization unit {0} ?"
   */
  @DefaultMessage("Do you really want to delete the organization unit {0} ?")
  @Key("adminOrgUnitRemoveConfirm")
  String adminOrgUnitRemoveConfirm(String arg0);

  /**
   * Translated "Are you sure you want to delete these privacy groups : ''{0}'' ?".
   * 
   * @return translated "Are you sure you want to delete these privacy groups : ''{0}'' ?"
   */
  @DefaultMessage("Are you sure you want to delete these privacy groups : ''{0}'' ?")
  @Key("adminPrivacyGroupsConfirmDelete")
  String adminPrivacyGroupsConfirmDelete(String arg0);

  /**
   * Translated "The following entities (fields : ''{0}'', profiles : ''{1}'') are still linked to the privacy group ''{2}''.  The privacy group has not been deleted.".
   * 
   * @return translated "The following entities (fields : ''{0}'', profiles : ''{1}'') are still linked to the privacy group ''{2}''.  The privacy group has not been deleted."
   */
  @DefaultMessage("The following entities (fields : ''{0}'', profiles : ''{1}'') are still linked to the privacy group ''{2}''.  The privacy group has not been deleted.")
  @Key("adminPrivacyGroupsWarnFieldsLinked")
  String adminPrivacyGroupsWarnFieldsLinked(String arg0,  String arg1,  String arg2);

  /**
   * Translated "You did not select any privacy group! Please select one row or more.".
   * 
   * @return translated "You did not select any privacy group! Please select one row or more."
   */
  @DefaultMessage("You did not select any privacy group! Please select one row or more.")
  @Key("adminPrivayGroupsDeleteNone")
  String adminPrivayGroupsDeleteNone();

  /**
   * Translated "Are you sure you want to delete these profiles : ''{0}'' ?".
   * 
   * @return translated "Are you sure you want to delete these profiles : ''{0}'' ?"
   */
  @DefaultMessage("Are you sure you want to delete these profiles : ''{0}'' ?")
  @Key("adminProfilesConfirmDelete")
  String adminProfilesConfirmDelete(String arg0);

  /**
   * Translated "You did not select any profile! Please select one row or more.".
   * 
   * @return translated "You did not select any profile! Please select one row or more."
   */
  @DefaultMessage("You did not select any profile! Please select one row or more.")
  @Key("adminProfilesDeleteNone")
  String adminProfilesDeleteNone();

  /**
   * Translated "The users {0} are still linked to the profile ''{1}''. The profile has not been deleted.".
   * 
   * @return translated "The users {0} are still linked to the profile ''{1}''. The profile has not been deleted."
   */
  @DefaultMessage("The users {0} are still linked to the profile ''{1}''. The profile has not been deleted.")
  @Key("adminProfilesWarnUsersLinked")
  String adminProfilesWarnUsersLinked(String arg0,  String arg1);

  /**
   * Translated "The name ''{0}'' already exists !".
   * 
   * @return translated "The name ''{0}'' already exists !"
   */
  @DefaultMessage("The name ''{0}'' already exists !")
  @Key("adminReportSecionInvalidName")
  String adminReportSecionInvalidName(String arg0);

  /**
   * Translated "Unable to save {0} !".
   * 
   * @return translated "Unable to save {0} !"
   */
  @DefaultMessage("Unable to save {0} !")
  @Key("adminStandardCreationFailure")
  String adminStandardCreationFailure(String arg0);

  /**
   * Translated "Unable to save {0} !".
   * 
   * @return translated "Unable to save {0} !"
   */
  @DefaultMessage("Unable to save {0} !")
  @Key("adminStandardCreationFailureF")
  String adminStandardCreationFailureF(String arg0);

  /**
   * Translated "A problem has occurred while saving {0} !".
   * 
   * @return translated "A problem has occurred while saving {0} !"
   */
  @DefaultMessage("A problem has occurred while saving {0} !")
  @Key("adminStandardCreationNull")
  String adminStandardCreationNull(String arg0);

  /**
   * Translated "A problem has occurred while saving {0} !".
   * 
   * @return translated "A problem has occurred while saving {0} !"
   */
  @DefaultMessage("A problem has occurred while saving {0} !")
  @Key("adminStandardCreationNullF")
  String adminStandardCreationNullF(String arg0);

  /**
   * Translated "New {0} has been successfully created.".
   * 
   * @return translated "New {0} has been successfully created."
   */
  @DefaultMessage("New {0} has been successfully created.")
  @Key("adminStandardCreationSuccess")
  String adminStandardCreationSuccess(String arg0);

  /**
   * Translated "New {0} has been successfully created.".
   * 
   * @return translated "New {0} has been successfully created."
   */
  @DefaultMessage("New {0} has been successfully created.")
  @Key("adminStandardCreationSuccessF")
  String adminStandardCreationSuccessF(String arg0);

  /**
   * Translated "fields group".
   * 
   * @return translated "fields group"
   */
  @DefaultMessage("fields group")
  @Key("adminStandardLayoutGroup")
  String adminStandardLayoutGroup();

  /**
   * Translated "phase".
   * 
   * @return translated "phase"
   */
  @DefaultMessage("phase")
  @Key("adminStandardPhase")
  String adminStandardPhase();

  /**
   * Translated "privacy group".
   * 
   * @return translated "privacy group"
   */
  @DefaultMessage("privacy group")
  @Key("adminStandardPrivacyGroup")
  String adminStandardPrivacyGroup();

  /**
   * Translated "profile".
   * 
   * @return translated "profile"
   */
  @DefaultMessage("profile")
  @Key("adminStandardProfile")
  String adminStandardProfile();

  /**
   * Translated "{0} has been successfully updated.".
   * 
   * @return translated "{0} has been successfully updated."
   */
  @DefaultMessage("{0} has been successfully updated.")
  @Key("adminStandardUpdateSuccess")
  String adminStandardUpdateSuccess(String arg0);

  /**
   * Translated "{0} has been successfully updated.".
   * 
   * @return translated "{0} has been successfully updated."
   */
  @DefaultMessage("{0} has been successfully updated.")
  @Key("adminStandardUpdateSuccessF")
  String adminStandardUpdateSuccessF(String arg0);

  /**
   * Translated "user".
   * 
   * @return translated "user"
   */
  @DefaultMessage("user")
  @Key("adminStandardUser")
  String adminStandardUser();

  /**
   * Translated "Unable to create user ''{0}'' !".
   * 
   * @return translated "Unable to create user ''{0}'' !"
   */
  @DefaultMessage("Unable to create user ''{0}'' !")
  @Key("adminUserCreationFailure")
  String adminUserCreationFailure(String arg0);

  /**
   * Translated "A problem has occurred while creating user ''{0}'' ! Email may be already used.".
   * 
   * @return translated "A problem has occurred while creating user ''{0}'' ! Email may be already used."
   */
  @DefaultMessage("A problem has occurred while creating user ''{0}'' ! Email may be already used.")
  @Key("adminUserCreationNull")
  String adminUserCreationNull(String arg0);

  /**
   * Translated "New user ''{0}'' has been successfully created.".
   * 
   * @return translated "New user ''{0}'' has been successfully created."
   */
  @DefaultMessage("New user ''{0}'' has been successfully created.")
  @Key("adminUserCreationSuccess")
  String adminUserCreationSuccess(String arg0);

  /**
   * Translated "User ''{0}'' has been successfully updated.".
   * 
   * @return translated "User ''{0}'' has been successfully updated."
   */
  @DefaultMessage("User ''{0}'' has been successfully updated.")
  @Key("adminUserUpdateSuccess")
  String adminUserUpdateSuccess(String arg0);

  /**
   * Translated "Amendment #{0}.{1}".
   * 
   * @return translated "Amendment #{0}.{1}"
   */
  @DefaultMessage("Amendment #{0}.{1}")
  @Key("amendmentName")
  String amendmentName(String arg0,  String arg1);

  /**
   * Translated "Problem summary:\n\n\nWhat did you expected to happen?\n\n\nWhat steps will reproduce the problem?\n1.\n2.\n3.\n\nSeverity of the problem (minor, medium, major)?\n\n\nTechnical informations\nVersion: {1}\nUser-agent: {0}\n".
   * 
   * @return translated "Problem summary:\n\n\nWhat did you expected to happen?\n\n\nWhat steps will reproduce the problem?\n1.\n2.\n3.\n\nSeverity of the problem (minor, medium, major)?\n\n\nTechnical informations\nVersion: {1}\nUser-agent: {0}\n"
   */
  @DefaultMessage("Problem summary:\n\n\nWhat did you expected to happen?\n\n\nWhat steps will reproduce the problem?\n1.\n2.\n3.\n\nSeverity of the problem (minor, medium, major)?\n\n\nTechnical informations\nVersion: {1}\nUser-agent: {0}\n")
  @Key("bugReportBody")
  String bugReportBody(String arg0,  String arg1);

  /**
   * Translated "[en] Bug report ({0}/{1}/{2})".
   * 
   * @return translated "[en] Bug report ({0}/{1}/{2})"
   */
  @DefaultMessage("[en] Bug report ({0}/{1}/{2})")
  @Key("bugReportMailObject")
  String bugReportMailObject(String arg0,  String arg1,  String arg2);

  /**
   * Translated "#Error: The Category  ''{0}''  is being used in project model  ''{1}''  by field  ''{2}''.".
   * 
   * @return translated "#Error: The Category  ''{0}''  is being used in project model  ''{1}''  by field  ''{2}''."
   */
  @DefaultMessage("#Error: The Category  ''{0}''  is being used in project model  ''{1}''  by field  ''{2}''.")
  @Key("categoryBeingUsed")
  String categoryBeingUsed(String arg0,  String arg1,  String arg2);

  /**
   * Translated "Are you sure you want to delete the activity at {0} ?".
   * 
   * @return translated "Are you sure you want to delete the activity at {0} ?"
   */
  @DefaultMessage("Are you sure you want to delete the activity at {0} ?")
  @Key("confirmDelete")
  String confirmDelete(String arg0);

  /**
   * Translated "Are you sure you want to delete the database <b>{0}</b>? <br><br>You will loose all activities and indicator results.".
   * 
   * @return translated "Are you sure you want to delete the database <b>{0}</b>? <br><br>You will loose all activities and indicator results."
   */
  @DefaultMessage("Are you sure you want to delete the database <b>{0}</b>? <br><br>You will loose all activities and indicator results.")
  @Key("confirmDeleteDb")
  String confirmDeleteDb(String arg0);

  /**
   * Translated "Are you sure you want to delete the following schemas : ''{0}''?".
   * 
   * @return translated "Are you sure you want to delete the following schemas : ''{0}''?"
   */
  @DefaultMessage("Are you sure you want to delete the following schemas : ''{0}''?")
  @Key("confirmDeleteSchemeModels")
  String confirmDeleteSchemeModels(String arg0);

  /**
   * Translated "Are you sure you want to delete the following schemas : ''{0}''?".
   * 
   * @return translated "Are you sure you want to delete the following schemas : ''{0}''?"
   */
  @DefaultMessage("Are you sure you want to delete the following schemas : ''{0}''?")
  @Key("confirmDeleteSchemes")
  String confirmDeleteSchemes(String arg0);

  /**
   * Translated "Are you sure you want to delete the matching rules for the following elements: ''{0}''?".
   * 
   * @return translated "Are you sure you want to delete the matching rules for the following elements: ''{0}''?"
   */
  @DefaultMessage("Are you sure you want to delete the matching rules for the following elements: ''{0}''?")
  @Key("confirmDeleteVariableFlexibleElements")
  String confirmDeleteVariableFlexibleElements(String arg0);

  /**
   * Translated "Are you sure you want to delete the following variables : ''{0}''?".
   * 
   * @return translated "Are you sure you want to delete the following variables : ''{0}''?"
   */
  @DefaultMessage("Are you sure you want to delete the following variables : ''{0}''?")
  @Key("confirmDeleteVariables")
  String confirmDeleteVariables(String arg0);

  /**
   * Translated "The coordinate falls outside of the bounds of {0}".
   * 
   * @return translated "The coordinate falls outside of the bounds of {0}"
   */
  @DefaultMessage("The coordinate falls outside of the bounds of {0}")
  @Key("coordOutsideBounds")
  String coordOutsideBounds(String arg0);

  /**
   * Translated "Copy Of {0}".
   * 
   * @return translated "Copy Of {0}"
   */
  @DefaultMessage("Copy Of {0}")
  @Key("copyOf")
  String copyOf(String arg0);

  /**
   * Translated "Please fill all the required fields to create {0}.".
   * 
   * @return translated "Please fill all the required fields to create {0}."
   */
  @DefaultMessage("Please fill all the required fields to create {0}.")
  @Key("createFormIncompleteDetails")
  String createFormIncompleteDetails(String arg0);

  /**
   * Translated "Please fill both organization unit and profiles to create user.".
   * 
   * @return translated "Please fill both organization unit and profiles to create user."
   */
  @DefaultMessage("Please fill both organization unit and profiles to create user.")
  @Key("createUserFormIncompleteDetails")
  String createUserFormIncompleteDetails();

  /**
   * Translated "Remove indicator from {0}".
   * 
   * @return translated "Remove indicator from {0}"
   */
  @DefaultMessage("Remove indicator from {0}")
  @Key("deleteIndicatorFrom")
  String deleteIndicatorFrom(String arg0);

  /**
   * Translated "An error happened while deleting ''{0}''. Please retry later, contact your administrator if the problem persists.".
   * 
   * @return translated "An error happened while deleting ''{0}''. Please retry later, contact your administrator if the problem persists."
   */
  @DefaultMessage("An error happened while deleting ''{0}''. Please retry later, contact your administrator if the problem persists.")
  @Key("entityDeleteEventError")
  String entityDeleteEventError(String arg0);

  /**
   * Translated "Export every {0} days".
   * 
   * @return translated "Export every {0} days"
   */
  @DefaultMessage("Export every {0} days")
  @Key("everyXDays")
  String everyXDays(String arg0);

  /**
   * Translated "Are you sure you want to delete the file ''{0}'' ?".
   * 
   * @return translated "Are you sure you want to delete the file ''{0}'' ?"
   */
  @DefaultMessage("Are you sure you want to delete the file ''{0}'' ?")
  @Key("flexibleElementFilesListConfirmDelete")
  String flexibleElementFilesListConfirmDelete(String arg0);

  /**
   * Translated "Are you sure you want to delete the version #{0} ?".
   * 
   * @return translated "Are you sure you want to delete the version #{0} ?"
   */
  @DefaultMessage("Are you sure you want to delete the version #{0} ?")
  @Key("flexibleElementFilesListConfirmVersionDelete")
  String flexibleElementFilesListConfirmVersionDelete(String arg0);

  /**
   * Translated "{0} file(s) max.".
   * 
   * @return translated "{0} file(s) max."
   */
  @DefaultMessage("{0} file(s) max.")
  @Key("flexibleElementFilesListLimitReached")
  String flexibleElementFilesListLimitReached(String arg0);

  /**
   * Translated "The file is too big ({0} MB, maximum allowed size: {1} MB).".
   * 
   * @return translated "The file is too big ({0} MB, maximum allowed size: {1} MB)."
   */
  @DefaultMessage("The file is too big ({0} MB, maximum allowed size: {1} MB).")
  @Key("flexibleElementFilesListUploadErrorTooBig")
  String flexibleElementFilesListUploadErrorTooBig(String arg0,  String arg1);

  /**
   * Translated "Are you sure you want to remove the indicator ''{0}'' from the list ?".
   * 
   * @return translated "Are you sure you want to remove the indicator ''{0}'' from the list ?"
   */
  @DefaultMessage("Are you sure you want to remove the indicator ''{0}'' from the list ?")
  @Key("flexibleElementIndicatorsListConfirmRemove")
  String flexibleElementIndicatorsListConfirmRemove(String arg0);

  /**
   * Translated "This element is linked to the category ''{0}''.".
   * 
   * @return translated "This element is linked to the category ''{0}''."
   */
  @DefaultMessage("This element is linked to the category ''{0}''.")
  @Key("flexibleElementQuestionCategory")
  String flexibleElementQuestionCategory(String arg0);

  /**
   * Translated "This element is linked to the quality criterion ''{0}''.".
   * 
   * @return translated "This element is linked to the quality criterion ''{0}''."
   */
  @DefaultMessage("This element is linked to the quality criterion ''{0}''.")
  @Key("flexibleElementQuestionQuality")
  String flexibleElementQuestionQuality(String arg0);

  /**
   * Translated "Enter or select a date between {0} and {1} (inclusive interval).".
   * 
   * @return translated "Enter or select a date between {0} and {1} (inclusive interval)."
   */
  @DefaultMessage("Enter or select a date between {0} and {1} (inclusive interval).")
  @Key("flexibleElementTextAreaDateRange")
  String flexibleElementTextAreaDateRange(String arg0,  String arg1);

  /**
   * Translated "Enter a number value ({0}) between {1} and {2} (inclusive interval).".
   * 
   * @return translated "Enter a number value ({0}) between {1} and {2} (inclusive interval)."
   */
  @DefaultMessage("Enter a number value ({0}) between {1} and {2} (inclusive interval).")
  @Key("flexibleElementTextAreaNumberRange")
  String flexibleElementTextAreaNumberRange(String arg0,  String arg1,  String arg2);

  /**
   * Translated "Enter your text (maximum {0} characters allowed).".
   * 
   * @return translated "Enter your text (maximum {0} characters allowed)."
   */
  @DefaultMessage("Enter your text (maximum {0} characters allowed).")
  @Key("flexibleElementTextAreaTextLength")
  String flexibleElementTextAreaTextLength(String arg0);

  /**
   * Translated "Confirm importation details for {0}".
   * 
   * @return translated "Confirm importation details for {0}"
   */
  @DefaultMessage("Confirm importation details for {0}")
  @Key("importConfirmationDetailsHeading")
  String importConfirmationDetailsHeading(String arg0);

  /**
   * Translated "Please fill all the required fields to import {0}.".
   * 
   * @return translated "Please fill all the required fields to import {0}."
   */
  @DefaultMessage("Please fill all the required fields to import {0}.")
  @Key("importFormIncompleteDetails")
  String importFormIncompleteDetails(String arg0);

  /**
   * Translated "The importation was successful for  ''{0}''".
   * 
   * @return translated "The importation was successful for  ''{0}''"
   */
  @DefaultMessage("The importation was successful for  ''{0}''")
  @Key("importSuccessful")
  String importSuccessful(String arg0);

  /**
   * Translated "Data entered directly into project \"{0}\"".
   * 
   * @return translated "Data entered directly into project \"{0}\""
   */
  @DefaultMessage("Data entered directly into project \"{0}\"")
  @Key("indicatorDatasourceDirect")
  String indicatorDatasourceDirect(String arg0);

  /**
   * Translated "The Email Address appears to be invalid. Please check the Email Address Again".
   * 
   * @return translated "The Email Address appears to be invalid. Please check the Email Address Again"
   */
  @DefaultMessage("The Email Address appears to be invalid. Please check the Email Address Again")
  @Key("invalidEmailAddress")
  String invalidEmailAddress();

  /**
   * Translated "Last Sync''d: {0}".
   * 
   * @return translated "Last Sync''d: {0}"
   */
  @DefaultMessage("Last Sync''d: {0}")
  @Key("lastSynced")
  String lastSynced(String arg0);

  /**
   * Translated "An email containing a secure link to change your password has been sent to the address ''{0}''. Please note that, the link will be expired within 24 hours.".
   * 
   * @return translated "An email containing a secure link to change your password has been sent to the address ''{0}''. Please note that, the link will be expired within 24 hours."
   */
  @DefaultMessage("An email containing a secure link to change your password has been sent to the address ''{0}''. Please note that, the link will be expired within 24 hours.")
  @Key("loginResetPasswordSuccessfull")
  String loginResetPasswordSuccessfull(String arg0);

  /**
   * Translated "The login ''{0}'' can not be found. Please try again.".
   * 
   * @return translated "The login ''{0}'' can not be found. Please try again."
   */
  @DefaultMessage("The login ''{0}'' can not be found. Please try again.")
  @Key("loginRetrievePasswordBadLogin")
  String loginRetrievePasswordBadLogin(String arg0);

  /**
   * Translated "Do you want to add a monitored point to the file ''{0}'' ?".
   * 
   * @return translated "Do you want to add a monitored point to the file ''{0}'' ?"
   */
  @DefaultMessage("Do you want to add a monitored point to the file ''{0}'' ?")
  @Key("monitoredPointAddWithFile")
  String monitoredPointAddWithFile(String arg0);

  /**
   * Translated "New {0}".
   * 
   * @return translated "New {0}"
   */
  @DefaultMessage("New {0}")
  @Key("newSite")
  String newSite(String arg0);

  /**
   * Translated "Older than {0} month(s)".
   * 
   * @return translated "Older than {0} month(s)"
   */
  @DefaultMessage("Older than {0} month(s)")
  @Key("olderThanXMonths")
  String olderThanXMonths(String arg0);

  /**
   * Translated "There is already data entered for the partner {0}. Before deleting this partner, you must delete the partner''s data.".
   * 
   * @return translated "There is already data entered for the partner {0}. Before deleting this partner, you must delete the partner''s data."
   */
  @DefaultMessage("There is already data entered for the partner {0}. Before deleting this partner, you must delete the partner''s data.")
  @Key("partnerHasDataWarning")
  String partnerHasDataWarning(String arg0);

  /**
   * Translated "Personal draft.".
   * 
   * @return translated "Personal draft."
   */
  @DefaultMessage("Personal draft.")
  @Key("personalDraft")
  String personalDraft();

  /**
   * Translated "Activate the phase ''{0}''".
   * 
   * @return translated "Activate the phase ''{0}''"
   */
  @DefaultMessage("Activate the phase ''{0}''")
  @Key("projectActivate")
  String projectActivate(String arg0);

  /**
   * Translated "Are you sure you want to close the phase ''{0}'' and to activate the phase ''{1}'' ? The closed phase cannot be changed later.".
   * 
   * @return translated "Are you sure you want to close the phase ''{0}'' and to activate the phase ''{1}'' ? The closed phase cannot be changed later."
   */
  @DefaultMessage("Are you sure you want to close the phase ''{0}'' and to activate the phase ''{1}'' ? The closed phase cannot be changed later.")
  @Key("projectCloseAndActivate")
  String projectCloseAndActivate(String arg0,  String arg1);

  /**
   * Translated "Are you sure you want to close the phase ''{0}'' and ends the project ?  Reminder: once a project is ended, all screens become read-only, including the Details sub-tab. ".
   * 
   * @return translated "Are you sure you want to close the phase ''{0}'' and ends the project ?  Reminder: once a project is ended, all screens become read-only, including the Details sub-tab. "
   */
  @DefaultMessage("Are you sure you want to close the phase ''{0}'' and ends the project ?  Reminder: once a project is ended, all screens become read-only, including the Details sub-tab. ")
  @Key("projectEnd")
  String projectEnd(String arg0);

  /**
   * Translated "Your project ''{0}'' finances this project in the amount of".
   * 
   * @return translated "Your project ''{0}'' finances this project in the amount of"
   */
  @DefaultMessage("Your project ''{0}'' finances this project in the amount of")
  @Key("projectFinancesDetails")
  String projectFinancesDetails(String arg0);

  /**
   * Translated "This project finances your project ''{0}'' in the amount of".
   * 
   * @return translated "This project finances your project ''{0}'' in the amount of"
   */
  @DefaultMessage("This project finances your project ''{0}'' in the amount of")
  @Key("projectFundedByDetails")
  String projectFundedByDetails(String arg0);

  /**
   * Translated "Values for the indicator: {0}".
   * 
   * @return translated "Values for the indicator: {0}"
   */
  @DefaultMessage("Values for the indicator: {0}")
  @Key("projectPivotByIndicator")
  String projectPivotByIndicator(String arg0);

  /**
   * Translated "Indicator values for the month of {0}".
   * 
   * @return translated "Indicator values for the month of {0}"
   */
  @DefaultMessage("Indicator values for the month of {0}")
  @Key("projectPivotByMonth")
  String projectPivotByMonth(String arg0);

  /**
   * Translated "Indicator values for the site: {0}".
   * 
   * @return translated "Indicator values for the site: {0}"
   */
  @DefaultMessage("Indicator values for the site: {0}")
  @Key("projectPivotBySite")
  String projectPivotBySite(String arg0);

  /**
   * Translated "Passwords don''t match.".
   * 
   * @return translated "Passwords don''t match."
   */
  @DefaultMessage("Passwords don''t match.")
  @Key("pwdMatchProblem")
  String pwdMatchProblem();

  /**
   * Translated "Private draft. Last saved the {0} at {1}.".
   * 
   * @return translated "Private draft. Last saved the {0} at {1}."
   */
  @DefaultMessage("Private draft. Last saved the {0} at {1}.")
  @Key("reportDraftHeader")
  String reportDraftHeader(String arg0,  String arg1);

  /**
   * Translated "Last saved the {0} at {1}.".
   * 
   * @return translated "Last saved the {0} at {1}."
   */
  @DefaultMessage("Last saved the {0} at {1}.")
  @Key("reportDraftLastChanged")
  String reportDraftLastChanged(String arg0,  String arg1);

  /**
   * Translated "Key-Question #{0}".
   * 
   * @return translated "Key-Question #{0}"
   */
  @DefaultMessage("Key-Question #{0}")
  @Key("reportKeyQuestionDialogTitle")
  String reportKeyQuestionDialogTitle(String arg0);

  /**
   * Translated "Key-Questions answered : {0}/{1}".
   * 
   * @return translated "Key-Questions answered : {0}/{1}"
   */
  @DefaultMessage("Key-Questions answered : {0}/{1}")
  @Key("reportKeyQuestions")
  String reportKeyQuestions(String arg0,  String arg1);

  /**
   * Translated "#Error: The report model ''{0}'' is being used by some project models !".
   * 
   * @return translated "#Error: The report model ''{0}'' is being used by some project models !"
   */
  @DefaultMessage("#Error: The report model ''{0}'' is being used by some project models !")
  @Key("reportModelAlreadyUsed")
  String reportModelAlreadyUsed(String arg0);

  /**
   * Translated "Open Report ''{0}''".
   * 
   * @return translated "Open Report ''{0}''"
   */
  @DefaultMessage("Open Report ''{0}''")
  @Key("reportOpenReport")
  String reportOpenReport(String arg0);

  /**
   * Translated "Do you really want to remove the report ''{0}''?".
   * 
   * @return translated "Do you really want to remove the report ''{0}''?"
   */
  @DefaultMessage("Do you really want to remove the report ''{0}''?")
  @Key("reportRemoveConfirm")
  String reportRemoveConfirm(String arg0);

  /**
   * Translated "Please select the ''{0}'' first.".
   * 
   * @return translated "Please select the ''{0}'' first."
   */
  @DefaultMessage("Please select the ''{0}'' first.")
  @Key("selectLevelFirst")
  String selectLevelFirst(String arg0);

  /**
   * Translated "The command ''{0}'' is unavailable offline.".
   * 
   * @return translated "The command ''{0}'' is unavailable offline."
   */
  @DefaultMessage("The command ''{0}'' is unavailable offline.")
  @Key("sigmahOfflineUnavailableCommand")
  String sigmahOfflineUnavailableCommand(String arg0);

  /**
   * Translated "{0} site(s) displayed, {1} site(s) missing coordinates".
   * 
   * @return translated "{0} site(s) displayed, {1} site(s) missing coordinates"
   */
  @DefaultMessage("{0} site(s) displayed, {1} site(s) missing coordinates")
  @Key("siteLoadStatus")
  String siteLoadStatus(String arg0,  String arg1);

  /**
   * Translated "{0} sites are missing geographic coordinates and will not appear on the map.".
   * 
   * @return translated "{0} sites are missing geographic coordinates and will not appear on the map."
   */
  @DefaultMessage("{0} sites are missing geographic coordinates and will not appear on the map.")
  @Key("sitesMissingCoordinates")
  String sitesMissingCoordinates(String arg0);

  /**
   * Translated "ActivityInfo r{0}".
   * 
   * @return translated "ActivityInfo r{0}"
   */
  @DefaultMessage("ActivityInfo r{0}")
  @Key("versionedActivityInfoTitle")
  String versionedActivityInfoTitle(String arg0);

  /**
   * Translated "The group called ''{0}'' in this container is set to the same vertical position you desire. One of the two groups will not appear when you will create a an object from this model. ".
   * 
   * @return translated "The group called ''{0}'' in this container is set to the same vertical position you desire. One of the two groups will not appear when you will create a an object from this model. "
   */
  @DefaultMessage("The group called ''{0}'' in this container is set to the same vertical position you desire. One of the two groups will not appear when you will create a an object from this model. ")
  @Key("verticalPositionConflict")
  String verticalPositionConflict(String arg0);
}
