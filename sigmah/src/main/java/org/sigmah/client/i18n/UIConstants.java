package org.sigmah.client.i18n;

/**
 * Interface to represent the constants contained in resource bundle:
 * 	'C:/Users/mikael/Sigmah/sigmah/src/main/java/org/sigmah/client/i18n/UIConstants.properties'.
 */
public interface UIConstants extends com.google.gwt.i18n.client.Constants {
  
  /**
   * Translated "ALL".
   * 
   * @return translated "ALL"
   */
  @DefaultStringValue("ALL")
  @Key("ALL")
  String ALL();

  /**
   * Translated "Banner".
   * 
   * @return translated "Banner"
   */
  @DefaultStringValue("Banner")
  @Key("Admin_BANNER")
  String Admin_BANNER();

  /**
   * Translated "Informations".
   * 
   * @return translated "Informations"
   */
  @DefaultStringValue("Informations")
  @Key("Admin_ORGUNIT_DETAILS")
  String Admin_ORGUNIT_DETAILS();

  /**
   * Translated "Phase model".
   * 
   * @return translated "Phase model"
   */
  @DefaultStringValue("Phase model")
  @Key("Admin_PHASE_MODEL")
  String Admin_PHASE_MODEL();

  /**
   * Translated "Details".
   * 
   * @return translated "Details"
   */
  @DefaultStringValue("Details")
  @Key("Admin_PROJECT_DETAILS")
  String Admin_PROJECT_DETAILS();

  /**
   * Translated "Change phase".
   * 
   * @return translated "Change phase"
   */
  @DefaultStringValue("Change phase")
  @Key("CHANGE_PHASE")
  String CHANGE_PHASE();

  /**
   * Translated "Create project".
   * 
   * @return translated "Create project"
   */
  @DefaultStringValue("Create project")
  @Key("CREATE_PROJECT")
  String CREATE_PROJECT();

  /**
   * Translated "Delete project".
   * 
   * @return translated "Delete project"
   */
  @DefaultStringValue("Delete project")
  @Key("DELETE_PROJECT")
  String DELETE_PROJECT();

  /**
   * Translated "Draft".
   * 
   * @return translated "Draft"
   */
  @DefaultStringValue("Draft")
  @Key("DRAFT")
  String DRAFT();

  /**
   * Translated "Edit agenda".
   * 
   * @return translated "Edit agenda"
   */
  @DefaultStringValue("Edit agenda")
  @Key("EDIT_AGENDA")
  String EDIT_AGENDA();

  /**
   * Translated "Edit all reminders".
   * 
   * @return translated "Edit all reminders"
   */
  @DefaultStringValue("Edit all reminders")
  @Key("EDIT_ALL_REMINDERS")
  String EDIT_ALL_REMINDERS();

  /**
   * Translated "Edit indicator".
   * 
   * @return translated "Edit indicator"
   */
  @DefaultStringValue("Edit indicator")
  @Key("EDIT_INDICATOR")
  String EDIT_INDICATOR();

  /**
   * Translated "Edit logframe".
   * 
   * @return translated "Edit logframe"
   */
  @DefaultStringValue("Edit logframe")
  @Key("EDIT_LOGFRAME")
  String EDIT_LOGFRAME();

  /**
   * Translated "Edit own reminders".
   * 
   * @return translated "Edit own reminders"
   */
  @DefaultStringValue("Edit own reminders")
  @Key("EDIT_OWN_REMINDERS")
  String EDIT_OWN_REMINDERS();

  /**
   * Translated "Edit project".
   * 
   * @return translated "Edit project"
   */
  @DefaultStringValue("Edit project")
  @Key("EDIT_PROJECT")
  String EDIT_PROJECT();

  /**
   * Translated "Manage indicator".
   * 
   * @return translated "Manage indicator"
   */
  @DefaultStringValue("Manage indicator")
  @Key("MANAGE_INDICATOR")
  String MANAGE_INDICATOR();

  /**
   * Translated "Manage organizational units".
   * 
   * @return translated "Manage organizational units"
   */
  @DefaultStringValue("Manage organizational units")
  @Key("MANAGE_UNIT")
  String MANAGE_UNIT();

  /**
   * Translated "Manage user".
   * 
   * @return translated "Manage user"
   */
  @DefaultStringValue("Manage user")
  @Key("MANAGE_USER")
  String MANAGE_USER();

  /**
   * Translated "Ready".
   * 
   * @return translated "Ready"
   */
  @DefaultStringValue("Ready")
  @Key("READY")
  String READY();

  /**
   * Translated "remove file".
   * 
   * @return translated "remove file"
   */
  @DefaultStringValue("remove file")
  @Key("REMOVE_FILE")
  String REMOVE_FILE();

  /**
   * Translated "Unavailable".
   * 
   * @return translated "Unavailable"
   */
  @DefaultStringValue("Unavailable")
  @Key("UNAVAILABLE")
  String UNAVAILABLE();

  /**
   * Translated "Available & used".
   * 
   * @return translated "Available & used"
   */
  @DefaultStringValue("Available & used")
  @Key("USED")
  String USED();

  /**
   * Translated "Validate amendment".
   * 
   * @return translated "Validate amendment"
   */
  @DefaultStringValue("Validate amendment")
  @Key("VALIDER_AMENDEMENT")
  String VALIDER_AMENDEMENT();

  /**
   * Translated "View ActivityInfo".
   * 
   * @return translated "View ActivityInfo"
   */
  @DefaultStringValue("View ActivityInfo")
  @Key("VIEW_ACTIVITYINFO")
  String VIEW_ACTIVITYINFO();

  /**
   * Translated "View admin".
   * 
   * @return translated "View admin"
   */
  @DefaultStringValue("View admin")
  @Key("VIEW_ADMIN")
  String VIEW_ADMIN();

  /**
   * Translated "View Agenda".
   * 
   * @return translated "View Agenda"
   */
  @DefaultStringValue("View Agenda")
  @Key("VIEW_AGENDA")
  String VIEW_AGENDA();

  /**
   * Translated "View indicator".
   * 
   * @return translated "View indicator"
   */
  @DefaultStringValue("View indicator")
  @Key("VIEW_INDICATOR")
  String VIEW_INDICATOR();

  /**
   * Translated "View logframe".
   * 
   * @return translated "View logframe"
   */
  @DefaultStringValue("View logframe")
  @Key("VIEW_LOGFRAME")
  String VIEW_LOGFRAME();

  /**
   * Translated "View project".
   * 
   * @return translated "View project"
   */
  @DefaultStringValue("View project")
  @Key("VIEW_PROJECT")
  String VIEW_PROJECT();

  /**
   * Translated "Acronym".
   * 
   * @return translated "Acronym"
   */
  @DefaultStringValue("Acronym")
  @Key("acronym")
  String acronym();

  /**
   * Translated "Activities".
   * 
   * @return translated "Activities"
   */
  @DefaultStringValue("Activities")
  @Key("activities")
  String activities();

  /**
   * Translated "Activity".
   * 
   * @return translated "Activity"
   */
  @DefaultStringValue("Activity")
  @Key("activity")
  String activity();

  /**
   * Translated "You can only enter indicator results for activities that are complete.".
   * 
   * @return translated "You can only enter indicator results for activities that are complete."
   */
  @DefaultStringValue("You can only enter indicator results for activities that are complete.")
  @Key("activityIncomplete")
  String activityIncomplete();

  /**
   * Translated "Add Activity".
   * 
   * @return translated "Add Activity"
   */
  @DefaultStringValue("Add Activity")
  @Key("addActivity")
  String addActivity();

  /**
   * Translated "Add Attribute".
   * 
   * @return translated "Add Attribute"
   */
  @DefaultStringValue("Add Attribute")
  @Key("addAttribute")
  String addAttribute();

  /**
   * Translated "Add Attribute Group".
   * 
   * @return translated "Add Attribute Group"
   */
  @DefaultStringValue("Add Attribute Group")
  @Key("addAttributeGroup")
  String addAttributeGroup();

  /**
   * Translated "Add Indicator".
   * 
   * @return translated "Add Indicator"
   */
  @DefaultStringValue("Add Indicator")
  @Key("addIndicator")
  String addIndicator();

  /**
   * Translated "Add".
   * 
   * @return translated "Add"
   */
  @DefaultStringValue("Add")
  @Key("addItem")
  String addItem();

  /**
   * Translated "Add Partner to Program".
   * 
   * @return translated "Add Partner to Program"
   */
  @DefaultStringValue("Add Partner to Program")
  @Key("addPartner")
  String addPartner();

  /**
   * Translated "Add User".
   * 
   * @return translated "Add User"
   */
  @DefaultStringValue("Add User")
  @Key("addUser")
  String addUser();

  /**
   * Translated "Add budget's sub-field".
   * 
   * @return translated "Add budget's sub-field"
   */
  @DefaultStringValue("Add budget's sub-field")
  @Key("adminAddBudgetSubField")
  String adminAddBudgetSubField();

  /**
   * Translated "Configure another scheme".
   * 
   * @return translated "Configure another scheme"
   */
  @DefaultStringValue("Configure another scheme")
  @Key("adminAddImportationSchemeModel")
  String adminAddImportationSchemeModel();

  /**
   * Translated "Set matching rule".
   * 
   * @return translated "Set matching rule"
   */
  @DefaultStringValue("Set matching rule")
  @Key("adminAddKeyVariableFlexibleElementHeading")
  String adminAddKeyVariableFlexibleElementHeading();

  /**
   * Translated "Add field importation rule".
   * 
   * @return translated "Add field importation rule"
   */
  @DefaultStringValue("Add field importation rule")
  @Key("adminAddVariableFlexibleElement")
  String adminAddVariableFlexibleElement();

  /**
   * Translated "Budget consumption ratio".
   * 
   * @return translated "Budget consumption ratio"
   */
  @DefaultStringValue("Budget consumption ratio")
  @Key("adminBudgetRatio")
  String adminBudgetRatio();

  /**
   * Translated "Budget sub-field".
   * 
   * @return translated "Budget sub-field"
   */
  @DefaultStringValue("Budget sub-field")
  @Key("adminBudgetSubField")
  String adminBudgetSubField();

  /**
   * Translated "Budget sub-field name".
   * 
   * @return translated "Budget sub-field name"
   */
  @DefaultStringValue("Budget sub-field name")
  @Key("adminBudgetSubFieldName")
  String adminBudgetSubFieldName();

  /**
   * Translated "Categories".
   * 
   * @return translated "Categories"
   */
  @DefaultStringValue("Categories")
  @Key("adminCategories")
  String adminCategories();

  /**
   * Translated "Circle".
   * 
   * @return translated "Circle"
   */
  @DefaultStringValue("Circle")
  @Key("adminCategoryCircle")
  String adminCategoryCircle();

  /**
   * Translated "Cross".
   * 
   * @return translated "Cross"
   */
  @DefaultStringValue("Cross")
  @Key("adminCategoryCross")
  String adminCategoryCross();

  /**
   * Translated "Diamond".
   * 
   * @return translated "Diamond"
   */
  @DefaultStringValue("Diamond")
  @Key("adminCategoryDiamond")
  String adminCategoryDiamond();

  /**
   * Translated "Color".
   * 
   * @return translated "Color"
   */
  @DefaultStringValue("Color")
  @Key("adminCategoryElementColor")
  String adminCategoryElementColor();

  /**
   * Translated "Label".
   * 
   * @return translated "Label"
   */
  @DefaultStringValue("Label")
  @Key("adminCategoryElementLabel")
  String adminCategoryElementLabel();

  /**
   * Translated "Category model importing".
   * 
   * @return translated "Category model importing"
   */
  @DefaultStringValue("Category model importing")
  @Key("adminCategoryImport")
  String adminCategoryImport();

  /**
   * Translated "The category model has been imported.".
   * 
   * @return translated "The category model has been imported."
   */
  @DefaultStringValue("The category model has been imported.")
  @Key("adminCategoryImportDetailt")
  String adminCategoryImportDetailt();

  /**
   * Translated "Error during the category model import.".
   * 
   * @return translated "Error during the category model import."
   */
  @DefaultStringValue("Error during the category model import.")
  @Key("adminCategoryImportError")
  String adminCategoryImportError();

  /**
   * Translated "Square".
   * 
   * @return translated "Square"
   */
  @DefaultStringValue("Square")
  @Key("adminCategorySquare")
  String adminCategorySquare();

  /**
   * Translated "Star".
   * 
   * @return translated "Star"
   */
  @DefaultStringValue("Star")
  @Key("adminCategoryStar")
  String adminCategoryStar();

  /**
   * Translated "Triangle".
   * 
   * @return translated "Triangle"
   */
  @DefaultStringValue("Triangle")
  @Key("adminCategoryTriangle")
  String adminCategoryTriangle();

  /**
   * Translated "Category saving".
   * 
   * @return translated "Category saving"
   */
  @DefaultStringValue("Category saving")
  @Key("adminCategoryTypeCreationBox")
  String adminCategoryTypeCreationBox();

  /**
   * Translated "Icon".
   * 
   * @return translated "Icon"
   */
  @DefaultStringValue("Icon")
  @Key("adminCategoryTypeIcon")
  String adminCategoryTypeIcon();

  /**
   * Translated "Category name".
   * 
   * @return translated "Category name"
   */
  @DefaultStringValue("Category name")
  @Key("adminCategoryTypeName")
  String adminCategoryTypeName();

  /**
   * Translated "category".
   * 
   * @return translated "category"
   */
  @DefaultStringValue("category")
  @Key("adminCategoryTypeStandard")
  String adminCategoryTypeStandard();

  /**
   * Translated "Pbm while loading...".
   * 
   * @return translated "Pbm while loading..."
   */
  @DefaultStringValue("Pbm while loading...")
  @Key("adminChoiceProblem")
  String adminChoiceProblem();

  /**
   * Translated "Import projects and organisational units".
   * 
   * @return translated "Import projects and organisational units"
   */
  @DefaultStringValue("Import projects and organisational units")
  @Key("adminDashboardImportWindowHeading")
  String adminDashboardImportWindowHeading();

  /**
   * Translated "Click on each field to edit".
   * 
   * @return translated "Click on each field to edit"
   */
  @DefaultStringValue("Click on each field to edit")
  @Key("adminEditGrid")
  String adminEditGrid();

  /**
   * Translated "File".
   * 
   * @return translated "File"
   */
  @DefaultStringValue("File")
  @Key("adminFileImport")
  String adminFileImport();

  /**
   * Translated "Field".
   * 
   * @return translated "Field"
   */
  @DefaultStringValue("Field")
  @Key("adminFlexible")
  String adminFlexible();

  /**
   * Translated "Add layout group".
   * 
   * @return translated "Add layout group"
   */
  @DefaultStringValue("Add layout group")
  @Key("adminFlexibleAddGroup")
  String adminFlexibleAddGroup();

  /**
   * Translated "Amendable".
   * 
   * @return translated "Amendable"
   */
  @DefaultStringValue("Amendable")
  @Key("adminFlexibleAmendable")
  String adminFlexibleAmendable();

  /**
   * Translated "Banner position".
   * 
   * @return translated "Banner position"
   */
  @DefaultStringValue("Banner position")
  @Key("adminFlexibleBannerPosition")
  String adminFlexibleBannerPosition();

  /**
   * Translated "Compulsory ".
   * 
   * @return translated "Compulsory "
   */
  @DefaultStringValue("Compulsory ")
  @Key("adminFlexibleCompulsory")
  String adminFlexibleCompulsory();

  /**
   * Translated "Container".
   * 
   * @return translated "Container"
   */
  @DefaultStringValue("Container")
  @Key("adminFlexibleContainer")
  String adminFlexibleContainer();

  /**
   * Translated "Attach to container...".
   * 
   * @return translated "Attach to container..."
   */
  @DefaultStringValue("Attach to container...")
  @Key("adminFlexibleContainerChoice")
  String adminFlexibleContainerChoice();

  /**
   * Translated "Fields saving".
   * 
   * @return translated "Fields saving"
   */
  @DefaultStringValue("Fields saving")
  @Key("adminFlexibleCreationBox")
  String adminFlexibleCreationBox();

  /**
   * Translated "Decimal?".
   * 
   * @return translated "Decimal?"
   */
  @DefaultStringValue("Decimal?")
  @Key("adminFlexibleDecimal")
  String adminFlexibleDecimal();

  /**
   * Translated "Delete selected fields".
   * 
   * @return translated "Delete selected fields"
   */
  @DefaultStringValue("Delete selected fields")
  @Key("adminFlexibleDeleteFlexibleElements")
  String adminFlexibleDeleteFlexibleElements();

  /**
   * Translated "The selected fields have been already deleted.".
   * 
   * @return translated "The selected fields have been already deleted."
   */
  @DefaultStringValue("The selected fields have been already deleted.")
  @Key("adminFlexibleDeleteFlexibleElementsConfirm")
  String adminFlexibleDeleteFlexibleElementsConfirm();

  /**
   * Translated "Exportable".
   * 
   * @return translated "Exportable"
   */
  @DefaultStringValue("Exportable")
  @Key("adminFlexibleExportable")
  String adminFlexibleExportable();

  /**
   * Translated "Field Id".
   * 
   * @return translated "Field Id"
   */
  @DefaultStringValue("Field Id")
  @Key("adminFlexibleFieldId")
  String adminFlexibleFieldId();

  /**
   * Translated "Container Group".
   * 
   * @return translated "Container Group"
   */
  @DefaultStringValue("Container Group")
  @Key("adminFlexibleGroup")
  String adminFlexibleGroup();

  /**
   * Translated "Attach to container group...".
   * 
   * @return translated "Attach to container group..."
   */
  @DefaultStringValue("Attach to container group...")
  @Key("adminFlexibleGroupChoice")
  String adminFlexibleGroupChoice();

  /**
   * Translated "Horizontal position".
   * 
   * @return translated "Horizontal position"
   */
  @DefaultStringValue("Horizontal position")
  @Key("adminFlexibleGroupHPosition")
  String adminFlexibleGroupHPosition();

  /**
   * Translated "Group Id".
   * 
   * @return translated "Group Id"
   */
  @DefaultStringValue("Group Id")
  @Key("adminFlexibleGroupId")
  String adminFlexibleGroupId();

  /**
   * Translated "Vertical position".
   * 
   * @return translated "Vertical position"
   */
  @DefaultStringValue("Vertical position")
  @Key("adminFlexibleGroupVPosition")
  String adminFlexibleGroupVPosition();

  /**
   * Translated "Number of characters".
   * 
   * @return translated "Number of characters"
   */
  @DefaultStringValue("Number of characters")
  @Key("adminFlexibleLength")
  String adminFlexibleLength();

  /**
   * Translated "Linked category".
   * 
   * @return translated "Linked category"
   */
  @DefaultStringValue("Linked category")
  @Key("adminFlexibleLinkedCategory")
  String adminFlexibleLinkedCategory();

  /**
   * Translated "Linked to quality criterion".
   * 
   * @return translated "Linked to quality criterion"
   */
  @DefaultStringValue("Linked to quality criterion")
  @Key("adminFlexibleLinkedToQuality")
  String adminFlexibleLinkedToQuality();

  /**
   * Translated "Max limit".
   * 
   * @return translated "Max limit"
   */
  @DefaultStringValue("Max limit")
  @Key("adminFlexibleMaxLimit")
  String adminFlexibleMaxLimit();

  /**
   * Translated "Min limit".
   * 
   * @return translated "Min limit"
   */
  @DefaultStringValue("Min limit")
  @Key("adminFlexibleMinLimit")
  String adminFlexibleMinLimit();

  /**
   * Translated "Multiple choice?".
   * 
   * @return translated "Multiple choice?"
   */
  @DefaultStringValue("Multiple choice?")
  @Key("adminFlexibleMultipleQ")
  String adminFlexibleMultipleQ();

  /**
   * Translated "Name".
   * 
   * @return translated "Name"
   */
  @DefaultStringValue("Name")
  @Key("adminFlexibleName")
  String adminFlexibleName();

  /**
   * Translated "No linked category".
   * 
   * @return translated "No linked category"
   */
  @DefaultStringValue("No linked category")
  @Key("adminFlexibleNoLinkedCategory")
  String adminFlexibleNoLinkedCategory();

  /**
   * Translated "Order".
   * 
   * @return translated "Order"
   */
  @DefaultStringValue("Order")
  @Key("adminFlexibleOrder")
  String adminFlexibleOrder();

  /**
   * Translated "Display order...".
   * 
   * @return translated "Display order..."
   */
  @DefaultStringValue("Display order...")
  @Key("adminFlexibleOrderChoice")
  String adminFlexibleOrderChoice();

  /**
   * Translated "Order in Group".
   * 
   * @return translated "Order in Group"
   */
  @DefaultStringValue("Order in Group")
  @Key("adminFlexibleOrderInGroup")
  String adminFlexibleOrderInGroup();

  /**
   * Translated "Custom choices".
   * 
   * @return translated "Custom choices"
   */
  @DefaultStringValue("Custom choices")
  @Key("adminFlexibleQChoices")
  String adminFlexibleQChoices();

  /**
   * Translated "Type of text area".
   * 
   * @return translated "Type of text area"
   */
  @DefaultStringValue("Type of text area")
  @Key("adminFlexibleTextType")
  String adminFlexibleTextType();

  /**
   * Translated "Date".
   * 
   * @return translated "Date"
   */
  @DefaultStringValue("Date")
  @Key("adminFlexibleTextTypeD")
  String adminFlexibleTextTypeD();

  /**
   * Translated "Number".
   * 
   * @return translated "Number"
   */
  @DefaultStringValue("Number")
  @Key("adminFlexibleTextTypeN")
  String adminFlexibleTextTypeN();

  /**
   * Translated "Paragraph".
   * 
   * @return translated "Paragraph"
   */
  @DefaultStringValue("Paragraph")
  @Key("adminFlexibleTextTypeP")
  String adminFlexibleTextTypeP();

  /**
   * Translated "Text field".
   * 
   * @return translated "Text field"
   */
  @DefaultStringValue("Text field")
  @Key("adminFlexibleTextTypeT")
  String adminFlexibleTextTypeT();

  /**
   * Translated "Type".
   * 
   * @return translated "Type"
   */
  @DefaultStringValue("Type")
  @Key("adminFlexibleType")
  String adminFlexibleType();

  /**
   * Translated "Update group".
   * 
   * @return translated "Update group"
   */
  @DefaultStringValue("Update group")
  @Key("adminFlexibleUpdateGroup")
  String adminFlexibleUpdateGroup();

  /**
   * Translated "Importation schemes".
   * 
   * @return translated "Importation schemes"
   */
  @DefaultStringValue("Importation schemes")
  @Key("adminImport")
  String adminImport();

  /**
   * Translated "Only text fields (text and numbers) are allowed to be the identification key".
   * 
   * @return translated "Only text fields (text and numbers) are allowed to be the identification key"
   */
  @DefaultStringValue("Only text fields (text and numbers) are allowed to be the identification key")
  @Key("adminImportExplicationIdKey")
  String adminImportExplicationIdKey();

  /**
   * Translated "Key Identification".
   * 
   * @return translated "Key Identification"
   */
  @DefaultStringValue("Key Identification")
  @Key("adminImportKeyIdentification")
  String adminImportKeyIdentification();

  /**
   * Translated "Please choose a valid identification key".
   * 
   * @return translated "Please choose a valid identification key"
   */
  @DefaultStringValue("Please choose a valid identification key")
  @Key("adminImportKeyIdentificationMessage")
  String adminImportKeyIdentificationMessage();

  /**
   * Translated "New value".
   * 
   * @return translated "New value"
   */
  @DefaultStringValue("New value")
  @Key("adminImportNewValue")
  String adminImportNewValue();

  /**
   * Translated "Old value".
   * 
   * @return translated "Old value"
   */
  @DefaultStringValue("Old value")
  @Key("adminImportOldValue")
  String adminImportOldValue();

  /**
   * Translated "Cell".
   * 
   * @return translated "Cell"
   */
  @DefaultStringValue("Cell")
  @Key("adminImportReferenceCell")
  String adminImportReferenceCell();

  /**
   * Translated "Column".
   * 
   * @return translated "Column"
   */
  @DefaultStringValue("Column")
  @Key("adminImportReferenceColumn")
  String adminImportReferenceColumn();

  /**
   * Translated "Sheet:Cell".
   * 
   * @return translated "Sheet:Cell"
   */
  @DefaultStringValue("Sheet:Cell")
  @Key("adminImportReferenceSheetCell")
  String adminImportReferenceSheetCell();

  /**
   * Translated "Manually(Define each cell/column one by one)".
   * 
   * @return translated "Manually(Define each cell/column one by one)"
   */
  @DefaultStringValue("Manually(Define each cell/column one by one)")
  @Key("adminImportSchemeDefinitionManually")
  String adminImportSchemeDefinitionManually();

  /**
   * Translated "With Template(Define the cells/columns by uploading a template)".
   * 
   * @return translated "With Template(Define the cells/columns by uploading a template)"
   */
  @DefaultStringValue("With Template(Define the cells/columns by uploading a template)")
  @Key("adminImportSchemeDefinitionTemplate")
  String adminImportSchemeDefinitionTemplate();

  /**
   * Translated "Projects to import by file".
   * 
   * @return translated "Projects to import by file"
   */
  @DefaultStringValue("Projects to import by file")
  @Key("adminImportSchemeFileImportType")
  String adminImportSchemeFileImportType();

  /**
   * Translated "By line(one project by line)".
   * 
   * @return translated "By line(one project by line)"
   */
  @DefaultStringValue("By line(one project by line)")
  @Key("adminImportSchemeFileImportTypeLine")
  String adminImportSchemeFileImportTypeLine();

  /**
   * Translated "Several(one project for each sheet found in the file)".
   * 
   * @return translated "Several(one project for each sheet found in the file)"
   */
  @DefaultStringValue("Several(one project for each sheet found in the file)")
  @Key("adminImportSchemeFileImportTypeSeveral")
  String adminImportSchemeFileImportTypeSeveral();

  /**
   * Translated "Unique(one project defined on multiple sheets)".
   * 
   * @return translated "Unique(one project defined on multiple sheets)"
   */
  @DefaultStringValue("Unique(one project defined on multiple sheets)")
  @Key("adminImportSchemeFileImportTypeUnique")
  String adminImportSchemeFileImportTypeUnique();

  /**
   * Translated "Variable's Definition".
   * 
   * @return translated "Variable's Definition"
   */
  @DefaultStringValue("Variable's Definition")
  @Key("adminImportSchemeVariableDefinition")
  String adminImportSchemeVariableDefinition();

  /**
   * Translated "Extracted variable".
   * 
   * @return translated "Extracted variable"
   */
  @DefaultStringValue("Extracted variable")
  @Key("adminImportVariable")
  String adminImportVariable();

  /**
   * Translated "Importation scheme".
   * 
   * @return translated "Importation scheme"
   */
  @DefaultStringValue("Importation scheme")
  @Key("adminImportationScheme")
  String adminImportationScheme();

  /**
   * Translated "The importation schemes have been succesfully created".
   * 
   * @return translated "The importation schemes have been succesfully created"
   */
  @DefaultStringValue("The importation schemes have been succesfully created")
  @Key("adminImportationSchemeAddConfirm")
  String adminImportationSchemeAddConfirm();

  /**
   * Translated "First Row".
   * 
   * @return translated "First Row"
   */
  @DefaultStringValue("First Row")
  @Key("adminImportationSchemeFirstRow")
  String adminImportationSchemeFirstRow();

  /**
   * Translated "Extracted variable".
   * 
   * @return translated "Extracted variable"
   */
  @DefaultStringValue("Extracted variable")
  @Key("adminImportationSchemeModelVariableHeading")
  String adminImportationSchemeModelVariableHeading();

  /**
   * Translated "Sheet Name".
   * 
   * @return translated "Sheet Name"
   */
  @DefaultStringValue("Sheet Name")
  @Key("adminImportationSchemeSheetName")
  String adminImportationSchemeSheetName();

  /**
   * Translated "The importation scheme have been succesfully updated".
   * 
   * @return translated "The importation scheme have been succesfully updated"
   */
  @DefaultStringValue("The importation scheme have been succesfully updated")
  @Key("adminImportationSchemeUpdateConfirm")
  String adminImportationSchemeUpdateConfirm();

  /**
   * Translated "The importation schemes have been succesfully deleted".
   * 
   * @return translated "The importation schemes have been succesfully deleted"
   */
  @DefaultStringValue("The importation schemes have been succesfully deleted")
  @Key("adminImportationSchemesDeleteConfirm")
  String adminImportationSchemesDeleteConfirm();

  /**
   * Translated "You did not select any scheme! Please select one row or more.".
   * 
   * @return translated "You did not select any scheme! Please select one row or more."
   */
  @DefaultStringValue("You did not select any scheme! Please select one row or more.")
  @Key("adminImportationSchemesDeleteNone")
  String adminImportationSchemesDeleteNone();

  /**
   * Translated "Activities (A)".
   * 
   * @return translated "Activities (A)"
   */
  @DefaultStringValue("Activities (A)")
  @Key("adminLogFrameActivities")
  String adminLogFrameActivities();

  /**
   * Translated "Enable A groups ?".
   * 
   * @return translated "Enable A groups ?"
   */
  @DefaultStringValue("Enable A groups ?")
  @Key("adminLogFrameEnableAGroups")
  String adminLogFrameEnableAGroups();

  /**
   * Translated "Enable SO groups ?".
   * 
   * @return translated "Enable SO groups ?"
   */
  @DefaultStringValue("Enable SO groups ?")
  @Key("adminLogFrameEnableOSGroups")
  String adminLogFrameEnableOSGroups();

  /**
   * Translated "Enable P groups ?".
   * 
   * @return translated "Enable P groups ?"
   */
  @DefaultStringValue("Enable P groups ?")
  @Key("adminLogFrameEnablePGroups")
  String adminLogFrameEnablePGroups();

  /**
   * Translated "Enable ER groups?".
   * 
   * @return translated "Enable ER groups?"
   */
  @DefaultStringValue("Enable ER groups?")
  @Key("adminLogFrameEnableRAGroups")
  String adminLogFrameEnableRAGroups();

  /**
   * Translated "Max number of A".
   * 
   * @return translated "Max number of A"
   */
  @DefaultStringValue("Max number of A")
  @Key("adminLogFrameMaxA")
  String adminLogFrameMaxA();

  /**
   * Translated "Max number of A groups".
   * 
   * @return translated "Max number of A groups"
   */
  @DefaultStringValue("Max number of A groups")
  @Key("adminLogFrameMaxAGroups")
  String adminLogFrameMaxAGroups();

  /**
   * Translated "Max number of A per group".
   * 
   * @return translated "Max number of A per group"
   */
  @DefaultStringValue("Max number of A per group")
  @Key("adminLogFrameMaxAPerGroup")
  String adminLogFrameMaxAPerGroup();

  /**
   * Translated "Max number of A per ER".
   * 
   * @return translated "Max number of A per ER"
   */
  @DefaultStringValue("Max number of A per ER")
  @Key("adminLogFrameMaxAPerRA")
  String adminLogFrameMaxAPerRA();

  /**
   * Translated "Max number of SO".
   * 
   * @return translated "Max number of SO"
   */
  @DefaultStringValue("Max number of SO")
  @Key("adminLogFrameMaxOS")
  String adminLogFrameMaxOS();

  /**
   * Translated "Max number of SO groups".
   * 
   * @return translated "Max number of SO groups"
   */
  @DefaultStringValue("Max number of SO groups")
  @Key("adminLogFrameMaxOSGroups")
  String adminLogFrameMaxOSGroups();

  /**
   * Translated "Max number of SO per group".
   * 
   * @return translated "Max number of SO per group"
   */
  @DefaultStringValue("Max number of SO per group")
  @Key("adminLogFrameMaxOSPerGroup")
  String adminLogFrameMaxOSPerGroup();

  /**
   * Translated "Max number of P".
   * 
   * @return translated "Max number of P"
   */
  @DefaultStringValue("Max number of P")
  @Key("adminLogFrameMaxP")
  String adminLogFrameMaxP();

  /**
   * Translated "Max number of P groups".
   * 
   * @return translated "Max number of P groups"
   */
  @DefaultStringValue("Max number of P groups")
  @Key("adminLogFrameMaxPGroups")
  String adminLogFrameMaxPGroups();

  /**
   * Translated "Max number of P per group".
   * 
   * @return translated "Max number of P per group"
   */
  @DefaultStringValue("Max number of P per group")
  @Key("adminLogFrameMaxPPerGroup")
  String adminLogFrameMaxPPerGroup();

  /**
   * Translated "Max number of ER".
   * 
   * @return translated "Max number of ER"
   */
  @DefaultStringValue("Max number of ER")
  @Key("adminLogFrameMaxRA")
  String adminLogFrameMaxRA();

  /**
   * Translated "Max number of ER groups".
   * 
   * @return translated "Max number of ER groups"
   */
  @DefaultStringValue("Max number of ER groups")
  @Key("adminLogFrameMaxRAGroups")
  String adminLogFrameMaxRAGroups();

  /**
   * Translated "Max number of ER per group".
   * 
   * @return translated "Max number of ER per group"
   */
  @DefaultStringValue("Max number of ER per group")
  @Key("adminLogFrameMaxRAPerGroup")
  String adminLogFrameMaxRAPerGroup();

  /**
   * Translated "Max number of ER per SO".
   * 
   * @return translated "Max number of ER per SO"
   */
  @DefaultStringValue("Max number of ER per SO")
  @Key("adminLogFrameMaxRAPerOS")
  String adminLogFrameMaxRAPerOS();

  /**
   * Translated "Log frame name".
   * 
   * @return translated "Log frame name"
   */
  @DefaultStringValue("Log frame name")
  @Key("adminLogFrameName")
  String adminLogFrameName();

  /**
   * Translated "Specific objectives (SO)".
   * 
   * @return translated "Specific objectives (SO)"
   */
  @DefaultStringValue("Specific objectives (SO)")
  @Key("adminLogFrameObjectives")
  String adminLogFrameObjectives();

  /**
   * Translated "Prerequisites (P)".
   * 
   * @return translated "Prerequisites (P)"
   */
  @DefaultStringValue("Prerequisites (P)")
  @Key("adminLogFramePrerequisites")
  String adminLogFramePrerequisites();

  /**
   * Translated "Expected results (ER)".
   * 
   * @return translated "Expected results (ER)"
   */
  @DefaultStringValue("Expected results (ER)")
  @Key("adminLogFrameResults")
  String adminLogFrameResults();

  /**
   * Translated "Unlimited".
   * 
   * @return translated "Unlimited"
   */
  @DefaultStringValue("Unlimited")
  @Key("adminLogFrameUnlimited")
  String adminLogFrameUnlimited();

  /**
   * Translated "Log frame update".
   * 
   * @return translated "Log frame update"
   */
  @DefaultStringValue("Log frame update")
  @Key("adminLogFrameUpdate")
  String adminLogFrameUpdate();

  /**
   * Translated "Global management".
   * 
   * @return translated "Global management"
   */
  @DefaultStringValue("Global management")
  @Key("adminManagementTitle")
  String adminManagementTitle();

  /**
   * Translated "Maximum Attempts reached".
   * 
   * @return translated "Maximum Attempts reached"
   */
  @DefaultStringValue("Maximum Attempts reached")
  @Key("adminMaxAttempts")
  String adminMaxAttempts();

  /**
   * Translated "Maximum attempts to modify profile's privacy groups have been reached. Please close form and retry.".
   * 
   * @return translated "Maximum attempts to modify profile's privacy groups have been reached. Please close form and retry."
   */
  @DefaultStringValue("Maximum attempts to modify profile's privacy groups have been reached. Please close form and retry.")
  @Key("adminMaxAttemptsProfiles")
  String adminMaxAttemptsProfiles();

  /**
   * Translated "Maximum attempts to modify question's choices have been reached. Please close form and retry.".
   * 
   * @return translated "Maximum attempts to modify question's choices have been reached. Please close form and retry."
   */
  @DefaultStringValue("Maximum attempts to modify question's choices have been reached. Please close form and retry.")
  @Key("adminMaxAttemptsQChoices")
  String adminMaxAttemptsQChoices();

  /**
   * Translated "Maximum attempts to modify user's profiles have been reached. Please close form and retry.".
   * 
   * @return translated "Maximum attempts to modify user's profiles have been reached. Please close form and retry."
   */
  @DefaultStringValue("Maximum attempts to modify user's profiles have been reached. Please close form and retry.")
  @Key("adminMaxAttemptsUsers")
  String adminMaxAttemptsUsers();

  /**
   * Translated "Model Check".
   * 
   * @return translated "Model Check"
   */
  @DefaultStringValue("Model Check")
  @Key("adminModelCheckError")
  String adminModelCheckError();

  /**
   * Translated "An error happened while checking the model by the server.".
   * 
   * @return translated "An error happened while checking the model by the server."
   */
  @DefaultStringValue("An error happened while checking the model by the server.")
  @Key("adminModelCheckErrorDetails")
  String adminModelCheckErrorDetails();

  /**
   * Translated "Duplicate".
   * 
   * @return translated "Duplicate"
   */
  @DefaultStringValue("Duplicate")
  @Key("adminModelCopy")
  String adminModelCopy();

  /**
   * Translated "Importation schemes".
   * 
   * @return translated "Importation schemes"
   */
  @DefaultStringValue("Importation schemes")
  @Key("adminModelImportationSchemes")
  String adminModelImportationSchemes();

  /**
   * Translated "Organizational units".
   * 
   * @return translated "Organizational units"
   */
  @DefaultStringValue("Organizational units")
  @Key("adminOrgUnit")
  String adminOrgUnit();

  /**
   * Translated "Create a new organizational unit".
   * 
   * @return translated "Create a new organizational unit"
   */
  @DefaultStringValue("Create a new organizational unit")
  @Key("adminOrgUnitAdd")
  String adminOrgUnitAdd();

  /**
   * Translated "Creation of the unit failed".
   * 
   * @return translated "Creation of the unit failed"
   */
  @DefaultStringValue("Creation of the unit failed")
  @Key("adminOrgUnitAddFailed")
  String adminOrgUnitAddFailed();

  /**
   * Translated "The creation of the new organizational unit has failed.".
   * 
   * @return translated "The creation of the new organizational unit has failed."
   */
  @DefaultStringValue("The creation of the new organizational unit has failed.")
  @Key("adminOrgUnitAddFailedDetails")
  String adminOrgUnitAddFailedDetails();

  /**
   * Translated "There is no organizational unit model available.".
   * 
   * @return translated "There is no organizational unit model available."
   */
  @DefaultStringValue("There is no organizational unit model available.")
  @Key("adminOrgUnitAddMissingModel")
  String adminOrgUnitAddMissingModel();

  /**
   * Translated "Select the parent organizational unit.".
   * 
   * @return translated "Select the parent organizational unit."
   */
  @DefaultStringValue("Select the parent organizational unit.")
  @Key("adminOrgUnitAddMissingSelection")
  String adminOrgUnitAddMissingSelection();

  /**
   * Translated "Organizational unit created.".
   * 
   * @return translated "Organizational unit created."
   */
  @DefaultStringValue("Organizational unit created.")
  @Key("adminOrgUnitAddSucceed")
  String adminOrgUnitAddSucceed();

  /**
   * Translated "Unable to create a new organizational unit".
   * 
   * @return translated "Unable to create a new organizational unit"
   */
  @DefaultStringValue("Unable to create a new organizational unit")
  @Key("adminOrgUnitAddUnavailable")
  String adminOrgUnitAddUnavailable();

  /**
   * Translated "Code".
   * 
   * @return translated "Code"
   */
  @DefaultStringValue("Code")
  @Key("adminOrgUnitCode")
  String adminOrgUnitCode();

  /**
   * Translated "Country".
   * 
   * @return translated "Country"
   */
  @DefaultStringValue("Country")
  @Key("adminOrgUnitCountry")
  String adminOrgUnitCountry();

  /**
   * Translated "Create".
   * 
   * @return translated "Create"
   */
  @DefaultStringValue("Create")
  @Key("adminOrgUnitCreateButton")
  String adminOrgUnitCreateButton();

  /**
   * Translated "Model".
   * 
   * @return translated "Model"
   */
  @DefaultStringValue("Model")
  @Key("adminOrgUnitModel")
  String adminOrgUnitModel();

  /**
   * Translated "Deleting OrgUnit model".
   * 
   * @return translated "Deleting OrgUnit model"
   */
  @DefaultStringValue("Deleting OrgUnit model")
  @Key("adminOrgUnitModelDelete")
  String adminOrgUnitModelDelete();

  /**
   * Translated "The OrgUnit model has been deleted.".
   * 
   * @return translated "The OrgUnit model has been deleted."
   */
  @DefaultStringValue("The OrgUnit model has been deleted.")
  @Key("adminOrgUnitModelDeleteDetail")
  String adminOrgUnitModelDeleteDetail();

  /**
   * Translated "Select an organizational unit model...".
   * 
   * @return translated "Select an organizational unit model..."
   */
  @DefaultStringValue("Select an organizational unit model...")
  @Key("adminOrgUnitModelEmptyChoice")
  String adminOrgUnitModelEmptyChoice();

  /**
   * Translated "The status of the model of the root organisational unit cannot be modified.".
   * 
   * @return translated "The status of the model of the root organisational unit cannot be modified."
   */
  @DefaultStringValue("The status of the model of the root organisational unit cannot be modified.")
  @Key("adminOrgUnitModelOfRoot")
  String adminOrgUnitModelOfRoot();

  /**
   * Translated "Move".
   * 
   * @return translated "Move"
   */
  @DefaultStringValue("Move")
  @Key("adminOrgUnitMove")
  String adminOrgUnitMove();

  /**
   * Translated "You cannot move an organisational unit as a child of one of its children.".
   * 
   * @return translated "You cannot move an organisational unit as a child of one of its children."
   */
  @DefaultStringValue("You cannot move an organisational unit as a child of one of its children.")
  @Key("adminOrgUnitMoveErrorCycle")
  String adminOrgUnitMoveErrorCycle();

  /**
   * Translated "Cannot move an organisational unit as it own child.".
   * 
   * @return translated "Cannot move an organisational unit as it own child."
   */
  @DefaultStringValue("Cannot move an organisational unit as it own child.")
  @Key("adminOrgUnitMoveErrorItself")
  String adminOrgUnitMoveErrorItself();

  /**
   * Translated "You cannot move the root organisational unit.".
   * 
   * @return translated "You cannot move the root organisational unit."
   */
  @DefaultStringValue("You cannot move the root organisational unit.")
  @Key("adminOrgUnitMoveErrorRoot")
  String adminOrgUnitMoveErrorRoot();

  /**
   * Translated "Moving of the unit failed".
   * 
   * @return translated "Moving of the unit failed"
   */
  @DefaultStringValue("Moving of the unit failed")
  @Key("adminOrgUnitMoveFailed")
  String adminOrgUnitMoveFailed();

  /**
   * Translated "The moving of this organizational unit has failed.".
   * 
   * @return translated "The moving of this organizational unit has failed."
   */
  @DefaultStringValue("The moving of this organizational unit has failed.")
  @Key("adminOrgUnitMoveFailedDetails")
  String adminOrgUnitMoveFailedDetails();

  /**
   * Translated "There is no organizational unit available.".
   * 
   * @return translated "There is no organizational unit available."
   */
  @DefaultStringValue("There is no organizational unit available.")
  @Key("adminOrgUnitMoveMissingUnit")
  String adminOrgUnitMoveMissingUnit();

  /**
   * Translated "New parent organisational unit".
   * 
   * @return translated "New parent organisational unit"
   */
  @DefaultStringValue("New parent organisational unit")
  @Key("adminOrgUnitMoveNewParent")
  String adminOrgUnitMoveNewParent();

  /**
   * Translated "You are about to move the root organisational unit. The new parent organisational unit you will choose will become the new root organisational unit.".
   * 
   * @return translated "You are about to move the root organisational unit. The new parent organisational unit you will choose will become the new root organisational unit."
   */
  @DefaultStringValue("You are about to move the root organisational unit. The new parent organisational unit you will choose will become the new root organisational unit.")
  @Key("adminOrgUnitMoveRoot")
  String adminOrgUnitMoveRoot();

  /**
   * Translated "Organizational unit moved.".
   * 
   * @return translated "Organizational unit moved."
   */
  @DefaultStringValue("Organizational unit moved.")
  @Key("adminOrgUnitMoveSucceed")
  String adminOrgUnitMoveSucceed();

  /**
   * Translated "Unable to moved this organizational unit".
   * 
   * @return translated "Unable to moved this organizational unit"
   */
  @DefaultStringValue("Unable to moved this organizational unit")
  @Key("adminOrgUnitMoveUnavailable")
  String adminOrgUnitMoveUnavailable();

  /**
   * Translated "Delete an organizational unit".
   * 
   * @return translated "Delete an organizational unit"
   */
  @DefaultStringValue("Delete an organizational unit")
  @Key("adminOrgUnitRemove")
  String adminOrgUnitRemove();

  /**
   * Translated "This organizational unit has sub-units and cannot be deleted.".
   * 
   * @return translated "This organizational unit has sub-units and cannot be deleted."
   */
  @DefaultStringValue("This organizational unit has sub-units and cannot be deleted.")
  @Key("adminOrgUnitRemoveHasChildren")
  String adminOrgUnitRemoveHasChildren();

  /**
   * Translated "This organizational unit has projects or sub-units and cannot be deleted.".
   * 
   * @return translated "This organizational unit has projects or sub-units and cannot be deleted."
   */
  @DefaultStringValue("This organizational unit has projects or sub-units and cannot be deleted.")
  @Key("adminOrgUnitRemoveHasChildrenOrProjects")
  String adminOrgUnitRemoveHasChildrenOrProjects();

  /**
   * Translated "This organisational unit has projects and cannot be deleted.".
   * 
   * @return translated "This organisational unit has projects and cannot be deleted."
   */
  @DefaultStringValue("This organisational unit has projects and cannot be deleted.")
  @Key("adminOrgUnitRemoveHasProjects")
  String adminOrgUnitRemoveHasProjects();

  /**
   * Translated "Cannot delete the top organisational unit.".
   * 
   * @return translated "Cannot delete the top organisational unit."
   */
  @DefaultStringValue("Cannot delete the top organisational unit.")
  @Key("adminOrgUnitRemoveIsRoot")
  String adminOrgUnitRemoveIsRoot();

  /**
   * Translated "Organizational unit deleted.".
   * 
   * @return translated "Organizational unit deleted."
   */
  @DefaultStringValue("Organizational unit deleted.")
  @Key("adminOrgUnitRemoveSucceed")
  String adminOrgUnitRemoveSucceed();

  /**
   * Translated "Unable to delete this organizational unit".
   * 
   * @return translated "Unable to delete this organizational unit"
   */
  @DefaultStringValue("Unable to delete this organizational unit")
  @Key("adminOrgUnitRemoveUnavailable")
  String adminOrgUnitRemoveUnavailable();

  /**
   * Translated "Title".
   * 
   * @return translated "Title"
   */
  @DefaultStringValue("Title")
  @Key("adminOrgUnitTitle")
  String adminOrgUnitTitle();

  /**
   * Translated "Can contain projects?".
   * 
   * @return translated "Can contain projects?"
   */
  @DefaultStringValue("Can contain projects?")
  @Key("adminOrgUnitsModelContainProjects")
  String adminOrgUnitsModelContainProjects();

  /**
   * Translated "Organizational unit model duplication".
   * 
   * @return translated "Organizational unit model duplication"
   */
  @DefaultStringValue("Organizational unit model duplication")
  @Key("adminOrgUnitsModelCopy")
  String adminOrgUnitsModelCopy();

  /**
   * Translated "The organizational unit model has been duplicated. ".
   * 
   * @return translated "The organizational unit model has been duplicated. "
   */
  @DefaultStringValue("The organizational unit model has been duplicated. ")
  @Key("adminOrgUnitsModelCopyDetail")
  String adminOrgUnitsModelCopyDetail();

  /**
   * Translated "Error during the organizational unit model duplication. ".
   * 
   * @return translated "Error during the organizational unit model duplication. "
   */
  @DefaultStringValue("Error during the organizational unit model duplication. ")
  @Key("adminOrgUnitsModelCopyError")
  String adminOrgUnitsModelCopyError();

  /**
   * Translated "Organizational unit model creation".
   * 
   * @return translated "Organizational unit model creation"
   */
  @DefaultStringValue("Organizational unit model creation")
  @Key("adminOrgUnitsModelCreationBox")
  String adminOrgUnitsModelCreationBox();

  /**
   * Translated "Has budget?".
   * 
   * @return translated "Has budget?"
   */
  @DefaultStringValue("Has budget?")
  @Key("adminOrgUnitsModelHasBudget")
  String adminOrgUnitsModelHasBudget();

  /**
   * Translated "Has a site?".
   * 
   * @return translated "Has a site?"
   */
  @DefaultStringValue("Has a site?")
  @Key("adminOrgUnitsModelHasSite")
  String adminOrgUnitsModelHasSite();

  /**
   * Translated "Organizational unit model importing".
   * 
   * @return translated "Organizational unit model importing"
   */
  @DefaultStringValue("Organizational unit model importing")
  @Key("adminOrgUnitsModelImport")
  String adminOrgUnitsModelImport();

  /**
   * Translated "The organizational unit model has been imported.".
   * 
   * @return translated "The organizational unit model has been imported."
   */
  @DefaultStringValue("The organizational unit model has been imported.")
  @Key("adminOrgUnitsModelImportDetail")
  String adminOrgUnitsModelImportDetail();

  /**
   * Translated "Error during the organizational unit model.".
   * 
   * @return translated "Error during the organizational unit model."
   */
  @DefaultStringValue("Error during the organizational unit model.")
  @Key("adminOrgUnitsModelImportError")
  String adminOrgUnitsModelImportError();

  /**
   * Translated "Maximum level".
   * 
   * @return translated "Maximum level"
   */
  @DefaultStringValue("Maximum level")
  @Key("adminOrgUnitsModelMaxLevel")
  String adminOrgUnitsModelMaxLevel();

  /**
   * Translated "Minimum level".
   * 
   * @return translated "Minimum level"
   */
  @DefaultStringValue("Minimum level")
  @Key("adminOrgUnitsModelMinLevel")
  String adminOrgUnitsModelMinLevel();

  /**
   * Translated "Model name".
   * 
   * @return translated "Model name"
   */
  @DefaultStringValue("Model name")
  @Key("adminOrgUnitsModelName")
  String adminOrgUnitsModelName();

  /**
   * Translated "organizational unit model".
   * 
   * @return translated "organizational unit model"
   */
  @DefaultStringValue("organizational unit model")
  @Key("adminOrgUnitsModelStandard")
  String adminOrgUnitsModelStandard();

  /**
   * Translated "Organizational unit type".
   * 
   * @return translated "Organizational unit type"
   */
  @DefaultStringValue("Organizational unit type")
  @Key("adminOrgUnitsModelTitle")
  String adminOrgUnitsModelTitle();

  /**
   * Translated "Organizational unit model update".
   * 
   * @return translated "Organizational unit model update"
   */
  @DefaultStringValue("Organizational unit model update")
  @Key("adminOrgUnitsModelUpdateBox")
  String adminOrgUnitsModelUpdateBox();

  /**
   * Translated "Organizational unit models".
   * 
   * @return translated "Organizational unit models"
   */
  @DefaultStringValue("Organizational unit models")
  @Key("adminOrgUnitsModels")
  String adminOrgUnitsModels();

  /**
   * Translated "Phases saving".
   * 
   * @return translated "Phases saving"
   */
  @DefaultStringValue("Phases saving")
  @Key("adminPhaseCreationBox")
  String adminPhaseCreationBox();

  /**
   * Translated "The phase mode has been deleted.".
   * 
   * @return translated "The phase mode has been deleted."
   */
  @DefaultStringValue("The phase mode has been deleted.")
  @Key("adminPhaseModelDeleteDetail")
  String adminPhaseModelDeleteDetail();

  /**
   * Translated "Is root?".
   * 
   * @return translated "Is root?"
   */
  @DefaultStringValue("Is root?")
  @Key("adminPhaseModelRoot")
  String adminPhaseModelRoot();

  /**
   * Translated "Rows number".
   * 
   * @return translated "Rows number"
   */
  @DefaultStringValue("Rows number")
  @Key("adminPhaseModelSize")
  String adminPhaseModelSize();

  /**
   * Translated "Phase name".
   * 
   * @return translated "Phase name"
   */
  @DefaultStringValue("Phase name")
  @Key("adminPhaseName")
  String adminPhaseName();

  /**
   * Translated "Order".
   * 
   * @return translated "Order"
   */
  @DefaultStringValue("Order")
  @Key("adminPhaseOrder")
  String adminPhaseOrder();

  /**
   * Translated "Next phases".
   * 
   * @return translated "Next phases"
   */
  @DefaultStringValue("Next phases")
  @Key("adminPhaseSuccessors")
  String adminPhaseSuccessors();

  /**
   * Translated "Add group".
   * 
   * @return translated "Add group"
   */
  @DefaultStringValue("Add group")
  @Key("adminPrivacyGroupAdd")
  String adminPrivacyGroupAdd();

  /**
   * Translated "Select a privacy group...".
   * 
   * @return translated "Select a privacy group..."
   */
  @DefaultStringValue("Select a privacy group...")
  @Key("adminPrivacyGroupChoice")
  String adminPrivacyGroupChoice();

  /**
   * Translated "Privacy groups saving".
   * 
   * @return translated "Privacy groups saving"
   */
  @DefaultStringValue("Privacy groups saving")
  @Key("adminPrivacyGroupCreationBox")
  String adminPrivacyGroupCreationBox();

  /**
   * Translated "Delete group".
   * 
   * @return translated "Delete group"
   */
  @DefaultStringValue("Delete group")
  @Key("adminPrivacyGroupDelete")
  String adminPrivacyGroupDelete();

  /**
   * Translated "Privacy groups".
   * 
   * @return translated "Privacy groups"
   */
  @DefaultStringValue("Privacy groups")
  @Key("adminPrivacyGroups")
  String adminPrivacyGroups();

  /**
   * Translated "Code".
   * 
   * @return translated "Code"
   */
  @DefaultStringValue("Code")
  @Key("adminPrivacyGroupsCode")
  String adminPrivacyGroupsCode();

  /**
   * Translated "Privacy groups have been deleted.".
   * 
   * @return translated "Privacy groups have been deleted."
   */
  @DefaultStringValue("Privacy groups have been deleted.")
  @Key("adminPrivacyGroupsDeleteSuccess")
  String adminPrivacyGroupsDeleteSuccess();

  /**
   * Translated "Id".
   * 
   * @return translated "Id"
   */
  @DefaultStringValue("Id")
  @Key("adminPrivacyGroupsId")
  String adminPrivacyGroupsId();

  /**
   * Translated "Title".
   * 
   * @return translated "Title"
   */
  @DefaultStringValue("Title")
  @Key("adminPrivacyGroupsName")
  String adminPrivacyGroupsName();

  /**
   * Translated "Privacy groups".
   * 
   * @return translated "Privacy groups"
   */
  @DefaultStringValue("Privacy groups")
  @Key("adminPrivacyGroupsPanel")
  String adminPrivacyGroupsPanel();

  /**
   * Translated "A problem happened while loading data!".
   * 
   * @return translated "A problem happened while loading data!"
   */
  @DefaultStringValue("A problem happened while loading data!")
  @Key("adminProblemLoading")
  String adminProblemLoading();

  /**
   * Translated "Add profile".
   * 
   * @return translated "Add profile"
   */
  @DefaultStringValue("Add profile")
  @Key("adminProfileAdd")
  String adminProfileAdd();

  /**
   * Translated "Profiles saving".
   * 
   * @return translated "Profiles saving"
   */
  @DefaultStringValue("Profiles saving")
  @Key("adminProfileCreationBox")
  String adminProfileCreationBox();

  /**
   * Translated "Delete profile".
   * 
   * @return translated "Delete profile"
   */
  @DefaultStringValue("Delete profile")
  @Key("adminProfileDelete")
  String adminProfileDelete();

  /**
   * Translated "Save profiles".
   * 
   * @return translated "Save profiles"
   */
  @DefaultStringValue("Save profiles")
  @Key("adminProfileSave")
  String adminProfileSave();

  /**
   * Translated "Profiles administration".
   * 
   * @return translated "Profiles administration"
   */
  @DefaultStringValue("Profiles administration")
  @Key("adminProfiles")
  String adminProfiles();

  /**
   * Translated "Profiles have been deleted.".
   * 
   * @return translated "Profiles have been deleted."
   */
  @DefaultStringValue("Profiles have been deleted.")
  @Key("adminProfilesDeleteSuccess")
  String adminProfilesDeleteSuccess();

  /**
   * Translated "Global Permissions".
   * 
   * @return translated "Global Permissions"
   */
  @DefaultStringValue("Global Permissions")
  @Key("adminProfilesGlobalPermissions")
  String adminProfilesGlobalPermissions();

  /**
   * Translated "Id".
   * 
   * @return translated "Id"
   */
  @DefaultStringValue("Id")
  @Key("adminProfilesId")
  String adminProfilesId();

  /**
   * Translated "Name".
   * 
   * @return translated "Name"
   */
  @DefaultStringValue("Name")
  @Key("adminProfilesName")
  String adminProfilesName();

  /**
   * Translated "Profiles".
   * 
   * @return translated "Profiles"
   */
  @DefaultStringValue("Profiles")
  @Key("adminProfilesPanel")
  String adminProfilesPanel();

  /**
   * Translated "Privacy Groups".
   * 
   * @return translated "Privacy Groups"
   */
  @DefaultStringValue("Privacy Groups")
  @Key("adminProfilesPrivacyGroups")
  String adminProfilesPrivacyGroups();

  /**
   * Translated "New model".
   * 
   * @return translated "New model"
   */
  @DefaultStringValue("New model")
  @Key("adminProjectModelAdd")
  String adminProjectModelAdd();

  /**
   * Translated "Banner".
   * 
   * @return translated "Banner"
   */
  @DefaultStringValue("Banner")
  @Key("adminProjectModelBanner")
  String adminProjectModelBanner();

  /**
   * Translated "Project model duplication".
   * 
   * @return translated "Project model duplication"
   */
  @DefaultStringValue("Project model duplication")
  @Key("adminProjectModelCopy")
  String adminProjectModelCopy();

  /**
   * Translated "The project model has been duplicated. ".
   * 
   * @return translated "The project model has been duplicated. "
   */
  @DefaultStringValue("The project model has been duplicated. ")
  @Key("adminProjectModelCopyDetail")
  String adminProjectModelCopyDetail();

  /**
   * Translated "Error during the project model duplication. ".
   * 
   * @return translated "Error during the project model duplication. "
   */
  @DefaultStringValue("Error during the project model duplication. ")
  @Key("adminProjectModelCopyError")
  String adminProjectModelCopyError();

  /**
   * Translated "Project models saving".
   * 
   * @return translated "Project models saving"
   */
  @DefaultStringValue("Project models saving")
  @Key("adminProjectModelCreationBox")
  String adminProjectModelCreationBox();

  /**
   * Translated "Delete".
   * 
   * @return translated "Delete"
   */
  @DefaultStringValue("Delete")
  @Key("adminProjectModelDelete")
  String adminProjectModelDelete();

  /**
   * Translated "The project model has been deleted.".
   * 
   * @return translated "The project model has been deleted."
   */
  @DefaultStringValue("The project model has been deleted.")
  @Key("adminProjectModelDeleteDetail")
  String adminProjectModelDeleteDetail();

  /**
   * Translated "Details tab".
   * 
   * @return translated "Details tab"
   */
  @DefaultStringValue("Details tab")
  @Key("adminProjectModelDetails")
  String adminProjectModelDetails();

  /**
   * Translated "All fields".
   * 
   * @return translated "All fields"
   */
  @DefaultStringValue("All fields")
  @Key("adminProjectModelFields")
  String adminProjectModelFields();

  /**
   * Translated "Project model importing".
   * 
   * @return translated "Project model importing"
   */
  @DefaultStringValue("Project model importing")
  @Key("adminProjectModelImport")
  String adminProjectModelImport();

  /**
   * Translated "The project model has been imported.".
   * 
   * @return translated "The project model has been imported."
   */
  @DefaultStringValue("The project model has been imported.")
  @Key("adminProjectModelImportDetail")
  String adminProjectModelImportDetail();

  /**
   * Translated "Error during the project model importation.".
   * 
   * @return translated "Error during the project model importation."
   */
  @DefaultStringValue("Error during the project model importation.")
  @Key("adminProjectModelImportError")
  String adminProjectModelImportError();

  /**
   * Translated "Log frame".
   * 
   * @return translated "Log frame"
   */
  @DefaultStringValue("Log frame")
  @Key("adminProjectModelLogFrame")
  String adminProjectModelLogFrame();

  /**
   * Translated "Phases".
   * 
   * @return translated "Phases"
   */
  @DefaultStringValue("Phases")
  @Key("adminProjectModelPhases")
  String adminProjectModelPhases();

  /**
   * Translated "Reports".
   * 
   * @return translated "Reports"
   */
  @DefaultStringValue("Reports")
  @Key("adminProjectModelReports")
  String adminProjectModelReports();

  /**
   * Translated "project model".
   * 
   * @return translated "project model"
   */
  @DefaultStringValue("project model")
  @Key("adminProjectModelStandard")
  String adminProjectModelStandard();

  /**
   * Translated "Project model type:".
   * 
   * @return translated "Project model type:"
   */
  @DefaultStringValue("Project model type:")
  @Key("adminProjectModelType")
  String adminProjectModelType();

  /**
   * Translated "Project model update".
   * 
   * @return translated "Project model update"
   */
  @DefaultStringValue("Project model update")
  @Key("adminProjectModelUpdateBox")
  String adminProjectModelUpdateBox();

  /**
   * Translated "Project models".
   * 
   * @return translated "Project models"
   */
  @DefaultStringValue("Project models")
  @Key("adminProjectModels")
  String adminProjectModels();

  /**
   * Translated "Id".
   * 
   * @return translated "Id"
   */
  @DefaultStringValue("Id")
  @Key("adminProjectModelsId")
  String adminProjectModelsId();

  /**
   * Translated "Model name".
   * 
   * @return translated "Model name"
   */
  @DefaultStringValue("Model name")
  @Key("adminProjectModelsName")
  String adminProjectModelsName();

  /**
   * Translated "Project models".
   * 
   * @return translated "Project models"
   */
  @DefaultStringValue("Project models")
  @Key("adminProjectModelsPanel")
  String adminProjectModelsPanel();

  /**
   * Translated "Status".
   * 
   * @return translated "Status"
   */
  @DefaultStringValue("Status")
  @Key("adminProjectModelsStatus")
  String adminProjectModelsStatus();

  /**
   * Translated "Use".
   * 
   * @return translated "Use"
   */
  @DefaultStringValue("Use")
  @Key("adminProjectModelsUse")
  String adminProjectModelsUse();

  /**
   * Translated "You may need to refresh profiles section. Confirm?".
   * 
   * @return translated "You may need to refresh profiles section. Confirm?"
   */
  @DefaultStringValue("You may need to refresh profiles section. Confirm?")
  @Key("adminRefreshProfilesBox")
  String adminRefreshProfilesBox();

  /**
   * Translated "You may need to refresh users section. Confirm?".
   * 
   * @return translated "You may need to refresh users section. Confirm?"
   */
  @DefaultStringValue("You may need to refresh users section. Confirm?")
  @Key("adminRefreshUsersBox")
  String adminRefreshUsersBox();

  /**
   * Translated "Number of text areas".
   * 
   * @return translated "Number of text areas"
   */
  @DefaultStringValue("Number of text areas")
  @Key("adminReportAreas")
  String adminReportAreas();

  /**
   * Translated "Report model saving".
   * 
   * @return translated "Report model saving"
   */
  @DefaultStringValue("Report model saving")
  @Key("adminReportModelCreationBox")
  String adminReportModelCreationBox();

  /**
   * Translated "Report model importing".
   * 
   * @return translated "Report model importing"
   */
  @DefaultStringValue("Report model importing")
  @Key("adminReportModelImport")
  String adminReportModelImport();

  /**
   * Translated "The report model has been imported.".
   * 
   * @return translated "The report model has been imported."
   */
  @DefaultStringValue("The report model has been imported.")
  @Key("adminReportModelImportDetail")
  String adminReportModelImportDetail();

  /**
   * Translated "Error during report model import.".
   * 
   * @return translated "Error during report model import."
   */
  @DefaultStringValue("Error during report model import.")
  @Key("adminReportModelImportError")
  String adminReportModelImportError();

  /**
   * Translated "report model".
   * 
   * @return translated "report model"
   */
  @DefaultStringValue("report model")
  @Key("adminReportModelStandard")
  String adminReportModelStandard();

  /**
   * Translated "Report model name".
   * 
   * @return translated "Report model name"
   */
  @DefaultStringValue("Report model name")
  @Key("adminReportName")
  String adminReportName();

  /**
   * Translated "Order".
   * 
   * @return translated "Order"
   */
  @DefaultStringValue("Order")
  @Key("adminReportOrder")
  String adminReportOrder();

  /**
   * Translated "Parent section".
   * 
   * @return translated "Parent section"
   */
  @DefaultStringValue("Parent section")
  @Key("adminReportParent")
  String adminReportParent();

  /**
   * Translated "Section title".
   * 
   * @return translated "Section title"
   */
  @DefaultStringValue("Section title")
  @Key("adminReportSection")
  String adminReportSection();

  /**
   * Translated "Please input a name of the section.".
   * 
   * @return translated "Please input a name of the section."
   */
  @DefaultStringValue("Please input a name of the section.")
  @Key("adminReportSectionEmptyName")
  String adminReportSectionEmptyName();

  /**
   * Translated "Id".
   * 
   * @return translated "Id"
   */
  @DefaultStringValue("Id")
  @Key("adminReportSectionId")
  String adminReportSectionId();

  /**
   * Translated "Sort order".
   * 
   * @return translated "Sort order"
   */
  @DefaultStringValue("Sort order")
  @Key("adminReportSectionIndex")
  String adminReportSectionIndex();

  /**
   * Translated "Invalid input.".
   * 
   * @return translated "Invalid input."
   */
  @DefaultStringValue("Invalid input.")
  @Key("adminReportSectionInvalidInput")
  String adminReportSectionInvalidInput();

  /**
   * Translated "Section name".
   * 
   * @return translated "Section name"
   */
  @DefaultStringValue("Section name")
  @Key("adminReportSectionName")
  String adminReportSectionName();

  /**
   * Translated "Number of text areas".
   * 
   * @return translated "Number of text areas"
   */
  @DefaultStringValue("Number of text areas")
  @Key("adminReportSectionNbText")
  String adminReportSectionNbText();

  /**
   * Translated "Parent section id".
   * 
   * @return translated "Parent section id"
   */
  @DefaultStringValue("Parent section id")
  @Key("adminReportSectionParentSection")
  String adminReportSectionParentSection();

  /**
   * Translated "Model id".
   * 
   * @return translated "Model id"
   */
  @DefaultStringValue("Model id")
  @Key("adminReportSectionProjectModelId")
  String adminReportSectionProjectModelId();

  /**
   * Translated "Report model root".
   * 
   * @return translated "Report model root"
   */
  @DefaultStringValue("Report model root")
  @Key("adminReportSectionRoot")
  String adminReportSectionRoot();

  /**
   * Translated "field".
   * 
   * @return translated "field"
   */
  @DefaultStringValue("field")
  @Key("adminStandardFlexibleName")
  String adminStandardFlexibleName();

  /**
   * Translated "Invalid values! Please make sure the values you entered are sufficient or if similar values are already present.".
   * 
   * @return translated "Invalid values! Please make sure the values you entered are sufficient or if similar values are already present."
   */
  @DefaultStringValue("Invalid values! Please make sure the values you entered are sufficient or if similar values are already present.")
  @Key("adminStandardInvalidValues")
  String adminStandardInvalidValues();

  /**
   * Translated "log frame".
   * 
   * @return translated "log frame"
   */
  @DefaultStringValue("log frame")
  @Key("adminStandardLogFrame")
  String adminStandardLogFrame();

  /**
   * Translated "model".
   * 
   * @return translated "model"
   */
  @DefaultStringValue("model")
  @Key("adminStandardModel")
  String adminStandardModel();

  /**
   * Translated "Users saving".
   * 
   * @return translated "Users saving"
   */
  @DefaultStringValue("Users saving")
  @Key("adminUserCreationBox")
  String adminUserCreationBox();

  /**
   * Translated "Attach to organization unit...".
   * 
   * @return translated "Attach to organization unit..."
   */
  @DefaultStringValue("Attach to organization unit...")
  @Key("adminUserCreationOrgUnitChoice")
  String adminUserCreationOrgUnitChoice();

  /**
   * Translated "Select one or multiple profiles...".
   * 
   * @return translated "Select one or multiple profiles..."
   */
  @DefaultStringValue("Select one or multiple profiles...")
  @Key("adminUserCreationProfileChoice")
  String adminUserCreationProfileChoice();

  /**
   * Translated "Associated profiles".
   * 
   * @return translated "Associated profiles"
   */
  @DefaultStringValue("Associated profiles")
  @Key("adminUserCreationSelectedProfiles")
  String adminUserCreationSelectedProfiles();

  /**
   * Translated "Disable/enable".
   * 
   * @return translated "Disable/enable"
   */
  @DefaultStringValue("Disable/enable")
  @Key("adminUserDisable")
  String adminUserDisable();

  /**
   * Translated "Users".
   * 
   * @return translated "Users"
   */
  @DefaultStringValue("Users")
  @Key("adminUsers")
  String adminUsers();

  /**
   * Translated "Status".
   * 
   * @return translated "Status"
   */
  @DefaultStringValue("Status")
  @Key("adminUsersActive")
  String adminUsersActive();

  /**
   * Translated "Last password change".
   * 
   * @return translated "Last password change"
   */
  @DefaultStringValue("Last password change")
  @Key("adminUsersDatePasswordChange")
  String adminUsersDatePasswordChange();

  /**
   * Translated "Email".
   * 
   * @return translated "Email"
   */
  @DefaultStringValue("Email")
  @Key("adminUsersEmail")
  String adminUsersEmail();

  /**
   * Translated "First name".
   * 
   * @return translated "First name"
   */
  @DefaultStringValue("First name")
  @Key("adminUsersFirstName")
  String adminUsersFirstName();

  /**
   * Translated "Active".
   * 
   * @return translated "Active"
   */
  @DefaultStringValue("Active")
  @Key("adminUsersIsActive")
  String adminUsersIsActive();

  /**
   * Translated "Invitation language".
   * 
   * @return translated "Invitation language"
   */
  @DefaultStringValue("Invitation language")
  @Key("adminUsersLocale")
  String adminUsersLocale();

  /**
   * Translated "Name".
   * 
   * @return translated "Name"
   */
  @DefaultStringValue("Name")
  @Key("adminUsersName")
  String adminUsersName();

  /**
   * Translated "No linked profiles".
   * 
   * @return translated "No linked profiles"
   */
  @DefaultStringValue("No linked profiles")
  @Key("adminUsersNoProfiles")
  String adminUsersNoProfiles();

  /**
   * Translated "Blocked".
   * 
   * @return translated "Blocked"
   */
  @DefaultStringValue("Blocked")
  @Key("adminUsersNotActive")
  String adminUsersNotActive();

  /**
   * Translated "Attachment unit".
   * 
   * @return translated "Attachment unit"
   */
  @DefaultStringValue("Attachment unit")
  @Key("adminUsersOrgUnit")
  String adminUsersOrgUnit();

  /**
   * Translated "Users".
   * 
   * @return translated "Users"
   */
  @DefaultStringValue("Users")
  @Key("adminUsersPanel")
  String adminUsersPanel();

  /**
   * Translated "Password change key".
   * 
   * @return translated "Password change key"
   */
  @DefaultStringValue("Password change key")
  @Key("adminUsersPasswordChange")
  String adminUsersPasswordChange();

  /**
   * Translated "Profiles".
   * 
   * @return translated "Profiles"
   */
  @DefaultStringValue("Profiles")
  @Key("adminUsersProfiles")
  String adminUsersProfiles();

  /**
   * Translated "Search user by name :".
   * 
   * @return translated "Search user by name :"
   */
  @DefaultStringValue("Search user by name :")
  @Key("adminUsersSearchByName")
  String adminUsersSearchByName();

  /**
   * Translated "Selection mode :".
   * 
   * @return translated "Selection mode :"
   */
  @DefaultStringValue("Selection mode :")
  @Key("adminUsersSelectionMode")
  String adminUsersSelectionMode();

  /**
   * Translated "The variables have been succesfully created".
   * 
   * @return translated "The variables have been succesfully created"
   */
  @DefaultStringValue("The variables have been succesfully created")
  @Key("adminVariableAddConfirm")
  String adminVariableAddConfirm();

  /**
   * Translated "The variables have been succesfully deleted".
   * 
   * @return translated "The variables have been succesfully deleted"
   */
  @DefaultStringValue("The variables have been succesfully deleted")
  @Key("adminVariableDeleteConfirm")
  String adminVariableDeleteConfirm();

  /**
   * Translated "The variable has been succesfully updated".
   * 
   * @return translated "The variable has been succesfully updated"
   */
  @DefaultStringValue("The variable has been succesfully updated")
  @Key("adminVariableUpdateConfirm")
  String adminVariableUpdateConfirm();

  /**
   * Translated "You did not select any variable! Please select one row or more.".
   * 
   * @return translated "You did not select any variable! Please select one row or more."
   */
  @DefaultStringValue("You did not select any variable! Please select one row or more.")
  @Key("adminVariablesDeleteNone")
  String adminVariablesDeleteNone();

  /**
   * Translated "Administration".
   * 
   * @return translated "Administration"
   */
  @DefaultStringValue("Administration")
  @Key("adminboard")
  String adminboard();

  /**
   * Translated "Aggregation Method".
   * 
   * @return translated "Aggregation Method"
   */
  @DefaultStringValue("Aggregation Method")
  @Key("aggregationMethod")
  String aggregationMethod();

  /**
   * Translated "All".
   * 
   * @return translated "All"
   */
  @DefaultStringValue("All")
  @Key("all")
  String all();

  /**
   * Translated "include all closed projects".
   * 
   * @return translated "include all closed projects"
   */
  @DefaultStringValue("include all closed projects")
  @Key("allFilter")
  String allFilter();

  /**
   * Translated "Design".
   * 
   * @return translated "Design"
   */
  @DefaultStringValue("Design")
  @Key("allowDesign")
  String allowDesign();

  /**
   * Translated "Allow user to modify activities, indicators, and attributes".
   * 
   * @return translated "Allow user to modify activities, indicators, and attributes"
   */
  @DefaultStringValue("Allow user to modify activities, indicators, and attributes")
  @Key("allowDesignLong")
  String allowDesignLong();

  /**
   * Translated "Edit".
   * 
   * @return translated "Edit"
   */
  @DefaultStringValue("Edit")
  @Key("allowEdit")
  String allowEdit();

  /**
   * Translated "Edit All".
   * 
   * @return translated "Edit All"
   */
  @DefaultStringValue("Edit All")
  @Key("allowEditAll")
  String allowEditAll();

  /**
   * Translated "Allow user to edit activities of all partners".
   * 
   * @return translated "Allow user to edit activities of all partners"
   */
  @DefaultStringValue("Allow user to edit activities of all partners")
  @Key("allowEditAllLong")
  String allowEditAllLong();

  /**
   * Translated "Allow user to edit activities".
   * 
   * @return translated "Allow user to edit activities"
   */
  @DefaultStringValue("Allow user to edit activities")
  @Key("allowEditLong")
  String allowEditLong();

  /**
   * Translated "Manage all users".
   * 
   * @return translated "Manage all users"
   */
  @DefaultStringValue("Manage all users")
  @Key("allowManageAllUsers")
  String allowManageAllUsers();

  /**
   * Translated "Manage users".
   * 
   * @return translated "Manage users"
   */
  @DefaultStringValue("Manage users")
  @Key("allowManageUsers")
  String allowManageUsers();

  /**
   * Translated "Allow Multiple".
   * 
   * @return translated "Allow Multiple"
   */
  @DefaultStringValue("Allow Multiple")
  @Key("allowMultiple")
  String allowMultiple();

  /**
   * Translated "View".
   * 
   * @return translated "View"
   */
  @DefaultStringValue("View")
  @Key("allowView")
  String allowView();

  /**
   * Translated "View All".
   * 
   * @return translated "View All"
   */
  @DefaultStringValue("View All")
  @Key("allowViewAll")
  String allowViewAll();

  /**
   * Translated "Allow user to view activity lists of all partners".
   * 
   * @return translated "Allow user to view activity lists of all partners"
   */
  @DefaultStringValue("Allow user to view activity lists of all partners")
  @Key("allowViewAllLong")
  String allowViewAllLong();

  /**
   * Translated "Allow user to view activity list".
   * 
   * @return translated "Allow user to view activity list"
   */
  @DefaultStringValue("Allow user to view activity list")
  @Key("allowViewLong")
  String allowViewLong();

  /**
   * Translated "An error occured while trying to execute the selected action.".
   * 
   * @return translated "An error occured while trying to execute the selected action."
   */
  @DefaultStringValue("An error occured while trying to execute the selected action.")
  @Key("amendmentActionError")
  String amendmentActionError();

  /**
   * Translated "Amendments".
   * 
   * @return translated "Amendments"
   */
  @DefaultStringValue("Amendments")
  @Key("amendmentBoxTitle")
  String amendmentBoxTitle();

  /**
   * Translated "New amendment".
   * 
   * @return translated "New amendment"
   */
  @DefaultStringValue("New amendment")
  @Key("amendmentCreate")
  String amendmentCreate();

  /**
   * Translated "Display".
   * 
   * @return translated "Display"
   */
  @DefaultStringValue("Display")
  @Key("amendmentDisplay")
  String amendmentDisplay();

  /**
   * Translated "Lock this amendment".
   * 
   * @return translated "Lock this amendment"
   */
  @DefaultStringValue("Lock this amendment")
  @Key("amendmentLock")
  String amendmentLock();

  /**
   * Translated "Reject this amendment".
   * 
   * @return translated "Reject this amendment"
   */
  @DefaultStringValue("Reject this amendment")
  @Key("amendmentReject")
  String amendmentReject();

  /**
   * Translated "Unlock this amendment".
   * 
   * @return translated "Unlock this amendment"
   */
  @DefaultStringValue("Unlock this amendment")
  @Key("amendmentUnlock")
  String amendmentUnlock();

  /**
   * Translated "Validate this amendment".
   * 
   * @return translated "Validate this amendment"
   */
  @DefaultStringValue("Validate this amendment")
  @Key("amendmentValidate")
  String amendmentValidate();

  /**
   * Translated "Analysis".
   * 
   * @return translated "Analysis"
   */
  @DefaultStringValue("Analysis")
  @Key("analysis")
  String analysis();

  /**
   * Translated "ActivityInfo".
   * 
   * @return translated "ActivityInfo"
   */
  @DefaultStringValue("ActivityInfo")
  @Key("appTitle")
  String appTitle();

  /**
   * Translated "Assessment".
   * 
   * @return translated "Assessment"
   */
  @DefaultStringValue("Assessment")
  @Key("assessment")
  String assessment();

  /**
   * Translated "Yes/No".
   * 
   * @return translated "Yes/No"
   */
  @DefaultStringValue("Yes/No")
  @Key("attributeTypeBoolean")
  String attributeTypeBoolean();

  /**
   * Translated "Quantity".
   * 
   * @return translated "Quantity"
   */
  @DefaultStringValue("Quantity")
  @Key("attributeTypeQuantity")
  String attributeTypeQuantity();

  /**
   * Translated "Choose".
   * 
   * @return translated "Choose"
   */
  @DefaultStringValue("Choose")
  @Key("attributeTypeSelect")
  String attributeTypeSelect();

  /**
   * Translated "Text".
   * 
   * @return translated "Text"
   */
  @DefaultStringValue("Text")
  @Key("attributeTypeText")
  String attributeTypeText();

  /**
   * Translated "Attributes".
   * 
   * @return translated "Attributes"
   */
  @DefaultStringValue("Attributes")
  @Key("attributes")
  String attributes();

  /**
   * Translated "Attributes".
   * 
   * @return translated "Attributes"
   */
  @DefaultStringValue("Attributes")
  @Key("attributs")
  String attributs();

  /**
   * Translated "The link that you followed is intended to reset a password. Since you are already logged in with your active password, in order to change the password go to Users' list in Administration section.   ".
   * 
   * @return translated "The link that you followed is intended to reset a password. Since you are already logged in with your active password, in order to change the password go to Users' list in Administration section.   "
   */
  @DefaultStringValue("The link that you followed is intended to reset a password. Since you are already logged in with your active password, in order to change the password go to Users' list in Administration section.   ")
  @Key("authorizedUserPasswordResetInfo")
  String authorizedUserPasswordResetInfo();

  /**
   * Translated "Auto-cleanup schedule".
   * 
   * @return translated "Auto-cleanup schedule"
   */
  @DefaultStringValue("Auto-cleanup schedule")
  @Key("autoCleanupSchedule")
  String autoCleanupSchedule();

  /**
   * Translated "Auto-export schedule".
   * 
   * @return translated "Auto-export schedule"
   */
  @DefaultStringValue("Auto-export schedule")
  @Key("autoExportSchedule")
  String autoExportSchedule();

  /**
   * Translated "Automatic".
   * 
   * @return translated "Automatic"
   */
  @DefaultStringValue("Automatic")
  @Key("automatic")
  String automatic();

  /**
   * Translated "Average".
   * 
   * @return translated "Average"
   */
  @DefaultStringValue("Average")
  @Key("average")
  String average();

  /**
   * Translated "Axe".
   * 
   * @return translated "Axe"
   */
  @DefaultStringValue("Axe")
  @Key("axe")
  String axe();

  /**
   * Translated "Backed-up data".
   * 
   * @return translated "Backed-up data"
   */
  @DefaultStringValue("Backed-up data")
  @Key("backedUpData")
  String backedUpData();

  /**
   * Translated "Background map".
   * 
   * @return translated "Background map"
   */
  @DefaultStringValue("Background map")
  @Key("backgroundMap")
  String backgroundMap();

  /**
   * Translated "All versions of each file".
   * 
   * @return translated "All versions of each file"
   */
  @DefaultStringValue("All versions of each file")
  @Key("backupManagementAllVersion")
  String backupManagementAllVersion();

  /**
   * Translated "Backup All Files".
   * 
   * @return translated "Backup All Files"
   */
  @DefaultStringValue("Backup All Files")
  @Key("backupManagementBackupAllFiles")
  String backupManagementBackupAllFiles();

  /**
   * Translated "Download :".
   * 
   * @return translated "Download :"
   */
  @DefaultStringValue("Download :")
  @Key("backupManagementDownload")
  String backupManagementDownload();

  /**
   * Translated "Only the last version of each file".
   * 
   * @return translated "Only the last version of each file"
   */
  @DefaultStringValue("Only the last version of each file")
  @Key("backupManagementOneVersion")
  String backupManagementOneVersion();

  /**
   * Translated "Root organizational unit to build the backup from :".
   * 
   * @return translated "Root organizational unit to build the backup from :"
   */
  @DefaultStringValue("Root organizational unit to build the backup from :")
  @Key("backupManagementRootOrganization")
  String backupManagementRootOrganization();

  /**
   * Translated "Backup all files".
   * 
   * @return translated "Backup all files"
   */
  @DefaultStringValue("Backup all files")
  @Key("backupManagementTitle")
  String backupManagementTitle();

  /**
   * Translated "Yes/No".
   * 
   * @return translated "Yes/No"
   */
  @DefaultStringValue("Yes/No")
  @Key("booleanType")
  String booleanType();

  /**
   * Translated "Report a bug".
   * 
   * @return translated "Report a bug"
   */
  @DefaultStringValue("Report a bug")
  @Key("bugReport")
  String bugReport();

  /**
   * Translated "[en] Bug report".
   * 
   * @return translated "[en] Bug report"
   */
  @DefaultStringValue("[en] Bug report")
  @Key("bugReportMailObject")
  String bugReportMailObject();

  /**
   * Translated "support@sigmah.org".
   * 
   * @return translated "support@sigmah.org"
   */
  @DefaultStringValue("support@sigmah.org")
  @Key("bugReportSupportAddress")
  String bugReportSupportAddress();

  /**
   * Translated "Add event".
   * 
   * @return translated "Add event"
   */
  @DefaultStringValue("Add event")
  @Key("calendarAddEvent")
  String calendarAddEvent();

  /**
   * Translated "Cannot create the new events, some mandatory fields are empty.".
   * 
   * @return translated "Cannot create the new events, some mandatory fields are empty."
   */
  @DefaultStringValue("Cannot create the new events, some mandatory fields are empty.")
  @Key("calendarAddEventEmptyFields")
  String calendarAddEventEmptyFields();

  /**
   * Translated "Error while creating a new calendar event.".
   * 
   * @return translated "Error while creating a new calendar event."
   */
  @DefaultStringValue("Error while creating a new calendar event.")
  @Key("calendarAddEventError")
  String calendarAddEventError();

  /**
   * Translated "Events".
   * 
   * @return translated "Events"
   */
  @DefaultStringValue("Events")
  @Key("calendarDefaultName")
  String calendarDefaultName();

  /**
   * Translated "Delete event".
   * 
   * @return translated "Delete event"
   */
  @DefaultStringValue("Delete event")
  @Key("calendarDeleteEvent")
  String calendarDeleteEvent();

  /**
   * Translated "An error happened while deleting the event. Please retry later, contact your administrator if the problem persists.".
   * 
   * @return translated "An error happened while deleting the event. Please retry later, contact your administrator if the problem persists."
   */
  @DefaultStringValue("An error happened while deleting the event. Please retry later, contact your administrator if the problem persists.")
  @Key("calendarDeleteEventError")
  String calendarDeleteEventError();

  /**
   * Translated "Edit event".
   * 
   * @return translated "Edit event"
   */
  @DefaultStringValue("Edit event")
  @Key("calendarEditEvent")
  String calendarEditEvent();

  /**
   * Translated "Select a calendar...".
   * 
   * @return translated "Select a calendar..."
   */
  @DefaultStringValue("Select a calendar...")
  @Key("calendarEmptyChoice")
  String calendarEmptyChoice();

  /**
   * Translated "Date".
   * 
   * @return translated "Date"
   */
  @DefaultStringValue("Date")
  @Key("calendarEventDate")
  String calendarEventDate();

  /**
   * Translated "Description".
   * 
   * @return translated "Description"
   */
  @DefaultStringValue("Description")
  @Key("calendarEventDescription")
  String calendarEventDescription();

  /**
   * Translated "End hour".
   * 
   * @return translated "End hour"
   */
  @DefaultStringValue("End hour")
  @Key("calendarEventEndHour")
  String calendarEventEndHour();

  /**
   * Translated "Object".
   * 
   * @return translated "Object"
   */
  @DefaultStringValue("Object")
  @Key("calendarEventObject")
  String calendarEventObject();

  /**
   * Translated "Start hour".
   * 
   * @return translated "Start hour"
   */
  @DefaultStringValue("Start hour")
  @Key("calendarEventStartHour")
  String calendarEventStartHour();

  /**
   * Translated "No calendar selected.".
   * 
   * @return translated "No calendar selected."
   */
  @DefaultStringValue("No calendar selected.")
  @Key("calendarNoCalendarSelected")
  String calendarNoCalendarSelected();

  /**
   * Translated "Cancel".
   * 
   * @return translated "Cancel"
   */
  @DefaultStringValue("Cancel")
  @Key("cancel")
  String cancel();

  /**
   * Translated "Cancelled".
   * 
   * @return translated "Cancelled"
   */
  @DefaultStringValue("Cancelled")
  @Key("cancelled")
  String cancelled();

  /**
   * Translated "Google Maps could not be loaded. Please verify your internet connection.".
   * 
   * @return translated "Google Maps could not be loaded. Please verify your internet connection."
   */
  @DefaultStringValue("Google Maps could not be loaded. Please verify your internet connection.")
  @Key("cannotLoadMap")
  String cannotLoadMap();

  /**
   * Translated "Maps".
   * 
   * @return translated "Maps"
   */
  @DefaultStringValue("Maps")
  @Key("cartes")
  String cartes();

  /**
   * Translated "Categories".
   * 
   * @return translated "Categories"
   */
  @DefaultStringValue("Categories")
  @Key("categories")
  String categories();

  /**
   * Translated "Category".
   * 
   * @return translated "Category"
   */
  @DefaultStringValue("Category")
  @Key("category")
  String category();

  /**
   * Translated "Administration".
   * 
   * @return translated "Administration"
   */
  @DefaultStringValue("Administration")
  @Key("categoryAdministration")
  String categoryAdministration();

  /**
   * Translated "Not in any Category".
   * 
   * @return translated "Not in any Category"
   */
  @DefaultStringValue("Not in any Category")
  @Key("categoryNotMapped")
  String categoryNotMapped();

  /**
   * Translated "Others".
   * 
   * @return translated "Others"
   */
  @DefaultStringValue("Others")
  @Key("categoryOthers")
  String categoryOthers();

  /**
   * Translated "Project".
   * 
   * @return translated "Project"
   */
  @DefaultStringValue("Project")
  @Key("categoryProject")
  String categoryProject();

  /**
   * Translated "Change configuration".
   * 
   * @return translated "Change configuration"
   */
  @DefaultStringValue("Change configuration")
  @Key("changeConfiguration")
  String changeConfiguration();

  /**
   * Translated "Changing OrgUint".
   * 
   * @return translated "Changing OrgUint"
   */
  @DefaultStringValue("Changing OrgUint")
  @Key("changeOrgUnit")
  String changeOrgUnit();

  /**
   * Translated "#Warning: The project will remain attached to the current country for indicators' sites management,not the country linked to the new OrgUnit. Are you sure to continue ?".
   * 
   * @return translated "#Warning: The project will remain attached to the current country for indicators' sites management,not the country linked to the new OrgUnit. Are you sure to continue ?"
   */
  @DefaultStringValue("#Warning: The project will remain attached to the current country for indicators' sites management,not the country linked to the new OrgUnit. Are you sure to continue ?")
  @Key("changeOrgUnitDetails")
  String changeOrgUnitDetails();

  /**
   * Translated "Type of chart".
   * 
   * @return translated "Type of chart"
   */
  @DefaultStringValue("Type of chart")
  @Key("chartType")
  String chartType();

  /**
   * Translated "Charts".
   * 
   * @return translated "Charts"
   */
  @DefaultStringValue("Charts")
  @Key("charts")
  String charts();

  /**
   * Translated "Quickly produce various graphs that summarize your data.".
   * 
   * @return translated "Quickly produce various graphs that summarize your data."
   */
  @DefaultStringValue("Quickly produce various graphs that summarize your data.")
  @Key("chartsDescription")
  String chartsDescription();

  /**
   * Translated "Type of choice".
   * 
   * @return translated "Type of choice"
   */
  @DefaultStringValue("Type of choice")
  @Key("choiceType")
  String choiceType();

  /**
   * Translated "Choose a file type".
   * 
   * @return translated "Choose a file type"
   */
  @DefaultStringValue("Choose a file type")
  @Key("chooseFileType")
  String chooseFileType();

  /**
   * Translated "Closed projects filter".
   * 
   * @return translated "Closed projects filter"
   */
  @DefaultStringValue("Closed projects filter")
  @Key("closedProjectsFilterText")
  String closedProjectsFilterText();

  /**
   * Translated "Clustering".
   * 
   * @return translated "Clustering"
   */
  @DefaultStringValue("Clustering")
  @Key("clustering")
  String clustering();

  /**
   * Translated "Code".
   * 
   * @return translated "Code"
   */
  @DefaultStringValue("Code")
  @Key("code")
  String code();

  /**
   * Translated "Collapse all".
   * 
   * @return translated "Collapse all"
   */
  @DefaultStringValue("Collapse all")
  @Key("collapseAll")
  String collapseAll();

  /**
   * Translated "Collect during Intervention".
   * 
   * @return translated "Collect during Intervention"
   */
  @DefaultStringValue("Collect during Intervention")
  @Key("collectIntervention")
  String collectIntervention();

  /**
   * Translated "Collect during Monitoring".
   * 
   * @return translated "Collect during Monitoring"
   */
  @DefaultStringValue("Collect during Monitoring")
  @Key("collectMonitoring")
  String collectMonitoring();

  /**
   * Translated "Color".
   * 
   * @return translated "Color"
   */
  @DefaultStringValue("Color")
  @Key("color")
  String color();

  /**
   * Translated "Columns".
   * 
   * @return translated "Columns"
   */
  @DefaultStringValue("Columns")
  @Key("columns")
  String columns();

  /**
   * Translated "Comments".
   * 
   * @return translated "Comments"
   */
  @DefaultStringValue("Comments")
  @Key("comments")
  String comments();

  /**
   * Translated "Complete".
   * 
   * @return translated "Complete"
   */
  @DefaultStringValue("Complete")
  @Key("complete")
  String complete();

  /**
   * Translated "Configure fields to export".
   * 
   * @return translated "Configure fields to export"
   */
  @DefaultStringValue("Configure fields to export")
  @Key("configureFieldsToExport")
  String configureFieldsToExport();

  /**
   * Translated "Are you sure you want to make the current view of this screen the default view you will get each time you will open this screen again?".
   * 
   * @return translated "Are you sure you want to make the current view of this screen the default view you will get each time you will open this screen again?"
   */
  @DefaultStringValue("Are you sure you want to make the current view of this screen the default view you will get each time you will open this screen again?")
  @Key("confirmDefaultViewChange")
  String confirmDefaultViewChange();

  /**
   * Translated "Are you sure you wish to delete this indicator? Any results for entered for this indicator will also be removed.".
   * 
   * @return translated "Are you sure you wish to delete this indicator? Any results for entered for this indicator will also be removed."
   */
  @DefaultStringValue("Are you sure you wish to delete this indicator? Any results for entered for this indicator will also be removed.")
  @Key("confirmDeleteIndicator")
  String confirmDeleteIndicator();

  /**
   * Translated "Do you really want to delete this project ?".
   * 
   * @return translated "Do you really want to delete this project ?"
   */
  @DefaultStringValue("Do you really want to delete this project ?")
  @Key("confirmDeleteProjectMessageBoxContent")
  String confirmDeleteProjectMessageBoxContent();

  /**
   * Translated "Delete project".
   * 
   * @return translated "Delete project"
   */
  @DefaultStringValue("Delete project")
  @Key("confirmDeleteProjectMessageBoxTitle")
  String confirmDeleteProjectMessageBoxTitle();

  /**
   * Translated "Confirm password".
   * 
   * @return translated "Confirm password"
   */
  @DefaultStringValue("Confirm password")
  @Key("confirmPassword")
  String confirmPassword();

  /**
   * Translated "Confirm Update".
   * 
   * @return translated "Confirm Update"
   */
  @DefaultStringValue("Confirm Update")
  @Key("confirmUpdate")
  String confirmUpdate();

  /**
   * Translated "This value is the product of an aggregation. If you change this value now, you will loose the more detailed data previously entered. Continue?".
   * 
   * @return translated "This value is the product of an aggregation. If you change this value now, you will loose the more detailed data previously entered. Continue?"
   */
  @DefaultStringValue("This value is the product of an aggregation. If you change this value now, you will loose the more detailed data previously entered. Continue?")
  @Key("confirmUpdateOfAggregatedCell")
  String confirmUpdateOfAggregatedCell();

  /**
   * Translated "Connecting ...".
   * 
   * @return translated "Connecting ..."
   */
  @DefaultStringValue("Connecting ...")
  @Key("connecting")
  String connecting();

  /**
   * Translated "Connection problem, will retry in a second".
   * 
   * @return translated "Connection problem, will retry in a second"
   */
  @DefaultStringValue("Connection problem, will retry in a second")
  @Key("connectionProblem")
  String connectionProblem();

  /**
   * Translated "Connection required".
   * 
   * @return translated "Connection required"
   */
  @DefaultStringValue("Connection required")
  @Key("connectionRequired")
  String connectionRequired();

  /**
   * Translated "The action you've selected requires an internet connection.<br>You're currently working offline, would you like to try to connect?\"".
   * 
   * @return translated "The action you've selected requires an internet connection.<br>You're currently working offline, would you like to try to connect?\""
   */
  @DefaultStringValue("The action you've selected requires an internet connection.<br>You're currently working offline, would you like to try to connect?\"")
  @Key("connectionRequiredExplanation")
  String connectionRequiredExplanation();

  /**
   * Translated "Copy".
   * 
   * @return translated "Copy"
   */
  @DefaultStringValue("Copy")
  @Key("copy")
  String copy();

  /**
   * Translated "Failed to connect".
   * 
   * @return translated "Failed to connect"
   */
  @DefaultStringValue("Failed to connect")
  @Key("couldNotConnect")
  String couldNotConnect();

  /**
   * Translated "Country".
   * 
   * @return translated "Country"
   */
  @DefaultStringValue("Country")
  @Key("country")
  String country();

  /**
   * Translated "Create Database".
   * 
   * @return translated "Create Database"
   */
  @DefaultStringValue("Create Database")
  @Key("createDatabase")
  String createDatabase();

  /**
   * Translated "Form incomplete".
   * 
   * @return translated "Form incomplete"
   */
  @DefaultStringValue("Form incomplete")
  @Key("createFormIncomplete")
  String createFormIncomplete();

  /**
   * Translated "Create a new project".
   * 
   * @return translated "Create a new project"
   */
  @DefaultStringValue("Create a new project")
  @Key("createProject")
  String createProject();

  /**
   * Translated "Create".
   * 
   * @return translated "Create"
   */
  @DefaultStringValue("Create")
  @Key("createProjectCreateButton")
  String createProjectCreateButton();

  /**
   * Translated "Unable to create a project".
   * 
   * @return translated "Unable to create a project"
   */
  @DefaultStringValue("Unable to create a project")
  @Key("createProjectDisable")
  String createProjectDisable();

  /**
   * Translated "We are sorry, but some essential data to create a project could not be recovered or are missing.".
   * 
   * @return translated "We are sorry, but some essential data to create a project could not be recovered or are missing."
   */
  @DefaultStringValue("We are sorry, but some essential data to create a project could not be recovered or are missing.")
  @Key("createProjectDisableDetails")
  String createProjectDisableDetails();

  /**
   * Translated "There is no project model available. Please, create or make available one or more project models.".
   * 
   * @return translated "There is no project model available. Please, create or make available one or more project models."
   */
  @DefaultStringValue("There is no project model available. Please, create or make available one or more project models.")
  @Key("createProjectDisableModel")
  String createProjectDisableModel();

  /**
   * Translated "An error occurred while getting the project models list. Cannot create a project.".
   * 
   * @return translated "An error occurred while getting the project models list. Cannot create a project."
   */
  @DefaultStringValue("An error occurred while getting the project models list. Cannot create a project.")
  @Key("createProjectDisableModelError")
  String createProjectDisableModelError();

  /**
   * Translated "There is no organizational unit which can contain projects available. Please, modify one or more organizational units in the way they can contain projects.".
   * 
   * @return translated "There is no organizational unit which can contain projects available. Please, modify one or more organizational units in the way they can contain projects."
   */
  @DefaultStringValue("There is no organizational unit which can contain projects available. Please, modify one or more organizational units in the way they can contain projects.")
  @Key("createProjectDisableOrgUnit")
  String createProjectDisableOrgUnit();

  /**
   * Translated "An error occurred while getting the organizational units list. Cannot create a project.".
   * 
   * @return translated "An error occurred while getting the organizational units list. Cannot create a project."
   */
  @DefaultStringValue("An error occurred while getting the organizational units list. Cannot create a project.")
  @Key("createProjectDisableOrgUnitError")
  String createProjectDisableOrgUnitError();

  /**
   * Translated "Project creation failed".
   * 
   * @return translated "Project creation failed"
   */
  @DefaultStringValue("Project creation failed")
  @Key("createProjectFailed")
  String createProjectFailed();

  /**
   * Translated "An error occurred during your project's creation.".
   * 
   * @return translated "An error occurred during your project's creation."
   */
  @DefaultStringValue("An error occurred during your project's creation.")
  @Key("createProjectFailedDetails")
  String createProjectFailedDetails();

  /**
   * Translated "Edit the funding source project to link with your project.".
   * 
   * @return translated "Edit the funding source project to link with your project."
   */
  @DefaultStringValue("Edit the funding source project to link with your project.")
  @Key("createProjectFundingProjectEditDetails")
  String createProjectFundingProjectEditDetails();

  /**
   * Translated "New project".
   * 
   * @return translated "New project"
   */
  @DefaultStringValue("New project")
  @Key("createProjectNewProject")
  String createProjectNewProject();

  /**
   * Translated "Edit the funded project to link with your project.".
   * 
   * @return translated "Edit the funded project to link with your project."
   */
  @DefaultStringValue("Edit the funded project to link with your project.")
  @Key("createProjectPartnerProjectEditDetails")
  String createProjectPartnerProjectEditDetails();

  /**
   * Translated "At".
   * 
   * @return translated "At"
   */
  @DefaultStringValue("At")
  @Key("createProjectPercentage")
  String createProjectPercentage();

  /**
   * Translated "N/A".
   * 
   * @return translated "N/A"
   */
  @DefaultStringValue("N/A")
  @Key("createProjectPercentageNotAvailable")
  String createProjectPercentageNotAvailable();

  /**
   * Translated "Project creation succeeded".
   * 
   * @return translated "Project creation succeeded"
   */
  @DefaultStringValue("Project creation succeeded")
  @Key("createProjectSucceeded")
  String createProjectSucceeded();

  /**
   * Translated "Your project was successfully created. You can now retrieve it on your dashboard.".
   * 
   * @return translated "Your project was successfully created. You can now retrieve it on your dashboard."
   */
  @DefaultStringValue("Your project was successfully created. You can now retrieve it on your dashboard.")
  @Key("createProjectSucceededDetails")
  String createProjectSucceededDetails();

  /**
   * Translated "Create a new draft project".
   * 
   * @return translated "Create a new draft project"
   */
  @DefaultStringValue("Create a new draft project")
  @Key("createProjectTest")
  String createProjectTest();

  /**
   * Translated "Project type".
   * 
   * @return translated "Project type"
   */
  @DefaultStringValue("Project type")
  @Key("createProjectType")
  String createProjectType();

  /**
   * Translated "Unable to add or to create a project".
   * 
   * @return translated "Unable to add or to create a project"
   */
  @DefaultStringValue("Unable to add or to create a project")
  @Key("createProjectTypeError")
  String createProjectTypeError();

  /**
   * Translated "We are sorry, but some essential data to add or to create a project could not be recovered or are missing.".
   * 
   * @return translated "We are sorry, but some essential data to add or to create a project could not be recovered or are missing."
   */
  @DefaultStringValue("We are sorry, but some essential data to add or to create a project could not be recovered or are missing.")
  @Key("createProjectTypeErrorDetails")
  String createProjectTypeErrorDetails();

  /**
   * Translated "Funding source project".
   * 
   * @return translated "Funding source project"
   */
  @DefaultStringValue("Funding source project")
  @Key("createProjectTypeFunding")
  String createProjectTypeFunding();

  /**
   * Translated "Funding project".
   * 
   * @return translated "Funding project"
   */
  @DefaultStringValue("Funding project")
  @Key("createProjectTypeFunding2")
  String createProjectTypeFunding2();

  /**
   * Translated "Create".
   * 
   * @return translated "Create"
   */
  @DefaultStringValue("Create")
  @Key("createProjectTypeFundingCreate")
  String createProjectTypeFundingCreate();

  /**
   * Translated "Create a new funding source project to link with your project".
   * 
   * @return translated "Create a new funding source project to link with your project"
   */
  @DefaultStringValue("Create a new funding source project to link with your project")
  @Key("createProjectTypeFundingCreateDetails")
  String createProjectTypeFundingCreateDetails();

  /**
   * Translated "An error occurs during the creation of the financial link.".
   * 
   * @return translated "An error occurs during the creation of the financial link."
   */
  @DefaultStringValue("An error occurs during the creation of the financial link.")
  @Key("createProjectTypeFundingCreationDetails")
  String createProjectTypeFundingCreationDetails();

  /**
   * Translated "Unable to create the financial link".
   * 
   * @return translated "Unable to create the financial link"
   */
  @DefaultStringValue("Unable to create the financial link")
  @Key("createProjectTypeFundingCreationError")
  String createProjectTypeFundingCreationError();

  /**
   * Translated "Select".
   * 
   * @return translated "Select"
   */
  @DefaultStringValue("Select")
  @Key("createProjectTypeFundingSelect")
  String createProjectTypeFundingSelect();

  /**
   * Translated "Select the funding source project to link with your project".
   * 
   * @return translated "Select the funding source project to link with your project"
   */
  @DefaultStringValue("Select the funding source project to link with your project")
  @Key("createProjectTypeFundingSelectDetails")
  String createProjectTypeFundingSelectDetails();

  /**
   * Translated "No funding source project available".
   * 
   * @return translated "No funding source project available"
   */
  @DefaultStringValue("No funding source project available")
  @Key("createProjectTypeFundingSelectNone")
  String createProjectTypeFundingSelectNone();

  /**
   * Translated "There isn't an available funding source project to link with your project. Please, create a new funding source project first.".
   * 
   * @return translated "There isn't an available funding source project to link with your project. Please, create a new funding source project first."
   */
  @DefaultStringValue("There isn't an available funding source project to link with your project. Please, create a new funding source project first.")
  @Key("createProjectTypeFundingSelectNoneDetails")
  String createProjectTypeFundingSelectNoneDetails();

  /**
   * Translated "The funding source project has been added correctly.".
   * 
   * @return translated "The funding source project has been added correctly."
   */
  @DefaultStringValue("The funding source project has been added correctly.")
  @Key("createProjectTypeFundingSelectOk")
  String createProjectTypeFundingSelectOk();

  /**
   * Translated "NGO project".
   * 
   * @return translated "NGO project"
   */
  @DefaultStringValue("NGO project")
  @Key("createProjectTypeNGO")
  String createProjectTypeNGO();

  /**
   * Translated "Funded project".
   * 
   * @return translated "Funded project"
   */
  @DefaultStringValue("Funded project")
  @Key("createProjectTypePartner")
  String createProjectTypePartner();

  /**
   * Translated "Local partner".
   * 
   * @return translated "Local partner"
   */
  @DefaultStringValue("Local partner")
  @Key("createProjectTypePartner2")
  String createProjectTypePartner2();

  /**
   * Translated "Create".
   * 
   * @return translated "Create"
   */
  @DefaultStringValue("Create")
  @Key("createProjectTypePartnerCreate")
  String createProjectTypePartnerCreate();

  /**
   * Translated "Create a new funded project to link with your project".
   * 
   * @return translated "Create a new funded project to link with your project"
   */
  @DefaultStringValue("Create a new funded project to link with your project")
  @Key("createProjectTypePartnerCreateDetails")
  String createProjectTypePartnerCreateDetails();

  /**
   * Translated "Select".
   * 
   * @return translated "Select"
   */
  @DefaultStringValue("Select")
  @Key("createProjectTypePartnerSelect")
  String createProjectTypePartnerSelect();

  /**
   * Translated "Select the funded project to link with your project".
   * 
   * @return translated "Select the funded project to link with your project"
   */
  @DefaultStringValue("Select the funded project to link with your project")
  @Key("createProjectTypePartnerSelectDetails")
  String createProjectTypePartnerSelectDetails();

  /**
   * Translated "No funded project available".
   * 
   * @return translated "No funded project available"
   */
  @DefaultStringValue("No funded project available")
  @Key("createProjectTypePartnerSelectNone")
  String createProjectTypePartnerSelectNone();

  /**
   * Translated "There isn't an available funded project to link with project. Please, create a new funded project first.".
   * 
   * @return translated "There isn't an available funded project to link with project. Please, create a new funded project first."
   */
  @DefaultStringValue("There isn't an available funded project to link with project. Please, create a new funded project first.")
  @Key("createProjectTypePartnerSelectNoneDetails")
  String createProjectTypePartnerSelectNoneDetails();

  /**
   * Translated "The funded project has been added correctly.".
   * 
   * @return translated "The funded project has been added correctly."
   */
  @DefaultStringValue("The funded project has been added correctly.")
  @Key("createProjectTypePartnerSelectOk")
  String createProjectTypePartnerSelectOk();

  /**
   * Translated "Draft project".
   * 
   * @return translated "Draft project"
   */
  @DefaultStringValue("Draft project")
  @Key("createTestProject")
  String createTestProject();

  /**
   * Translated "Your draft projects".
   * 
   * @return translated "Your draft projects"
   */
  @DefaultStringValue("Your draft projects")
  @Key("createTestProjectListe")
  String createTestProjectListe();

  /**
   * Translated "Your draft project was successfully created. You can now retrieve it on your draft projects list.".
   * 
   * @return translated "Your draft project was successfully created. You can now retrieve it on your draft projects list."
   */
  @DefaultStringValue("Your draft project was successfully created. You can now retrieve it on your draft projects list.")
  @Key("createTestProjectSucceededDetails")
  String createTestProjectSucceededDetails();

  /**
   * Translated "Credits".
   * 
   * @return translated "Credits"
   */
  @DefaultStringValue("Credits")
  @Key("credits")
  String credits();

  /**
   * Translated "CSV".
   * 
   * @return translated "CSV"
   */
  @DefaultStringValue("CSV")
  @Key("csv")
  String csv();

  /**
   * Translated "&euro;".
   * 
   * @return translated "&euro;"
   */
  @DefaultStringValue("&euro;")
  @Key("currencyEuro")
  String currencyEuro();

  /**
   * Translated "Custom Date Range".
   * 
   * @return translated "Custom Date Range"
   */
  @DefaultStringValue("Custom Date Range")
  @Key("customDateRange")
  String customDateRange();

  /**
   * Translated "choose past time limit".
   * 
   * @return translated "choose past time limit"
   */
  @DefaultStringValue("choose past time limit")
  @Key("customFilter")
  String customFilter();

  /**
   * Translated "Daily".
   * 
   * @return translated "Daily"
   */
  @DefaultStringValue("Daily")
  @Key("daily")
  String daily();

  /**
   * Translated "Dashboard".
   * 
   * @return translated "Dashboard"
   */
  @DefaultStringValue("Dashboard")
  @Key("dashboard")
  String dashboard();

  /**
   * Translated "Data Collection".
   * 
   * @return translated "Data Collection"
   */
  @DefaultStringValue("Data Collection")
  @Key("dataCollection")
  String dataCollection();

  /**
   * Translated "Data Entry".
   * 
   * @return translated "Data Entry"
   */
  @DefaultStringValue("Data Entry")
  @Key("dataEntry")
  String dataEntry();

  /**
   * Translated "Enter the results of your activities with easy to use forms, worksheets, and interactive maps.".
   * 
   * @return translated "Enter the results of your activities with easy to use forms, worksheets, and interactive maps."
   */
  @DefaultStringValue("Enter the results of your activities with easy to use forms, worksheets, and interactive maps.")
  @Key("dataEntryDescription")
  String dataEntryDescription();

  /**
   * Translated "Database".
   * 
   * @return translated "Database"
   */
  @DefaultStringValue("Database")
  @Key("database")
  String database();

  /**
   * Translated "Databases".
   * 
   * @return translated "Databases"
   */
  @DefaultStringValue("Databases")
  @Key("databases")
  String databases();

  /**
   * Translated "Date".
   * 
   * @return translated "Date"
   */
  @DefaultStringValue("Date")
  @Key("date")
  String date();

  /**
   * Translated "Day".
   * 
   * @return translated "Day"
   */
  @DefaultStringValue("Day")
  @Key("day")
  String day();

  /**
   * Translated "Day in Month".
   * 
   * @return translated "Day in Month"
   */
  @DefaultStringValue("Day in Month")
  @Key("dayInMonth")
  String dayInMonth();

  /**
   * Translated "Day in Week".
   * 
   * @return translated "Day in Week"
   */
  @DefaultStringValue("Day in Week")
  @Key("dayInWeek")
  String dayInWeek();

  /**
   * Translated "Sunday".
   * 
   * @return translated "Sunday"
   */
  @DefaultStringValue("Sunday")
  @Key("dayName_1")
  String dayName_1();

  /**
   * Translated "Monday".
   * 
   * @return translated "Monday"
   */
  @DefaultStringValue("Monday")
  @Key("dayName_2")
  String dayName_2();

  /**
   * Translated "Tuesday".
   * 
   * @return translated "Tuesday"
   */
  @DefaultStringValue("Tuesday")
  @Key("dayName_3")
  String dayName_3();

  /**
   * Translated "Wednesday".
   * 
   * @return translated "Wednesday"
   */
  @DefaultStringValue("Wednesday")
  @Key("dayName_4")
  String dayName_4();

  /**
   * Translated "Thursday".
   * 
   * @return translated "Thursday"
   */
  @DefaultStringValue("Thursday")
  @Key("dayName_5")
  String dayName_5();

  /**
   * Translated "Friday".
   * 
   * @return translated "Friday"
   */
  @DefaultStringValue("Friday")
  @Key("dayName_6")
  String dayName_6();

  /**
   * Translated "Saturday".
   * 
   * @return translated "Saturday"
   */
  @DefaultStringValue("Saturday")
  @Key("dayName_7")
  String dayName_7();

  /**
   * Translated "Default spreadsheet export file format".
   * 
   * @return translated "Default spreadsheet export file format"
   */
  @DefaultStringValue("Default spreadsheet export file format")
  @Key("defaultExportFormat")
  String defaultExportFormat();

  /**
   * Translated "Default view".
   * 
   * @return translated "Default view"
   */
  @DefaultStringValue("Default view")
  @Key("defaultView")
  String defaultView();

  /**
   * Translated "The default view has been saved.".
   * 
   * @return translated "The default view has been saved."
   */
  @DefaultStringValue("The default view has been saved.")
  @Key("defaultViewChanged")
  String defaultViewChanged();

  /**
   * Translated "Delete".
   * 
   * @return translated "Delete"
   */
  @DefaultStringValue("Delete")
  @Key("delete")
  String delete();

  /**
   * Translated "An error occurred while getting the project models list. Cannot delete a category or category element.".
   * 
   * @return translated "An error occurred while getting the project models list. Cannot delete a category or category element."
   */
  @DefaultStringValue("An error occurred while getting the project models list. Cannot delete a category or category element.")
  @Key("deleteCategoryGetProjectModelsError")
  String deleteCategoryGetProjectModelsError();

  /**
   * Translated "Deletion confirmation".
   * 
   * @return translated "Deletion confirmation"
   */
  @DefaultStringValue("Deletion confirmation")
  @Key("deleteConfirm")
  String deleteConfirm();

  /**
   * Translated "Are you sure to delete ?".
   * 
   * @return translated "Are you sure to delete ?"
   */
  @DefaultStringValue("Are you sure to delete ?")
  @Key("deleteConfirmMessage")
  String deleteConfirmMessage();

  /**
   * Translated "Warning: You are trying to delete a OrgUnit model. Are you sure to continue ?".
   * 
   * @return translated "Warning: You are trying to delete a OrgUnit model. Are you sure to continue ?"
   */
  @DefaultStringValue("Warning: You are trying to delete a OrgUnit model. Are you sure to continue ?")
  @Key("deleteDraftOrgUnitModelConfirm")
  String deleteDraftOrgUnitModelConfirm();

  /**
   * Translated "Warning: all objects and projects using this project model will be deleted. Are you sure to delete it ?".
   * 
   * @return translated "Warning: all objects and projects using this project model will be deleted. Are you sure to delete it ?"
   */
  @DefaultStringValue("Warning: all objects and projects using this project model will be deleted. Are you sure to delete it ?")
  @Key("deleteDraftProjectModelConfirm")
  String deleteDraftProjectModelConfirm();

  /**
   * Translated "Sorry,you can not delete a non draft OrgUnit model !".
   * 
   * @return translated "Sorry,you can not delete a non draft OrgUnit model !"
   */
  @DefaultStringValue("Sorry,you can not delete a non draft OrgUnit model !")
  @Key("deleteNotDraftOrgUnitModelError")
  String deleteNotDraftOrgUnitModelError();

  /**
   * Translated "Sorry, you can not delete a non draft project model.".
   * 
   * @return translated "Sorry, you can not delete a non draft project model."
   */
  @DefaultStringValue("Sorry, you can not delete a non draft project model.")
  @Key("deleteNotDraftProjectModelError")
  String deleteNotDraftProjectModelError();

  /**
   * Translated "Warning: all phases and fields using this phase model will be also deleted! The projects using this model maybe will not work any more. Are you sure to delete it?".
   * 
   * @return translated "Warning: all phases and fields using this phase model will be also deleted! The projects using this model maybe will not work any more. Are you sure to delete it?"
   */
  @DefaultStringValue("Warning: all phases and fields using this phase model will be also deleted! The projects using this model maybe will not work any more. Are you sure to delete it?")
  @Key("deletePhaseModelConfirm")
  String deletePhaseModelConfirm();

  /**
   * Translated "Delete this project".
   * 
   * @return translated "Delete this project"
   */
  @DefaultStringValue("Delete this project")
  @Key("deleteProjectAnchor")
  String deleteProjectAnchor();

  /**
   * Translated "The project was correctly deleted".
   * 
   * @return translated "The project was correctly deleted"
   */
  @DefaultStringValue("The project was correctly deleted")
  @Key("deleteProjectNotificationContent")
  String deleteProjectNotificationContent();

  /**
   * Translated "Project deleted".
   * 
   * @return translated "Project deleted"
   */
  @DefaultStringValue("Project deleted")
  @Key("deleteProjectNotificationTitle")
  String deleteProjectNotificationTitle();

  /**
   * Translated "Warning: This is the last phase model in this project model. All phases and fields using this phase model will be also deleted! The projects using this model maybe will not work any more. Are you sure to delete it?".
   * 
   * @return translated "Warning: This is the last phase model in this project model. All phases and fields using this phase model will be also deleted! The projects using this model maybe will not work any more. Are you sure to delete it?"
   */
  @DefaultStringValue("Warning: This is the last phase model in this project model. All phases and fields using this phase model will be also deleted! The projects using this model maybe will not work any more. Are you sure to delete it?")
  @Key("deleteRootPhaseModelConfirm")
  String deleteRootPhaseModelConfirm();

  /**
   * Translated "You can not delete this phase because it is the phase chosen as initial for your project model.".
   * 
   * @return translated "You can not delete this phase because it is the phase chosen as initial for your project model."
   */
  @DefaultStringValue("You can not delete this phase because it is the phase chosen as initial for your project model.")
  @Key("deleteRootPhaseModelError")
  String deleteRootPhaseModelError();

  /**
   * Translated "Delete Site".
   * 
   * @return translated "Delete Site"
   */
  @DefaultStringValue("Delete Site")
  @Key("deleteSite")
  String deleteSite();

  /**
   * Translated "An error occurred while deleting your draft project.".
   * 
   * @return translated "An error occurred while deleting your draft project."
   */
  @DefaultStringValue("An error occurred while deleting your draft project.")
  @Key("deleteTestProject")
  String deleteTestProject();

  /**
   * Translated "Do you want to delete this draft project ?".
   * 
   * @return translated "Do you want to delete this draft project ?"
   */
  @DefaultStringValue("Do you want to delete this draft project ?")
  @Key("deleteTestProjectConfirm")
  String deleteTestProjectConfirm();

  /**
   * Translated "Draft project deleting".
   * 
   * @return translated "Draft project deleting"
   */
  @DefaultStringValue("Draft project deleting")
  @Key("deleteTestProjectHeader")
  String deleteTestProjectHeader();

  /**
   * Translated "Your draft project was successfully deleted.".
   * 
   * @return translated "Your draft project was successfully deleted."
   */
  @DefaultStringValue("Your draft project was successfully deleted.")
  @Key("deleteTestProjectSucceededDetails")
  String deleteTestProjectSucceededDetails();

  /**
   * Translated "Deleting...".
   * 
   * @return translated "Deleting..."
   */
  @DefaultStringValue("Deleting...")
  @Key("deleting")
  String deleting();

  /**
   * Translated "Deletion Error".
   * 
   * @return translated "Deletion Error"
   */
  @DefaultStringValue("Deletion Error")
  @Key("deletionError")
  String deletionError();

  /**
   * Translated "Description".
   * 
   * @return translated "Description"
   */
  @DefaultStringValue("Description")
  @Key("description")
  String description();

  /**
   * Translated "Design".
   * 
   * @return translated "Design"
   */
  @DefaultStringValue("Design")
  @Key("design")
  String design();

  /**
   * Translated "Create or change the activities and their indicators which are part of this database.".
   * 
   * @return translated "Create or change the activities and their indicators which are part of this database."
   */
  @DefaultStringValue("Create or change the activities and their indicators which are part of this database.")
  @Key("designDescription")
  String designDescription();

  /**
   * Translated "Details".
   * 
   * @return translated "Details"
   */
  @DefaultStringValue("Details")
  @Key("details")
  String details();

  /**
   * Translated "Dimensions".
   * 
   * @return translated "Dimensions"
   */
  @DefaultStringValue("Dimensions")
  @Key("dimensions")
  String dimensions();

  /**
   * Translated "Discard Changes".
   * 
   * @return translated "Discard Changes"
   */
  @DefaultStringValue("Discard Changes")
  @Key("discardChanges")
  String discardChanges();

  /**
   * Translated "#Error: A '\"Draft\"' model can be only shifted to status '\"Ready\"' !".
   * 
   * @return translated "#Error: A '\"Draft\"' model can be only shifted to status '\"Ready\"' !"
   */
  @DefaultStringValue("#Error: A '\"Draft\"' model can be only shifted to status '\"Ready\"' !")
  @Key("draftModelStatusChangeError")
  String draftModelStatusChangeError();

  /**
   * Translated "Drilldown".
   * 
   * @return translated "Drilldown"
   */
  @DefaultStringValue("Drilldown")
  @Key("drilldown")
  String drilldown();

  /**
   * Translated "During the intervention".
   * 
   * @return translated "During the intervention"
   */
  @DefaultStringValue("During the intervention")
  @Key("duringIntervention")
  String duringIntervention();

  /**
   * Translated "During monitoring".
   * 
   * @return translated "During monitoring"
   */
  @DefaultStringValue("During monitoring")
  @Key("duringMonitoring")
  String duringMonitoring();

  /**
   * Translated "E".
   * 
   * @return translated "E"
   */
  @DefaultStringValue("E")
  @Key("eastHemiChars")
  String eastHemiChars();

  /**
   * Translated "Edit".
   * 
   * @return translated "Edit"
   */
  @DefaultStringValue("Edit")
  @Key("edit")
  String edit();

  /**
   * Translated "Edit".
   * 
   * @return translated "Edit"
   */
  @DefaultStringValue("Edit")
  @Key("editItem")
  String editItem();

  /**
   * Translated "Changer le mot de passe".
   * 
   * @return translated "Changer le mot de passe"
   */
  @DefaultStringValue("Changer le mot de passe")
  @Key("editPassword")
  String editPassword();

  /**
   * Translated "Edit Site".
   * 
   * @return translated "Edit Site"
   */
  @DefaultStringValue("Edit Site")
  @Key("editSite")
  String editSite();

  /**
   * Translated "Email".
   * 
   * @return translated "Email"
   */
  @DefaultStringValue("Email")
  @Key("email")
  String email();

  /**
   * Translated "Empty".
   * 
   * @return translated "Empty"
   */
  @DefaultStringValue("Empty")
  @Key("empty")
  String empty();

  /**
   * Translated "<empty>".
   * 
   * @return translated "<empty>"
   */
  @DefaultStringValue("<empty>")
  @Key("emptyIndicatorValue")
  String emptyIndicatorValue();

  /**
   * Translated "End Date".
   * 
   * @return translated "End Date"
   */
  @DefaultStringValue("End Date")
  @Key("endDate")
  String endDate();

  /**
   * Translated "Data Entry".
   * 
   * @return translated "Data Entry"
   */
  @DefaultStringValue("Data Entry")
  @Key("enterData")
  String enterData();

  /**
   * Translated "Error".
   * 
   * @return translated "Error"
   */
  @DefaultStringValue("Error")
  @Key("error")
  String error();

  /**
   * Translated "An error occurred on the server.".
   * 
   * @return translated "An error occurred on the server."
   */
  @DefaultStringValue("An error occurred on the server.")
  @Key("errorOnServer")
  String errorOnServer();

  /**
   * Translated "Every Month".
   * 
   * @return translated "Every Month"
   */
  @DefaultStringValue("Every Month")
  @Key("everyMonth")
  String everyMonth();

  /**
   * Translated "Every Week".
   * 
   * @return translated "Every Week"
   */
  @DefaultStringValue("Every Week")
  @Key("everyWeek")
  String everyWeek();

  /**
   * Translated "Excel".
   * 
   * @return translated "Excel"
   */
  @DefaultStringValue("Excel")
  @Key("excel")
  String excel();

  /**
   * Translated "Excel 97-2003".
   * 
   * @return translated "Excel 97-2003"
   */
  @DefaultStringValue("Excel 97-2003")
  @Key("excel1997")
  String excel1997();

  /**
   * Translated "Excel 2007".
   * 
   * @return translated "Excel 2007"
   */
  @DefaultStringValue("Excel 2007")
  @Key("excel2007")
  String excel2007();

  /**
   * Translated "Expand all".
   * 
   * @return translated "Expand all"
   */
  @DefaultStringValue("Expand all")
  @Key("expandAll")
  String expandAll();

  /**
   * Translated "Export".
   * 
   * @return translated "Export"
   */
  @DefaultStringValue("Export")
  @Key("export")
  String export();

  /**
   * Translated "Export all".
   * 
   * @return translated "Export all"
   */
  @DefaultStringValue("Export all")
  @Key("exportAll")
  String exportAll();

  /**
   * Translated "Export backup selection".
   * 
   * @return translated "Export backup selection"
   */
  @DefaultStringValue("Export backup selection")
  @Key("exportBackSelection")
  String exportBackSelection();

  /**
   * Translated "Export data".
   * 
   * @return translated "Export data"
   */
  @DefaultStringValue("Export data")
  @Key("exportData")
  String exportData();

  /**
   * Translated "Export".
   * 
   * @return translated "Export"
   */
  @DefaultStringValue("Export")
  @Key("exportItem")
  String exportItem();

  /**
   * Translated "Your changes are correctly saved !".
   * 
   * @return translated "Your changes are correctly saved !"
   */
  @DefaultStringValue("Your changes are correctly saved !")
  @Key("exportManagementSaveChangesNotificationMessage")
  String exportManagementSaveChangesNotificationMessage();

  /**
   * Translated "Changes saved".
   * 
   * @return translated "Changes saved"
   */
  @DefaultStringValue("Changes saved")
  @Key("exportManagementSaveChangesNotificationTitle")
  String exportManagementSaveChangesNotificationTitle();

  /**
   * Translated "Export options".
   * 
   * @return translated "Export options"
   */
  @DefaultStringValue("Export options")
  @Key("exportOptions")
  String exportOptions();

  /**
   * Translated "Export to Excel".
   * 
   * @return translated "Export to Excel"
   */
  @DefaultStringValue("Export to Excel")
  @Key("exportToExcel")
  String exportToExcel();

  /**
   * Translated "Export to Word".
   * 
   * @return translated "Export to Word"
   */
  @DefaultStringValue("Export to Word")
  @Key("exportToWord")
  String exportToWord();

  /**
   * Translated "Feature not yet implemented".
   * 
   * @return translated "Feature not yet implemented"
   */
  @DefaultStringValue("Feature not yet implemented")
  @Key("featureNotImplemented")
  String featureNotImplemented();

  /**
   * Translated "We are sorry, this feature hasn't yet been implemented. It will be done in a future release.".
   * 
   * @return translated "We are sorry, this feature hasn't yet been implemented. It will be done in a future release."
   */
  @DefaultStringValue("We are sorry, this feature hasn't yet been implemented. It will be done in a future release.")
  @Key("featureNotImplementedDetails")
  String featureNotImplementedDetails();

  /**
   * Translated "Filter".
   * 
   * @return translated "Filter"
   */
  @DefaultStringValue("Filter")
  @Key("filter")
  String filter();

  /**
   * Translated "Filter by date".
   * 
   * @return translated "Filter by date"
   */
  @DefaultStringValue("Filter by date")
  @Key("filterByDate")
  String filterByDate();

  /**
   * Translated "Filter by custom date range".
   * 
   * @return translated "Filter by custom date range"
   */
  @DefaultStringValue("Filter by custom date range")
  @Key("filterByDateRange")
  String filterByDateRange();

  /**
   * Translated "Filter by geography".
   * 
   * @return translated "Filter by geography"
   */
  @DefaultStringValue("Filter by geography")
  @Key("filterByGeography")
  String filterByGeography();

  /**
   * Translated "Filter by partner".
   * 
   * @return translated "Filter by partner"
   */
  @DefaultStringValue("Filter by partner")
  @Key("filterByPartner")
  String filterByPartner();

  /**
   * Translated "Budget distribution".
   * 
   * @return translated "Budget distribution"
   */
  @DefaultStringValue("Budget distribution")
  @Key("flexibleElementBudgetDistribution")
  String flexibleElementBudgetDistribution();

  /**
   * Translated "Spent budget".
   * 
   * @return translated "Spent budget"
   */
  @DefaultStringValue("Spent budget")
  @Key("flexibleElementBudgetDistributionRatio")
  String flexibleElementBudgetDistributionRatio();

  /**
   * Translated "Checkbox".
   * 
   * @return translated "Checkbox"
   */
  @DefaultStringValue("Checkbox")
  @Key("flexibleElementCheckbox")
  String flexibleElementCheckbox();

  /**
   * Translated "decimal".
   * 
   * @return translated "decimal"
   */
  @DefaultStringValue("decimal")
  @Key("flexibleElementDecimalValue")
  String flexibleElementDecimalValue();

  /**
   * Translated "Default element".
   * 
   * @return translated "Default element"
   */
  @DefaultStringValue("Default element")
  @Key("flexibleElementDefault")
  String flexibleElementDefault();

  /**
   * Translated "Select a country...".
   * 
   * @return translated "Select a country..."
   */
  @DefaultStringValue("Select a country...")
  @Key("flexibleElementDefaultSelectCountry")
  String flexibleElementDefaultSelectCountry();

  /**
   * Translated "Select a manager...".
   * 
   * @return translated "Select a manager..."
   */
  @DefaultStringValue("Select a manager...")
  @Key("flexibleElementDefaultSelectManager")
  String flexibleElementDefaultSelectManager();

  /**
   * Translated "Files list".
   * 
   * @return translated "Files list"
   */
  @DefaultStringValue("Files list")
  @Key("flexibleElementFilesList")
  String flexibleElementFilesList();

  /**
   * Translated "Add file".
   * 
   * @return translated "Add file"
   */
  @DefaultStringValue("Add file")
  @Key("flexibleElementFilesListAddDocument")
  String flexibleElementFilesListAddDocument();

  /**
   * Translated "Add a file.".
   * 
   * @return translated "Add a file."
   */
  @DefaultStringValue("Add a file.")
  @Key("flexibleElementFilesListAddDocumentDetails")
  String flexibleElementFilesListAddDocumentDetails();

  /**
   * Translated "This phase is closed, unable to attach a document anymore.".
   * 
   * @return translated "This phase is closed, unable to attach a document anymore."
   */
  @DefaultStringValue("This phase is closed, unable to attach a document anymore.")
  @Key("flexibleElementFilesListAddErrorPhaseClosed")
  String flexibleElementFilesListAddErrorPhaseClosed();

  /**
   * Translated "This phase isn't activated, unable to attach a document yet.".
   * 
   * @return translated "This phase isn't activated, unable to attach a document yet."
   */
  @DefaultStringValue("This phase isn't activated, unable to attach a document yet.")
  @Key("flexibleElementFilesListAddErrorPhaseInactive")
  String flexibleElementFilesListAddErrorPhaseInactive();

  /**
   * Translated "Author".
   * 
   * @return translated "Author"
   */
  @DefaultStringValue("Author")
  @Key("flexibleElementFilesListAuthor")
  String flexibleElementFilesListAuthor();

  /**
   * Translated "Comments".
   * 
   * @return translated "Comments"
   */
  @DefaultStringValue("Comments")
  @Key("flexibleElementFilesListComments")
  String flexibleElementFilesListComments();

  /**
   * Translated "Date".
   * 
   * @return translated "Date"
   */
  @DefaultStringValue("Date")
  @Key("flexibleElementFilesListDate")
  String flexibleElementFilesListDate();

  /**
   * Translated "Delete a file".
   * 
   * @return translated "Delete a file"
   */
  @DefaultStringValue("Delete a file")
  @Key("flexibleElementFilesListDelete")
  String flexibleElementFilesListDelete();

  /**
   * Translated "Deletion error.".
   * 
   * @return translated "Deletion error."
   */
  @DefaultStringValue("Deletion error.")
  @Key("flexibleElementFilesListDeleteError")
  String flexibleElementFilesListDeleteError();

  /**
   * Translated "An error occurred during the deletion of the this file.".
   * 
   * @return translated "An error occurred during the deletion of the this file."
   */
  @DefaultStringValue("An error occurred during the deletion of the this file.")
  @Key("flexibleElementFilesListDeleteErrorDetails")
  String flexibleElementFilesListDeleteErrorDetails();

  /**
   * Translated "Details".
   * 
   * @return translated "Details"
   */
  @DefaultStringValue("Details")
  @Key("flexibleElementFilesListDetails")
  String flexibleElementFilesListDetails();

  /**
   * Translated "Download".
   * 
   * @return translated "Download"
   */
  @DefaultStringValue("Download")
  @Key("flexibleElementFilesListDownload")
  String flexibleElementFilesListDownload();

  /**
   * Translated "Downloading error".
   * 
   * @return translated "Downloading error"
   */
  @DefaultStringValue("Downloading error")
  @Key("flexibleElementFilesListDownloadError")
  String flexibleElementFilesListDownloadError();

  /**
   * Translated "An error occurred during the download.".
   * 
   * @return translated "An error occurred during the download."
   */
  @DefaultStringValue("An error occurred during the download.")
  @Key("flexibleElementFilesListDownloadErrorDetails")
  String flexibleElementFilesListDownloadErrorDetails();

  /**
   * Translated "History of".
   * 
   * @return translated "History of"
   */
  @DefaultStringValue("History of")
  @Key("flexibleElementFilesListFileHistory")
  String flexibleElementFilesListFileHistory();

  /**
   * Translated "History".
   * 
   * @return translated "History"
   */
  @DefaultStringValue("History")
  @Key("flexibleElementFilesListHistory")
  String flexibleElementFilesListHistory();

  /**
   * Translated "Name".
   * 
   * @return translated "Name"
   */
  @DefaultStringValue("Name")
  @Key("flexibleElementFilesListName")
  String flexibleElementFilesListName();

  /**
   * Translated "Next version number".
   * 
   * @return translated "Next version number"
   */
  @DefaultStringValue("Next version number")
  @Key("flexibleElementFilesListNextVersion")
  String flexibleElementFilesListNextVersion();

  /**
   * Translated "Properties".
   * 
   * @return translated "Properties"
   */
  @DefaultStringValue("Properties")
  @Key("flexibleElementFilesListProperties")
  String flexibleElementFilesListProperties();

  /**
   * Translated "Size".
   * 
   * @return translated "Size"
   */
  @DefaultStringValue("Size")
  @Key("flexibleElementFilesListSize")
  String flexibleElementFilesListSize();

  /**
   * Translated "B".
   * 
   * @return translated "B"
   */
  @DefaultStringValue("B")
  @Key("flexibleElementFilesListSizeByteUnit")
  String flexibleElementFilesListSizeByteUnit();

  /**
   * Translated "GB".
   * 
   * @return translated "GB"
   */
  @DefaultStringValue("GB")
  @Key("flexibleElementFilesListSizeGByteUnit")
  String flexibleElementFilesListSizeGByteUnit();

  /**
   * Translated "KB".
   * 
   * @return translated "KB"
   */
  @DefaultStringValue("KB")
  @Key("flexibleElementFilesListSizeKByteUnit")
  String flexibleElementFilesListSizeKByteUnit();

  /**
   * Translated "MB".
   * 
   * @return translated "MB"
   */
  @DefaultStringValue("MB")
  @Key("flexibleElementFilesListSizeMByteUnit")
  String flexibleElementFilesListSizeMByteUnit();

  /**
   * Translated "TB".
   * 
   * @return translated "TB"
   */
  @DefaultStringValue("TB")
  @Key("flexibleElementFilesListSizeTByteUnit")
  String flexibleElementFilesListSizeTByteUnit();

  /**
   * Translated "Upload".
   * 
   * @return translated "Upload"
   */
  @DefaultStringValue("Upload")
  @Key("flexibleElementFilesListUpload")
  String flexibleElementFilesListUpload();

  /**
   * Translated "Uploading error".
   * 
   * @return translated "Uploading error"
   */
  @DefaultStringValue("Uploading error")
  @Key("flexibleElementFilesListUploadError")
  String flexibleElementFilesListUploadError();

  /**
   * Translated "An error occurred during the upload.".
   * 
   * @return translated "An error occurred during the upload."
   */
  @DefaultStringValue("An error occurred during the upload.")
  @Key("flexibleElementFilesListUploadErrorDetails")
  String flexibleElementFilesListUploadErrorDetails();

  /**
   * Translated "The file is empty.".
   * 
   * @return translated "The file is empty."
   */
  @DefaultStringValue("The file is empty.")
  @Key("flexibleElementFilesListUploadErrorEmpty")
  String flexibleElementFilesListUploadErrorEmpty();

  /**
   * Translated "The file is too big.".
   * 
   * @return translated "The file is too big."
   */
  @DefaultStringValue("The file is too big.")
  @Key("flexibleElementFilesListUploadErrorTooBig")
  String flexibleElementFilesListUploadErrorTooBig();

  /**
   * Translated "Add a new file".
   * 
   * @return translated "Add a new file"
   */
  @DefaultStringValue("Add a new file")
  @Key("flexibleElementFilesListUploadFile")
  String flexibleElementFilesListUploadFile();

  /**
   * Translated "Your document has been correctly added.".
   * 
   * @return translated "Your document has been correctly added."
   */
  @DefaultStringValue("Your document has been correctly added.")
  @Key("flexibleElementFilesListUploadFileConfirm")
  String flexibleElementFilesListUploadFileConfirm();

  /**
   * Translated "New version".
   * 
   * @return translated "New version"
   */
  @DefaultStringValue("New version")
  @Key("flexibleElementFilesListUploadVersion")
  String flexibleElementFilesListUploadVersion();

  /**
   * Translated "Version".
   * 
   * @return translated "Version"
   */
  @DefaultStringValue("Version")
  @Key("flexibleElementFilesListVersion")
  String flexibleElementFilesListVersion();

  /**
   * Translated "Delete a version".
   * 
   * @return translated "Delete a version"
   */
  @DefaultStringValue("Delete a version")
  @Key("flexibleElementFilesListVersionDelete")
  String flexibleElementFilesListVersionDelete();

  /**
   * Translated "Unauthorized operation".
   * 
   * @return translated "Unauthorized operation"
   */
  @DefaultStringValue("Unauthorized operation")
  @Key("flexibleElementFilesListVersionDeleteForbidden")
  String flexibleElementFilesListVersionDeleteForbidden();

  /**
   * Translated "A file must have at least one version. You cannot delete the unique version of this file.".
   * 
   * @return translated "A file must have at least one version. You cannot delete the unique version of this file."
   */
  @DefaultStringValue("A file must have at least one version. You cannot delete the unique version of this file.")
  @Key("flexibleElementFilesListVersionDeleteForbiddenDetails")
  String flexibleElementFilesListVersionDeleteForbiddenDetails();

  /**
   * Translated "#".
   * 
   * @return translated "#"
   */
  @DefaultStringValue("#")
  @Key("flexibleElementFilesListVersionNumber")
  String flexibleElementFilesListVersionNumber();

  /**
   * Translated "Versions list".
   * 
   * @return translated "Versions list"
   */
  @DefaultStringValue("Versions list")
  @Key("flexibleElementFilesListVersionsList")
  String flexibleElementFilesListVersionsList();

  /**
   * Translated "Indicators list".
   * 
   * @return translated "Indicators list"
   */
  @DefaultStringValue("Indicators list")
  @Key("flexibleElementIndicatorsList")
  String flexibleElementIndicatorsList();

  /**
   * Translated "Add an indicator".
   * 
   * @return translated "Add an indicator"
   */
  @DefaultStringValue("Add an indicator")
  @Key("flexibleElementIndicatorsListAdd")
  String flexibleElementIndicatorsListAdd();

  /**
   * Translated "Code".
   * 
   * @return translated "Code"
   */
  @DefaultStringValue("Code")
  @Key("flexibleElementIndicatorsListCode")
  String flexibleElementIndicatorsListCode();

  /**
   * Translated "Name".
   * 
   * @return translated "Name"
   */
  @DefaultStringValue("Name")
  @Key("flexibleElementIndicatorsListName")
  String flexibleElementIndicatorsListName();

  /**
   * Translated "Remove an indicator from the list.".
   * 
   * @return translated "Remove an indicator from the list."
   */
  @DefaultStringValue("Remove an indicator from the list.")
  @Key("flexibleElementIndicatorsListRemoval")
  String flexibleElementIndicatorsListRemoval();

  /**
   * Translated "Units".
   * 
   * @return translated "Units"
   */
  @DefaultStringValue("Units")
  @Key("flexibleElementIndicatorsListUnits")
  String flexibleElementIndicatorsListUnits();

  /**
   * Translated "Message".
   * 
   * @return translated "Message"
   */
  @DefaultStringValue("Message")
  @Key("flexibleElementMessage")
  String flexibleElementMessage();

  /**
   * Translated "non-decimal".
   * 
   * @return translated "non-decimal"
   */
  @DefaultStringValue("non-decimal")
  @Key("flexibleElementNonDecimalValue")
  String flexibleElementNonDecimalValue();

  /**
   * Translated "Choice list".
   * 
   * @return translated "Choice list"
   */
  @DefaultStringValue("Choice list")
  @Key("flexibleElementQuestion")
  String flexibleElementQuestion();

  /**
   * Translated "Answer".
   * 
   * @return translated "Answer"
   */
  @DefaultStringValue("Answer")
  @Key("flexibleElementQuestionAnswer")
  String flexibleElementQuestionAnswer();

  /**
   * Translated "Select an answer...".
   * 
   * @return translated "Select an answer..."
   */
  @DefaultStringValue("Select an answer...")
  @Key("flexibleElementQuestionEmptyChoice")
  String flexibleElementQuestionEmptyChoice();

  /**
   * Translated "Select one or more answers...".
   * 
   * @return translated "Select one or more answers..."
   */
  @DefaultStringValue("Select one or more answers...")
  @Key("flexibleElementQuestionMutiple")
  String flexibleElementQuestionMutiple();

  /**
   * Translated "Report".
   * 
   * @return translated "Report"
   */
  @DefaultStringValue("Report")
  @Key("flexibleElementReport")
  String flexibleElementReport();

  /**
   * Translated "Report list".
   * 
   * @return translated "Report list"
   */
  @DefaultStringValue("Report list")
  @Key("flexibleElementReportList")
  String flexibleElementReportList();

  /**
   * Translated "Text field".
   * 
   * @return translated "Text field"
   */
  @DefaultStringValue("Text field")
  @Key("flexibleElementTextArea")
  String flexibleElementTextArea();

  /**
   * Translated "triplet".
   * 
   * @return translated "triplet"
   */
  @DefaultStringValue("triplet")
  @Key("flexibleElementTriplet")
  String flexibleElementTriplet();

  /**
   * Translated "triplets".
   * 
   * @return translated "triplets"
   */
  @DefaultStringValue("triplets")
  @Key("flexibleElementTriplets")
  String flexibleElementTriplets();

  /**
   * Translated "Triplets list".
   * 
   * @return translated "Triplets list"
   */
  @DefaultStringValue("Triplets list")
  @Key("flexibleElementTripletsList")
  String flexibleElementTripletsList();

  /**
   * Translated "Code".
   * 
   * @return translated "Code"
   */
  @DefaultStringValue("Code")
  @Key("flexibleElementTripletsListCode")
  String flexibleElementTripletsListCode();

  /**
   * Translated "Name".
   * 
   * @return translated "Name"
   */
  @DefaultStringValue("Name")
  @Key("flexibleElementTripletsListName")
  String flexibleElementTripletsListName();

  /**
   * Translated "Period".
   * 
   * @return translated "Period"
   */
  @DefaultStringValue("Period")
  @Key("flexibleElementTripletsListPeriod")
  String flexibleElementTripletsListPeriod();

  /**
   * Translated "Font".
   * 
   * @return translated "Font"
   */
  @DefaultStringValue("Font")
  @Key("font")
  String font();

  /**
   * Translated "Delete".
   * 
   * @return translated "Delete"
   */
  @DefaultStringValue("Delete")
  @Key("formWindowDeleteAction")
  String formWindowDeleteAction();

  /**
   * Translated "Some fields aren't filled".
   * 
   * @return translated "Some fields aren't filled"
   */
  @DefaultStringValue("Some fields aren't filled")
  @Key("formWindowFieldsUnfilled")
  String formWindowFieldsUnfilled();

  /**
   * Translated "Please, fill all the required fields.".
   * 
   * @return translated "Please, fill all the required fields."
   */
  @DefaultStringValue("Please, fill all the required fields.")
  @Key("formWindowFieldsUnfilledDetails")
  String formWindowFieldsUnfilledDetails();

  /**
   * Translated "Select an element...".
   * 
   * @return translated "Select an element..."
   */
  @DefaultStringValue("Select an element...")
  @Key("formWindowListEmptyText")
  String formWindowListEmptyText();

  /**
   * Translated "Save".
   * 
   * @return translated "Save"
   */
  @DefaultStringValue("Save")
  @Key("formWindowSubmitAction")
  String formWindowSubmitAction();

  /**
   * Translated "from".
   * 
   * @return translated "from"
   */
  @DefaultStringValue("from")
  @Key("fromDate")
  String fromDate();

  /**
   * Translated "Full Name".
   * 
   * @return translated "Full Name"
   */
  @DefaultStringValue("Full Name")
  @Key("fullName")
  String fullName();

  /**
   * Translated "In order to use ActivityInfo offline, you must first install <b>Google Gears</b>. Click <a href=\"http://tools.google.com/gears/\" target=\"_blank\">here</a> to install.".
   * 
   * @return translated "In order to use ActivityInfo offline, you must first install <b>Google Gears</b>. Click <a href=\"http://tools.google.com/gears/\" target=\"_blank\">here</a> to install."
   */
  @DefaultStringValue("In order to use ActivityInfo offline, you must first install <b>Google Gears</b>. Click <a href=\"http://tools.google.com/gears/\" target=\"_blank\">here</a> to install.")
  @Key("gearsRequired")
  String gearsRequired();

  /**
   * Translated "Geographic Position".
   * 
   * @return translated "Geographic Position"
   */
  @DefaultStringValue("Geographic Position")
  @Key("geoPosition")
  String geoPosition();

  /**
   * Translated "Geography".
   * 
   * @return translated "Geography"
   */
  @DefaultStringValue("Geography")
  @Key("geography")
  String geography();

  /**
   * Translated "Global export".
   * 
   * @return translated "Global export"
   */
  @DefaultStringValue("Global export")
  @Key("globalExport")
  String globalExport();

  /**
   * Translated "Global export configuration".
   * 
   * @return translated "Global export configuration"
   */
  @DefaultStringValue("Global export configuration")
  @Key("globalExportConfiguration")
  String globalExportConfiguration();

  /**
   * Translated "Go to indicators list".
   * 
   * @return translated "Go to indicators list"
   */
  @DefaultStringValue("Go to indicators list")
  @Key("goToIndicatorsList")
  String goToIndicatorsList();

  /**
   * Translated "Group".
   * 
   * @return translated "Group"
   */
  @DefaultStringValue("Group")
  @Key("group")
  String group();

  /**
   * Translated "Half PowerPoint Slide".
   * 
   * @return translated "Half PowerPoint Slide"
   */
  @DefaultStringValue("Half PowerPoint Slide")
  @Key("halfSlidePowerPoint")
  String halfSlidePowerPoint();

  /**
   * Translated "Help".
   * 
   * @return translated "Help"
   */
  @DefaultStringValue("Help")
  @Key("help")
  String help();

  /**
   * Translated "History".
   * 
   * @return translated "History"
   */
  @DefaultStringValue("History")
  @Key("history")
  String history();

  /**
   * Translated "Add".
   * 
   * @return translated "Add"
   */
  @DefaultStringValue("Add")
  @Key("historyAdd")
  String historyAdd();

  /**
   * Translated "Date".
   * 
   * @return translated "Date"
   */
  @DefaultStringValue("Date")
  @Key("historyDate")
  String historyDate();

  /**
   * Translated "Edit".
   * 
   * @return translated "Edit"
   */
  @DefaultStringValue("Edit")
  @Key("historyEdit")
  String historyEdit();

  /**
   * Translated "&lt;empty&gt;".
   * 
   * @return translated "&lt;empty&gt;"
   */
  @DefaultStringValue("&lt;empty&gt;")
  @Key("historyEmptyString")
  String historyEmptyString();

  /**
   * Translated "History error".
   * 
   * @return translated "History error"
   */
  @DefaultStringValue("History error")
  @Key("historyError")
  String historyError();

  /**
   * Translated "An error occurred while fetching of the history.".
   * 
   * @return translated "An error occurred while fetching of the history."
   */
  @DefaultStringValue("An error occurred while fetching of the history.")
  @Key("historyErrorDetails")
  String historyErrorDetails();

  /**
   * Translated "modification".
   * 
   * @return translated "modification"
   */
  @DefaultStringValue("modification")
  @Key("historyModification")
  String historyModification();

  /**
   * Translated "Modification type".
   * 
   * @return translated "Modification type"
   */
  @DefaultStringValue("Modification type")
  @Key("historyModificationType")
  String historyModificationType();

  /**
   * Translated "modifications".
   * 
   * @return translated "modifications"
   */
  @DefaultStringValue("modifications")
  @Key("historyModifications")
  String historyModifications();

  /**
   * Translated "This element doesn't have a history.".
   * 
   * @return translated "This element doesn't have a history."
   */
  @DefaultStringValue("This element doesn't have a history.")
  @Key("historyNoHistory")
  String historyNoHistory();

  /**
   * Translated "Remove".
   * 
   * @return translated "Remove"
   */
  @DefaultStringValue("Remove")
  @Key("historyRemove")
  String historyRemove();

  /**
   * Translated "Show history".
   * 
   * @return translated "Show history"
   */
  @DefaultStringValue("Show history")
  @Key("historyShow")
  String historyShow();

  /**
   * Translated "User".
   * 
   * @return translated "User"
   */
  @DefaultStringValue("User")
  @Key("historyUser")
  String historyUser();

  /**
   * Translated "Value".
   * 
   * @return translated "Value"
   */
  @DefaultStringValue("Value")
  @Key("historyValue")
  String historyValue();

  /**
   * Translated "Image".
   * 
   * @return translated "Image"
   */
  @DefaultStringValue("Image")
  @Key("image")
  String image();

  /**
   * Translated "Confirm importation details".
   * 
   * @return translated "Confirm importation details"
   */
  @DefaultStringValue("Confirm importation details")
  @Key("importButtonConfirmDetails")
  String importButtonConfirmDetails();

  /**
   * Translated "Unlock project core".
   * 
   * @return translated "Unlock project core"
   */
  @DefaultStringValue("Unlock project core")
  @Key("importButtonUnlock")
  String importButtonUnlock();

  /**
   * Translated "No project/organisational unit has been imported".
   * 
   * @return translated "No project/organisational unit has been imported"
   */
  @DefaultStringValue("No project/organisational unit has been imported")
  @Key("importDetailsWindowSelectionEmpty")
  String importDetailsWindowSelectionEmpty();

  /**
   * Translated "The extracted value is not allowed for the field".
   * 
   * @return translated "The extracted value is not allowed for the field"
   */
  @DefaultStringValue("The extracted value is not allowed for the field")
  @Key("importElementExtractedValueStatusForbiddenValue")
  String importElementExtractedValueStatusForbiddenValue();

  /**
   * Translated "The extracted value is not a valid date".
   * 
   * @return translated "The extracted value is not a valid date"
   */
  @DefaultStringValue("The extracted value is not a valid date")
  @Key("importElementExtractedValueStatusInvalidDateValue")
  String importElementExtractedValueStatusInvalidDateValue();

  /**
   * Translated "The extracted value is not a valid number".
   * 
   * @return translated "The extracted value is not a valid number"
   */
  @DefaultStringValue("The extracted value is not a valid number")
  @Key("importElementExtractedValueStatusInvalidNumberValue")
  String importElementExtractedValueStatusInvalidNumberValue();

  /**
   * Translated "The extracted value is not a valid question value".
   * 
   * @return translated "The extracted value is not a valid question value"
   */
  @DefaultStringValue("The extracted value is not a valid question value")
  @Key("importElementExtractedValueStatusInvalidQuestionValue")
  String importElementExtractedValueStatusInvalidQuestionValue();

  /**
   * Translated "The extracted value is not a valid triplet value".
   * 
   * @return translated "The extracted value is not a valid triplet value"
   */
  @DefaultStringValue("The extracted value is not a valid triplet value")
  @Key("importElementExtractedValueStatusInvalidTripletValue")
  String importElementExtractedValueStatusInvalidTripletValue();

  /**
   * Translated "This field is not importable".
   * 
   * @return translated "This field is not importable"
   */
  @DefaultStringValue("This field is not importable")
  @Key("importElementExtractedValueStatusNotImportable")
  String importElementExtractedValueStatusNotImportable();

  /**
   * Translated "There was no corresponding projects or organisational units models (check that they each have an identification key)".
   * 
   * @return translated "There was no corresponding projects or organisational units models (check that they each have an identification key)"
   */
  @DefaultStringValue("There was no corresponding projects or organisational units models (check that they each have an identification key)")
  @Key("importEntitesEmpty")
  String importEntitesEmpty();

  /**
   * Translated "Import status".
   * 
   * @return translated "Import status"
   */
  @DefaultStringValue("Import status")
  @Key("importHeadingStatus")
  String importHeadingStatus();

  /**
   * Translated "Import".
   * 
   * @return translated "Import"
   */
  @DefaultStringValue("Import")
  @Key("importItem")
  String importItem();

  /**
   * Translated "Organisational unit found and importation ready".
   * 
   * @return translated "Organisational unit found and importation ready"
   */
  @DefaultStringValue("Organisational unit found and importation ready")
  @Key("importOrgUnitFound")
  String importOrgUnitFound();

  /**
   * Translated "Organisational unit not found".
   * 
   * @return translated "Organisational unit not found"
   */
  @DefaultStringValue("Organisational unit not found")
  @Key("importOrgUnitNotFound")
  String importOrgUnitNotFound();

  /**
   * Translated "Project found but importation impossible because modifications required on a locked project core".
   * 
   * @return translated "Project found but importation impossible because modifications required on a locked project core"
   */
  @DefaultStringValue("Project found but importation impossible because modifications required on a locked project core")
  @Key("importProjectCoreLocked")
  String importProjectCoreLocked();

  /**
   * Translated "Project found and importation ready".
   * 
   * @return translated "Project found and importation ready"
   */
  @DefaultStringValue("Project found and importation ready")
  @Key("importProjectFound")
  String importProjectFound();

  /**
   * Translated "Project not found".
   * 
   * @return translated "Project not found"
   */
  @DefaultStringValue("Project not found")
  @Key("importProjectNotFound")
  String importProjectNotFound();

  /**
   * Translated "List of projects/organisational units modified by this importation".
   * 
   * @return translated "List of projects/organisational units modified by this importation"
   */
  @DefaultStringValue("List of projects/organisational units modified by this importation")
  @Key("importProjectOrgUnitsPanelHeader")
  String importProjectOrgUnitsPanelHeader();

  /**
   * Translated "Importation validation".
   * 
   * @return translated "Importation validation"
   */
  @DefaultStringValue("Importation validation")
  @Key("importProjectOrgUnitsWindowTitle")
  String importProjectOrgUnitsWindowTitle();

  /**
   * Translated "File Format".
   * 
   * @return translated "File Format"
   */
  @DefaultStringValue("File Format")
  @Key("importSchemeFileFormat")
  String importSchemeFileFormat();

  /**
   * Translated "Name".
   * 
   * @return translated "Name"
   */
  @DefaultStringValue("Name")
  @Key("importSchemeName")
  String importSchemeName();

  /**
   * Translated "Several organisational units found: a single choice should be made to proceed with importation".
   * 
   * @return translated "Several organisational units found: a single choice should be made to proceed with importation"
   */
  @DefaultStringValue("Several organisational units found: a single choice should be made to proceed with importation")
  @Key("importSeveralOrgUnitsFound")
  String importSeveralOrgUnitsFound();

  /**
   * Translated "Several projects found: a single choice should be made to proceed with importation".
   * 
   * @return translated "Several projects found: a single choice should be made to proceed with importation"
   */
  @DefaultStringValue("Several projects found: a single choice should be made to proceed with importation")
  @Key("importSeveralProjectsFound")
  String importSeveralProjectsFound();

  /**
   * Translated "By Row".
   * 
   * @return translated "By Row"
   */
  @DefaultStringValue("By Row")
  @Key("importTypeRow")
  String importTypeRow();

  /**
   * Translated "Several".
   * 
   * @return translated "Several"
   */
  @DefaultStringValue("Several")
  @Key("importTypeSeveral")
  String importTypeSeveral();

  /**
   * Translated "Unique".
   * 
   * @return translated "Unique"
   */
  @DefaultStringValue("Unique")
  @Key("importTypeUnique")
  String importTypeUnique();

  /**
   * Translated "Name".
   * 
   * @return translated "Name"
   */
  @DefaultStringValue("Name")
  @Key("importVariableName")
  String importVariableName();

  /**
   * Translated "Reference".
   * 
   * @return translated "Reference"
   */
  @DefaultStringValue("Reference")
  @Key("importVariableReference")
  String importVariableReference();

  /**
   * Translated "Tasks".
   * 
   * @return translated "Tasks"
   */
  @DefaultStringValue("Tasks")
  @Key("importantPoints")
  String importantPoints();

  /**
   * Translated "In Progress".
   * 
   * @return translated "In Progress"
   */
  @DefaultStringValue("In Progress")
  @Key("inProgress")
  String inProgress();

  /**
   * Translated "A new version of the software has been posted to the server. You will need to refresh before continuing. ".
   * 
   * @return translated "A new version of the software has been posted to the server. You will need to refresh before continuing. "
   */
  @DefaultStringValue("A new version of the software has been posted to the server. You will need to refresh before continuing. ")
  @Key("incompatibleRemoteServiceException")
  String incompatibleRemoteServiceException();

  /**
   * Translated "The completion date must be after the start date.".
   * 
   * @return translated "The completion date must be after the start date."
   */
  @DefaultStringValue("The completion date must be after the start date.")
  @Key("inconsistentDateRangeWarning")
  String inconsistentDateRangeWarning();

  /**
   * Translated "Indicator".
   * 
   * @return translated "Indicator"
   */
  @DefaultStringValue("Indicator")
  @Key("indicator")
  String indicator();

  /**
   * Translated "Code".
   * 
   * @return translated "Code"
   */
  @DefaultStringValue("Code")
  @Key("indicatorCode")
  String indicatorCode();

  /**
   * Translated "Comments".
   * 
   * @return translated "Comments"
   */
  @DefaultStringValue("Comments")
  @Key("indicatorComments")
  String indicatorComments();

  /**
   * Translated "Other indicators?".
   * 
   * @return translated "Other indicators?"
   */
  @DefaultStringValue("Other indicators?")
  @Key("indicatorDataSourceOther")
  String indicatorDataSourceOther();

  /**
   * Translated "Indicator data sources".
   * 
   * @return translated "Indicator data sources"
   */
  @DefaultStringValue("Indicator data sources")
  @Key("indicatorDataSources")
  String indicatorDataSources();

  /**
   * Translated "Direct data entry for this indicator is disabled; you can enter results in this indicator's data sources".
   * 
   * @return translated "Direct data entry for this indicator is disabled; you can enter results in this indicator's data sources"
   */
  @DefaultStringValue("Direct data entry for this indicator is disabled; you can enter results in this indicator's data sources")
  @Key("indicatorDirectEntry")
  String indicatorDirectEntry();

  /**
   * Translated "Indicator values for".
   * 
   * @return translated "Indicator values for"
   */
  @DefaultStringValue("Indicator values for")
  @Key("indicatorFilterToolBarLabel")
  String indicatorFilterToolBarLabel();

  /**
   * Translated "Indicators".
   * 
   * @return translated "Indicators"
   */
  @DefaultStringValue("Indicators")
  @Key("indicators")
  String indicators();

  /**
   * Translated "Confirmation".
   * 
   * @return translated "Confirmation"
   */
  @DefaultStringValue("Confirmation")
  @Key("infoConfirmation")
  String infoConfirmation();

  /**
   * Translated "Install offline mode".
   * 
   * @return translated "Install offline mode"
   */
  @DefaultStringValue("Install offline mode")
  @Key("installOffline")
  String installOffline();

  /**
   * Translated "Installing...".
   * 
   * @return translated "Installing..."
   */
  @DefaultStringValue("Installing...")
  @Key("installingOffline")
  String installingOffline();

  /**
   * Translated "Invalid coordinate".
   * 
   * @return translated "Invalid coordinate"
   */
  @DefaultStringValue("Invalid coordinate")
  @Key("invalidCoordinate")
  String invalidCoordinate();

  /**
   * Translated "Invalid Link".
   * 
   * @return translated "Invalid Link"
   */
  @DefaultStringValue("Invalid Link")
  @Key("invalidLink")
  String invalidLink();

  /**
   * Translated "The link that you followed was expired or invalid or incomplete. Try copying and pasting the full link into the address bar. Alternatively, try sending another request by visiting a login page.".
   * 
   * @return translated "The link that you followed was expired or invalid or incomplete. Try copying and pasting the full link into the address bar. Alternatively, try sending another request by visiting a login page."
   */
  @DefaultStringValue("The link that you followed was expired or invalid or incomplete. Try copying and pasting the full link into the address bar. Alternatively, try sending another request by visiting a login page.")
  @Key("invalidLinkDetail")
  String invalidLinkDetail();

  /**
   * Translated "Minutes must be between 0-59.9".
   * 
   * @return translated "Minutes must be between 0-59.9"
   */
  @DefaultStringValue("Minutes must be between 0-59.9")
  @Key("invalidMinutes")
  String invalidMinutes();

  /**
   * Translated "Seconds must be between 0-59.9".
   * 
   * @return translated "Seconds must be between 0-59.9"
   */
  @DefaultStringValue("Seconds must be between 0-59.9")
  @Key("invalidSeconds")
  String invalidSeconds();

  /**
   * Translated "Is Assessment".
   * 
   * @return translated "Is Assessment"
   */
  @DefaultStringValue("Is Assessment")
  @Key("isAssessment")
  String isAssessment();

  /**
   * Translated "Check this box if this activity is an assessment activity".
   * 
   * @return translated "Check this box if this activity is an assessment activity"
   */
  @DefaultStringValue("Check this box if this activity is an assessment activity")
  @Key("isAssessmentToolTip")
  String isAssessmentToolTip();

  /**
   * Translated "English".
   * 
   * @return translated "English"
   */
  @DefaultStringValue("English")
  @Key("languageEnglish")
  String languageEnglish();

  /**
   * Translated "Français".
   * 
   * @return translated "Français"
   */
  @DefaultStringValue("Français")
  @Key("languageFrench")
  String languageFrench();

  /**
   * Translated "Español".
   * 
   * @return translated "Español"
   */
  @DefaultStringValue("Español")
  @Key("languageSpanish")
  String languageSpanish();

  /**
   * Translated "Lat:".
   * 
   * @return translated "Lat:"
   */
  @DefaultStringValue("Lat:")
  @Key("lat")
  String lat();

  /**
   * Translated "Latitude".
   * 
   * @return translated "Latitude"
   */
  @DefaultStringValue("Latitude")
  @Key("latitude")
  String latitude();

  /**
   * Translated "Legend".
   * 
   * @return translated "Legend"
   */
  @DefaultStringValue("Legend")
  @Key("legend")
  String legend();

  /**
   * Translated "Link the copied indicators to the source logframe.".
   * 
   * @return translated "Link the copied indicators to the source logframe."
   */
  @DefaultStringValue("Link the copied indicators to the source logframe.")
  @Key("linkIndicators")
  String linkIndicators();

  /**
   * Translated "This logframe will include the results from the source project.".
   * 
   * @return translated "This logframe will include the results from the source project."
   */
  @DefaultStringValue("This logframe will include the results from the source project.")
  @Key("linkIndicatorsExplanation")
  String linkIndicatorsExplanation();

  /**
   * Translated "The projects have been correctly modified.".
   * 
   * @return translated "The projects have been correctly modified."
   */
  @DefaultStringValue("The projects have been correctly modified.")
  @Key("linkedProjectUpdateConfirm")
  String linkedProjectUpdateConfirm();

  /**
   * Translated "Modification error.".
   * 
   * @return translated "Modification error."
   */
  @DefaultStringValue("Modification error.")
  @Key("linkedProjectUpdateError")
  String linkedProjectUpdateError();

  /**
   * Translated "An error occurred during the modification of the inked projects.".
   * 
   * @return translated "An error occurred during the modification of the inked projects."
   */
  @DefaultStringValue("An error occurred during the modification of the inked projects.")
  @Key("linkedProjectUpdateErrorDetails")
  String linkedProjectUpdateErrorDetails();

  /**
   * Translated "List Header".
   * 
   * @return translated "List Header"
   */
  @DefaultStringValue("List Header")
  @Key("listHeader")
  String listHeader();

  /**
   * Translated "Activity Site Lists".
   * 
   * @return translated "Activity Site Lists"
   */
  @DefaultStringValue("Activity Site Lists")
  @Key("listsOfActivities")
  String listsOfActivities();

  /**
   * Translated "Live data".
   * 
   * @return translated "Live data"
   */
  @DefaultStringValue("Live data")
  @Key("liveData")
  String liveData();

  /**
   * Translated "Lng:".
   * 
   * @return translated "Lng:"
   */
  @DefaultStringValue("Lng:")
  @Key("lng")
  String lng();

  /**
   * Translated "Loading...".
   * 
   * @return translated "Loading..."
   */
  @DefaultStringValue("Loading...")
  @Key("loading")
  String loading();

  /**
   * Translated "Loading application...".
   * 
   * @return translated "Loading application..."
   */
  @DefaultStringValue("Loading application...")
  @Key("loadingApp")
  String loadingApp();

  /**
   * Translated "Loading Component...".
   * 
   * @return translated "Loading Component..."
   */
  @DefaultStringValue("Loading Component...")
  @Key("loadingComponent")
  String loadingComponent();

  /**
   * Translated "Loading Databases".
   * 
   * @return translated "Loading Databases"
   */
  @DefaultStringValue("Loading Databases")
  @Key("loadingDatabases")
  String loadingDatabases();

  /**
   * Translated "Deleting draft project ...".
   * 
   * @return translated "Deleting draft project ..."
   */
  @DefaultStringValue("Deleting draft project ...")
  @Key("loadingDeleteProject")
  String loadingDeleteProject();

  /**
   * Translated "Loading Google Maps...".
   * 
   * @return translated "Loading Google Maps..."
   */
  @DefaultStringValue("Loading Google Maps...")
  @Key("loadingGoogleMaps")
  String loadingGoogleMaps();

  /**
   * Translated "Loading maps...".
   * 
   * @return translated "Loading maps..."
   */
  @DefaultStringValue("Loading maps...")
  @Key("loadingMap")
  String loadingMap();

  /**
   * Translated "Loading the offline module...".
   * 
   * @return translated "Loading the offline module..."
   */
  @DefaultStringValue("Loading the offline module...")
  @Key("loadingOfflineMode")
  String loadingOfflineMode();

  /**
   * Translated "Loading database definitions".
   * 
   * @return translated "Loading database definitions"
   */
  @DefaultStringValue("Loading database definitions")
  @Key("loadingPrograms")
  String loadingPrograms();

  /**
   * Translated "Language".
   * 
   * @return translated "Language"
   */
  @DefaultStringValue("Language")
  @Key("locale")
  String locale();

  /**
   * Translated "Location".
   * 
   * @return translated "Location"
   */
  @DefaultStringValue("Location")
  @Key("location")
  String location();

  /**
   * Translated "Location Type ".
   * 
   * @return translated "Location Type "
   */
  @DefaultStringValue("Location Type ")
  @Key("locationType")
  String locationType();

  /**
   * Translated "Logical framework".
   * 
   * @return translated "Logical framework"
   */
  @DefaultStringValue("Logical framework")
  @Key("logFrame")
  String logFrame();

  /**
   * Translated "Delete".
   * 
   * @return translated "Delete"
   */
  @DefaultStringValue("Delete")
  @Key("logFrameActionDelete")
  String logFrameActionDelete();

  /**
   * Translated "This element cannot be deleted because it has sub-elements.".
   * 
   * @return translated "This element cannot be deleted because it has sub-elements."
   */
  @DefaultStringValue("This element cannot be deleted because it has sub-elements.")
  @Key("logFrameActionDeleteUnavailable")
  String logFrameActionDeleteUnavailable();

  /**
   * Translated "Down".
   * 
   * @return translated "Down"
   */
  @DefaultStringValue("Down")
  @Key("logFrameActionDown")
  String logFrameActionDown();

  /**
   * Translated "This element cannot be moved down.".
   * 
   * @return translated "This element cannot be moved down."
   */
  @DefaultStringValue("This element cannot be moved down.")
  @Key("logFrameActionDownUnavailable")
  String logFrameActionDownUnavailable();

  /**
   * Translated "Remove a group".
   * 
   * @return translated "Remove a group"
   */
  @DefaultStringValue("Remove a group")
  @Key("logFrameActionRemove")
  String logFrameActionRemove();

  /**
   * Translated "Rename".
   * 
   * @return translated "Rename"
   */
  @DefaultStringValue("Rename")
  @Key("logFrameActionRename")
  String logFrameActionRename();

  /**
   * Translated "Rename a group".
   * 
   * @return translated "Rename a group"
   */
  @DefaultStringValue("Rename a group")
  @Key("logFrameActionRenameGroup")
  String logFrameActionRenameGroup();

  /**
   * Translated "New name".
   * 
   * @return translated "New name"
   */
  @DefaultStringValue("New name")
  @Key("logFrameActionRenameNewName")
  String logFrameActionRenameNewName();

  /**
   * Translated "This element cannot be renamed.".
   * 
   * @return translated "This element cannot be renamed."
   */
  @DefaultStringValue("This element cannot be renamed.")
  @Key("logFrameActionRenameUnavailable")
  String logFrameActionRenameUnavailable();

  /**
   * Translated "Title of the action".
   * 
   * @return translated "Title of the action"
   */
  @DefaultStringValue("Title of the action")
  @Key("logFrameActionTitle")
  String logFrameActionTitle();

  /**
   * Translated "Up".
   * 
   * @return translated "Up"
   */
  @DefaultStringValue("Up")
  @Key("logFrameActionUp")
  String logFrameActionUp();

  /**
   * Translated "This element cannot be moved up.".
   * 
   * @return translated "This element cannot be moved up."
   */
  @DefaultStringValue("This element cannot be moved up.")
  @Key("logFrameActionUpUnavailable")
  String logFrameActionUpUnavailable();

  /**
   * Translated "Activities".
   * 
   * @return translated "Activities"
   */
  @DefaultStringValue("Activities")
  @Key("logFrameActivities")
  String logFrameActivities();

  /**
   * Translated "A".
   * 
   * @return translated "A"
   */
  @DefaultStringValue("A")
  @Key("logFrameActivitiesCode")
  String logFrameActivitiesCode();

  /**
   * Translated "Activity".
   * 
   * @return translated "Activity"
   */
  @DefaultStringValue("Activity")
  @Key("logFrameActivity")
  String logFrameActivity();

  /**
   * Translated "Advancement".
   * 
   * @return translated "Advancement"
   */
  @DefaultStringValue("Advancement")
  @Key("logFrameActivityAdvancement")
  String logFrameActivityAdvancement();

  /**
   * Translated "Content".
   * 
   * @return translated "Content"
   */
  @DefaultStringValue("Content")
  @Key("logFrameActivityContent")
  String logFrameActivityContent();

  /**
   * Translated "End date".
   * 
   * @return translated "End date"
   */
  @DefaultStringValue("End date")
  @Key("logFrameActivityEndDate")
  String logFrameActivityEndDate();

  /**
   * Translated "Start date".
   * 
   * @return translated "Start date"
   */
  @DefaultStringValue("Start date")
  @Key("logFrameActivityStartDate")
  String logFrameActivityStartDate();

  /**
   * Translated "Title".
   * 
   * @return translated "Title"
   */
  @DefaultStringValue("Title")
  @Key("logFrameActivityTitle")
  String logFrameActivityTitle();

  /**
   * Translated "Add an activity".
   * 
   * @return translated "Add an activity"
   */
  @DefaultStringValue("Add an activity")
  @Key("logFrameAddA")
  String logFrameAddA();

  /**
   * Translated "Add an expected result".
   * 
   * @return translated "Add an expected result"
   */
  @DefaultStringValue("Add an expected result")
  @Key("logFrameAddER")
  String logFrameAddER();

  /**
   * Translated "Add a group".
   * 
   * @return translated "Add a group"
   */
  @DefaultStringValue("Add a group")
  @Key("logFrameAddGroup")
  String logFrameAddGroup();

  /**
   * Translated "Type the name of the group to add to the activities.".
   * 
   * @return translated "Type the name of the group to add to the activities."
   */
  @DefaultStringValue("Type the name of the group to add to the activities.")
  @Key("logFrameAddGroupToA")
  String logFrameAddGroupToA();

  /**
   * Translated "Type the name of the group to add to the expected results.".
   * 
   * @return translated "Type the name of the group to add to the expected results."
   */
  @DefaultStringValue("Type the name of the group to add to the expected results.")
  @Key("logFrameAddGroupToER")
  String logFrameAddGroupToER();

  /**
   * Translated "Type the name of the group to add to the specific objectives.".
   * 
   * @return translated "Type the name of the group to add to the specific objectives."
   */
  @DefaultStringValue("Type the name of the group to add to the specific objectives.")
  @Key("logFrameAddGroupToOS")
  String logFrameAddGroupToOS();

  /**
   * Translated "Type the name of the group to add to the prerequisites.".
   * 
   * @return translated "Type the name of the group to add to the prerequisites."
   */
  @DefaultStringValue("Type the name of the group to add to the prerequisites.")
  @Key("logFrameAddGroupToP")
  String logFrameAddGroupToP();

  /**
   * Translated "Add a specific objective".
   * 
   * @return translated "Add a specific objective"
   */
  @DefaultStringValue("Add a specific objective")
  @Key("logFrameAddOS")
  String logFrameAddOS();

  /**
   * Translated "Add a prerequisite".
   * 
   * @return translated "Add a prerequisite"
   */
  @DefaultStringValue("Add a prerequisite")
  @Key("logFrameAddP")
  String logFrameAddP();

  /**
   * Translated "Add".
   * 
   * @return translated "Add"
   */
  @DefaultStringValue("Add")
  @Key("logFrameAddRow")
  String logFrameAddRow();

  /**
   * Translated "Logical framework copied.".
   * 
   * @return translated "Logical framework copied."
   */
  @DefaultStringValue("Logical framework copied.")
  @Key("logFrameCopied")
  String logFrameCopied();

  /**
   * Translated "Excepted result".
   * 
   * @return translated "Excepted result"
   */
  @DefaultStringValue("Excepted result")
  @Key("logFrameExceptedResult")
  String logFrameExceptedResult();

  /**
   * Translated "Excepted results".
   * 
   * @return translated "Excepted results"
   */
  @DefaultStringValue("Excepted results")
  @Key("logFrameExceptedResults")
  String logFrameExceptedResults();

  /**
   * Translated "ER".
   * 
   * @return translated "ER"
   */
  @DefaultStringValue("ER")
  @Key("logFrameExceptedResultsCode")
  String logFrameExceptedResultsCode();

  /**
   * Translated "Group".
   * 
   * @return translated "Group"
   */
  @DefaultStringValue("Group")
  @Key("logFrameGroup")
  String logFrameGroup();

  /**
   * Translated "This kind of logical frameworks doesn't allows groups creation for the activities.".
   * 
   * @return translated "This kind of logical frameworks doesn't allows groups creation for the activities."
   */
  @DefaultStringValue("This kind of logical frameworks doesn't allows groups creation for the activities.")
  @Key("logFrameGroupsDisabledA")
  String logFrameGroupsDisabledA();

  /**
   * Translated "This kind of logical frameworks doesn't allows groups creation for the expected results.".
   * 
   * @return translated "This kind of logical frameworks doesn't allows groups creation for the expected results."
   */
  @DefaultStringValue("This kind of logical frameworks doesn't allows groups creation for the expected results.")
  @Key("logFrameGroupsDisabledER")
  String logFrameGroupsDisabledER();

  /**
   * Translated "This kind of logical frameworks doesn't allows groups creation for the specific objectives.".
   * 
   * @return translated "This kind of logical frameworks doesn't allows groups creation for the specific objectives."
   */
  @DefaultStringValue("This kind of logical frameworks doesn't allows groups creation for the specific objectives.")
  @Key("logFrameGroupsDisabledOS")
  String logFrameGroupsDisabledOS();

  /**
   * Translated "This kind of logical frameworks doesn't allows groups creation for the prerequisites.".
   * 
   * @return translated "This kind of logical frameworks doesn't allows groups creation for the prerequisites."
   */
  @DefaultStringValue("This kind of logical frameworks doesn't allows groups creation for the prerequisites.")
  @Key("logFrameGroupsDisabledP")
  String logFrameGroupsDisabledP();

  /**
   * Translated "The maximum number of groups for the activities of this kind of logical frameworks is reached.".
   * 
   * @return translated "The maximum number of groups for the activities of this kind of logical frameworks is reached."
   */
  @DefaultStringValue("The maximum number of groups for the activities of this kind of logical frameworks is reached.")
  @Key("logFrameGroupsMaxReachedA")
  String logFrameGroupsMaxReachedA();

  /**
   * Translated "The maximum number of groups for the expected results of this kind of logical frameworks is reached.".
   * 
   * @return translated "The maximum number of groups for the expected results of this kind of logical frameworks is reached."
   */
  @DefaultStringValue("The maximum number of groups for the expected results of this kind of logical frameworks is reached.")
  @Key("logFrameGroupsMaxReachedER")
  String logFrameGroupsMaxReachedER();

  /**
   * Translated "The maximum number of groups for the specific objectives of this kind of logical frameworks is reached.".
   * 
   * @return translated "The maximum number of groups for the specific objectives of this kind of logical frameworks is reached."
   */
  @DefaultStringValue("The maximum number of groups for the specific objectives of this kind of logical frameworks is reached.")
  @Key("logFrameGroupsMaxReachedOS")
  String logFrameGroupsMaxReachedOS();

  /**
   * Translated "The maximum number of groups for the prerequisites of this kind of logical frameworks is reached.".
   * 
   * @return translated "The maximum number of groups for the prerequisites of this kind of logical frameworks is reached."
   */
  @DefaultStringValue("The maximum number of groups for the prerequisites of this kind of logical frameworks is reached.")
  @Key("logFrameGroupsMaxReachedP")
  String logFrameGroupsMaxReachedP();

  /**
   * Translated "Intervention logic".
   * 
   * @return translated "Intervention logic"
   */
  @DefaultStringValue("Intervention logic")
  @Key("logFrameInterventionLogic")
  String logFrameInterventionLogic();

  /**
   * Translated "Main objective".
   * 
   * @return translated "Main objective"
   */
  @DefaultStringValue("Main objective")
  @Key("logFrameMainObjective")
  String logFrameMainObjective();

  /**
   * Translated "The maximum number of activities for this kind of logical frameworks is reached.".
   * 
   * @return translated "The maximum number of activities for this kind of logical frameworks is reached."
   */
  @DefaultStringValue("The maximum number of activities for this kind of logical frameworks is reached.")
  @Key("logFrameMaxReachedA")
  String logFrameMaxReachedA();

  /**
   * Translated "The maximum number of expected results for this kind of logical frameworks is reached.".
   * 
   * @return translated "The maximum number of expected results for this kind of logical frameworks is reached."
   */
  @DefaultStringValue("The maximum number of expected results for this kind of logical frameworks is reached.")
  @Key("logFrameMaxReachedER")
  String logFrameMaxReachedER();

  /**
   * Translated "The maximum number of specific objectives for this kind of logical frameworks is reached.".
   * 
   * @return translated "The maximum number of specific objectives for this kind of logical frameworks is reached."
   */
  @DefaultStringValue("The maximum number of specific objectives for this kind of logical frameworks is reached.")
  @Key("logFrameMaxReachedOS")
  String logFrameMaxReachedOS();

  /**
   * Translated "The maximum number of prerequisites for this kind of logical frameworks is reached.".
   * 
   * @return translated "The maximum number of prerequisites for this kind of logical frameworks is reached."
   */
  @DefaultStringValue("The maximum number of prerequisites for this kind of logical frameworks is reached.")
  @Key("logFrameMaxReachedP")
  String logFrameMaxReachedP();

  /**
   * Translated "Means of verification".
   * 
   * @return translated "Means of verification"
   */
  @DefaultStringValue("Means of verification")
  @Key("logFrameMeansOfVerification")
  String logFrameMeansOfVerification();

  /**
   * Translated "Edit an activity".
   * 
   * @return translated "Edit an activity"
   */
  @DefaultStringValue("Edit an activity")
  @Key("logFrameModifyA")
  String logFrameModifyA();

  /**
   * Translated "No expected result".
   * 
   * @return translated "No expected result"
   */
  @DefaultStringValue("No expected result")
  @Key("logFrameNoExceptedResults")
  String logFrameNoExceptedResults();

  /**
   * Translated "This logical framework does not contain already an expected result. Please, create one before adding some activities.".
   * 
   * @return translated "This logical framework does not contain already an expected result. Please, create one before adding some activities."
   */
  @DefaultStringValue("This logical framework does not contain already an expected result. Please, create one before adding some activities.")
  @Key("logFrameNoExceptedResultsDetails")
  String logFrameNoExceptedResultsDetails();

  /**
   * Translated "No specific objective".
   * 
   * @return translated "No specific objective"
   */
  @DefaultStringValue("No specific objective")
  @Key("logFrameNoSpecificObjective")
  String logFrameNoSpecificObjective();

  /**
   * Translated "This logical framework does not contain already a specific objective. Please, create one before adding some expected results.".
   * 
   * @return translated "This logical framework does not contain already a specific objective. Please, create one before adding some expected results."
   */
  @DefaultStringValue("This logical framework does not contain already a specific objective. Please, create one before adding some expected results.")
  @Key("logFrameNoSpecificObjectiveDetails")
  String logFrameNoSpecificObjectiveDetails();

  /**
   * Translated "Are you sure you want to replace the current logical framework?".
   * 
   * @return translated "Are you sure you want to replace the current logical framework?"
   */
  @DefaultStringValue("Are you sure you want to replace the current logical framework?")
  @Key("logFramePasteConfirm")
  String logFramePasteConfirm();

  /**
   * Translated "An error occured while trying to paste the logical framework.".
   * 
   * @return translated "An error occured while trying to paste the logical framework."
   */
  @DefaultStringValue("An error occured while trying to paste the logical framework.")
  @Key("logFramePasteError")
  String logFramePasteError();

  /**
   * Translated "Logical framework pasted.".
   * 
   * @return translated "Logical framework pasted."
   */
  @DefaultStringValue("Logical framework pasted.")
  @Key("logFramePasted")
  String logFramePasted();

  /**
   * Translated "Prerequisite".
   * 
   * @return translated "Prerequisite"
   */
  @DefaultStringValue("Prerequisite")
  @Key("logFramePrerequisite")
  String logFramePrerequisite();

  /**
   * Translated "Prerequisites".
   * 
   * @return translated "Prerequisites"
   */
  @DefaultStringValue("Prerequisites")
  @Key("logFramePrerequisites")
  String logFramePrerequisites();

  /**
   * Translated "P".
   * 
   * @return translated "P"
   */
  @DefaultStringValue("P")
  @Key("logFramePrerequisitesCode")
  String logFramePrerequisitesCode();

  /**
   * Translated "Risks and Assumptions".
   * 
   * @return translated "Risks and Assumptions"
   */
  @DefaultStringValue("Risks and Assumptions")
  @Key("logFrameRisksAndAssumptions")
  String logFrameRisksAndAssumptions();

  /**
   * Translated "Select the expected result to which this activity belongs.".
   * 
   * @return translated "Select the expected result to which this activity belongs."
   */
  @DefaultStringValue("Select the expected result to which this activity belongs.")
  @Key("logFrameSelectGroup2A")
  String logFrameSelectGroup2A();

  /**
   * Translated "Select the specific objective to which this expected result belongs.".
   * 
   * @return translated "Select the specific objective to which this expected result belongs."
   */
  @DefaultStringValue("Select the specific objective to which this expected result belongs.")
  @Key("logFrameSelectGroup2ER")
  String logFrameSelectGroup2ER();

  /**
   * Translated "Select the expected result and the group to which this activity belongs.".
   * 
   * @return translated "Select the expected result and the group to which this activity belongs."
   */
  @DefaultStringValue("Select the expected result and the group to which this activity belongs.")
  @Key("logFrameSelectGroupA")
  String logFrameSelectGroupA();

  /**
   * Translated "Select the specific objective and the group to which this expected result belongs.".
   * 
   * @return translated "Select the specific objective and the group to which this expected result belongs."
   */
  @DefaultStringValue("Select the specific objective and the group to which this expected result belongs.")
  @Key("logFrameSelectGroupER")
  String logFrameSelectGroupER();

  /**
   * Translated "Select the group to which this specific objective belongs.".
   * 
   * @return translated "Select the group to which this specific objective belongs."
   */
  @DefaultStringValue("Select the group to which this specific objective belongs.")
  @Key("logFrameSelectGroupOS")
  String logFrameSelectGroupOS();

  /**
   * Translated "Select the group to which this prerequisite belongs.".
   * 
   * @return translated "Select the group to which this prerequisite belongs."
   */
  @DefaultStringValue("Select the group to which this prerequisite belongs.")
  @Key("logFrameSelectGroupP")
  String logFrameSelectGroupP();

  /**
   * Translated "Select an objective".
   * 
   * @return translated "Select an objective"
   */
  @DefaultStringValue("Select an objective")
  @Key("logFrameSelectObjective")
  String logFrameSelectObjective();

  /**
   * Translated "Select an expected result".
   * 
   * @return translated "Select an expected result"
   */
  @DefaultStringValue("Select an expected result")
  @Key("logFrameSelectResult")
  String logFrameSelectResult();

  /**
   * Translated "Specific objective".
   * 
   * @return translated "Specific objective"
   */
  @DefaultStringValue("Specific objective")
  @Key("logFrameSpecificObjective")
  String logFrameSpecificObjective();

  /**
   * Translated "Specific objectives".
   * 
   * @return translated "Specific objectives"
   */
  @DefaultStringValue("Specific objectives")
  @Key("logFrameSpecificObjectives")
  String logFrameSpecificObjectives();

  /**
   * Translated "SO".
   * 
   * @return translated "SO"
   */
  @DefaultStringValue("SO")
  @Key("logFrameSpecificObjectivesCode")
  String logFrameSpecificObjectivesCode();

  /**
   * Translated "Unauthorized action ".
   * 
   * @return translated "Unauthorized action "
   */
  @DefaultStringValue("Unauthorized action ")
  @Key("logFrameUnauthorizedAction")
  String logFrameUnauthorizedAction();

  /**
   * Translated "Login".
   * 
   * @return translated "Login"
   */
  @DefaultStringValue("Login")
  @Key("login")
  String login();

  /**
   * Translated "Sign in".
   * 
   * @return translated "Sign in"
   */
  @DefaultStringValue("Sign in")
  @Key("loginConnectButton")
  String loginConnectButton();

  /**
   * Translated "Bad login".
   * 
   * @return translated "Bad login"
   */
  @DefaultStringValue("Bad login")
  @Key("loginConnectErrorBadLogin")
  String loginConnectErrorBadLogin();

  /**
   * Translated "Error".
   * 
   * @return translated "Error"
   */
  @DefaultStringValue("Error")
  @Key("loginConnectErrorTitle")
  String loginConnectErrorTitle();

  /**
   * Translated "Login Failed".
   * 
   * @return translated "Login Failed"
   */
  @DefaultStringValue("Login Failed")
  @Key("loginFailed")
  String loginFailed();

  /**
   * Translated "Language".
   * 
   * @return translated "Language"
   */
  @DefaultStringValue("Language")
  @Key("loginLanguageField")
  String loginLanguageField();

  /**
   * Translated "Email Address".
   * 
   * @return translated "Email Address"
   */
  @DefaultStringValue("Email Address")
  @Key("loginLoginField")
  String loginLoginField();

  /**
   * Translated "Password".
   * 
   * @return translated "Password"
   */
  @DefaultStringValue("Password")
  @Key("loginPasswordField")
  String loginPasswordField();

  /**
   * Translated "Did you forget your password?".
   * 
   * @return translated "Did you forget your password?"
   */
  @DefaultStringValue("Did you forget your password?")
  @Key("loginPasswordForgotten")
  String loginPasswordForgotten();

  /**
   * Translated "Logout".
   * 
   * @return translated "Logout"
   */
  @DefaultStringValue("Logout")
  @Key("logout")
  String logout();

  /**
   * Translated "An error occurred while trying to log you out".
   * 
   * @return translated "An error occurred while trying to log you out"
   */
  @DefaultStringValue("An error occurred while trying to log you out")
  @Key("logoutErrorTitle")
  String logoutErrorTitle();

  /**
   * Translated "Longitude".
   * 
   * @return translated "Longitude"
   */
  @DefaultStringValue("Longitude")
  @Key("longitude")
  String longitude();

  /**
   * Translated "Manage all users".
   * 
   * @return translated "Manage all users"
   */
  @DefaultStringValue("Manage all users")
  @Key("manageAllUsers")
  String manageAllUsers();

  /**
   * Translated "There is no help manual for your language. The default language manual will be opened.".
   * 
   * @return translated "There is no help manual for your language. The default language manual will be opened."
   */
  @DefaultStringValue("There is no help manual for your language. The default language manual will be opened.")
  @Key("manualExistingError")
  String manualExistingError();

  /**
   * Translated "The help manual cannot be opened.".
   * 
   * @return translated "The help manual cannot be opened."
   */
  @DefaultStringValue("The help manual cannot be opened.")
  @Key("manualOpeningError")
  String manualOpeningError();

  /**
   * Translated "Error while opening the help manual".
   * 
   * @return translated "Error while opening the help manual"
   */
  @DefaultStringValue("Error while opening the help manual")
  @Key("manualOpeningErrorTitle")
  String manualOpeningErrorTitle();

  /**
   * Translated "Map".
   * 
   * @return translated "Map"
   */
  @DefaultStringValue("Map")
  @Key("map")
  String map();

  /**
   * Translated "Map Icon".
   * 
   * @return translated "Map Icon"
   */
  @DefaultStringValue("Map Icon")
  @Key("mapIcon")
  String mapIcon();

  /**
   * Translated "Maps".
   * 
   * @return translated "Maps"
   */
  @DefaultStringValue("Maps")
  @Key("maps")
  String maps();

  /**
   * Translated "Quickly produce maps of your indicators".
   * 
   * @return translated "Quickly produce maps of your indicators"
   */
  @DefaultStringValue("Quickly produce maps of your indicators")
  @Key("mapsDescription")
  String mapsDescription();

  /**
   * Translated "Menu".
   * 
   * @return translated "Menu"
   */
  @DefaultStringValue("Menu")
  @Key("menu")
  String menu();

  /**
   * Translated "Point".
   * 
   * @return translated "Point"
   */
  @DefaultStringValue("Point")
  @Key("monitoredPoint")
  String monitoredPoint();

  /**
   * Translated "Add a monitored point".
   * 
   * @return translated "Add a monitored point"
   */
  @DefaultStringValue("Add a monitored point")
  @Key("monitoredPointAdd")
  String monitoredPointAdd();

  /**
   * Translated "The new point has been correctly created.".
   * 
   * @return translated "The new point has been correctly created."
   */
  @DefaultStringValue("The new point has been correctly created.")
  @Key("monitoredPointAddConfirm")
  String monitoredPointAddConfirm();

  /**
   * Translated "Fill the properties of this point.".
   * 
   * @return translated "Fill the properties of this point."
   */
  @DefaultStringValue("Fill the properties of this point.")
  @Key("monitoredPointAddDetails")
  String monitoredPointAddDetails();

  /**
   * Translated "Creation error".
   * 
   * @return translated "Creation error"
   */
  @DefaultStringValue("Creation error")
  @Key("monitoredPointAddError")
  String monitoredPointAddError();

  /**
   * Translated "An error occurred during the creation of the point.".
   * 
   * @return translated "An error occurred during the creation of the point."
   */
  @DefaultStringValue("An error occurred during the creation of the point.")
  @Key("monitoredPointAddErrorDetails")
  String monitoredPointAddErrorDetails();

  /**
   * Translated "All".
   * 
   * @return translated "All"
   */
  @DefaultStringValue("All")
  @Key("monitoredPointAll")
  String monitoredPointAll();

  /**
   * Translated "Close".
   * 
   * @return translated "Close"
   */
  @DefaultStringValue("Close")
  @Key("monitoredPointClose")
  String monitoredPointClose();

  /**
   * Translated "Point closed".
   * 
   * @return translated "Point closed"
   */
  @DefaultStringValue("Point closed")
  @Key("monitoredPointClosed")
  String monitoredPointClosed();

  /**
   * Translated "Completed".
   * 
   * @return translated "Completed"
   */
  @DefaultStringValue("Completed")
  @Key("monitoredPointCompleted")
  String monitoredPointCompleted();

  /**
   * Translated "Completed date".
   * 
   * @return translated "Completed date"
   */
  @DefaultStringValue("Completed date")
  @Key("monitoredPointCompletionDate")
  String monitoredPointCompletionDate();

  /**
   * Translated "The monitored point has been correctly deleted.".
   * 
   * @return translated "The monitored point has been correctly deleted."
   */
  @DefaultStringValue("The monitored point has been correctly deleted.")
  @Key("monitoredPointDeletionConfirm")
  String monitoredPointDeletionConfirm();

  /**
   * Translated "An error occurred while deleting the monitored point.".
   * 
   * @return translated "An error occurred while deleting the monitored point."
   */
  @DefaultStringValue("An error occurred while deleting the monitored point.")
  @Key("monitoredPointDeletionErrorDetails")
  String monitoredPointDeletionErrorDetails();

  /**
   * Translated "Date exceeded".
   * 
   * @return translated "Date exceeded"
   */
  @DefaultStringValue("Date exceeded")
  @Key("monitoredPointExceeded")
  String monitoredPointExceeded();

  /**
   * Translated "Expected date".
   * 
   * @return translated "Expected date"
   */
  @DefaultStringValue("Expected date")
  @Key("monitoredPointExpectedDate")
  String monitoredPointExpectedDate();

  /**
   * Translated "Label".
   * 
   * @return translated "Label"
   */
  @DefaultStringValue("Label")
  @Key("monitoredPointLabel")
  String monitoredPointLabel();

  /**
   * Translated "Reopen".
   * 
   * @return translated "Reopen"
   */
  @DefaultStringValue("Reopen")
  @Key("monitoredPointOpen")
  String monitoredPointOpen();

  /**
   * Translated "Uncompleted".
   * 
   * @return translated "Uncompleted"
   */
  @DefaultStringValue("Uncompleted")
  @Key("monitoredPointUncompleted")
  String monitoredPointUncompleted();

  /**
   * Translated "Update a point".
   * 
   * @return translated "Update a point"
   */
  @DefaultStringValue("Update a point")
  @Key("monitoredPointUpdate")
  String monitoredPointUpdate();

  /**
   * Translated "The monitored point has been correctly modified.".
   * 
   * @return translated "The monitored point has been correctly modified."
   */
  @DefaultStringValue("The monitored point has been correctly modified.")
  @Key("monitoredPointUpdateConfirm")
  String monitoredPointUpdateConfirm();

  /**
   * Translated "Update the properties of this point.".
   * 
   * @return translated "Update the properties of this point."
   */
  @DefaultStringValue("Update the properties of this point.")
  @Key("monitoredPointUpdateDetails")
  String monitoredPointUpdateDetails();

  /**
   * Translated "Modification error".
   * 
   * @return translated "Modification error"
   */
  @DefaultStringValue("Modification error")
  @Key("monitoredPointUpdateError")
  String monitoredPointUpdateError();

  /**
   * Translated "An error occurred during the modification of the monitored point.".
   * 
   * @return translated "An error occurred during the modification of the monitored point."
   */
  @DefaultStringValue("An error occurred during the modification of the monitored point.")
  @Key("monitoredPointUpdateErrorDetails")
  String monitoredPointUpdateErrorDetails();

  /**
   * Translated "Pending".
   * 
   * @return translated "Pending"
   */
  @DefaultStringValue("Pending")
  @Key("monitoredPoints")
  String monitoredPoints();

  /**
   * Translated "Month".
   * 
   * @return translated "Month"
   */
  @DefaultStringValue("Month")
  @Key("month")
  String month();

  /**
   * Translated "Monthly".
   * 
   * @return translated "Monthly"
   */
  @DefaultStringValue("Monthly")
  @Key("monthly")
  String monthly();

  /**
   * Translated "Monthly Reports".
   * 
   * @return translated "Monthly Reports"
   */
  @DefaultStringValue("Monthly Reports")
  @Key("monthlyReports")
  String monthlyReports();

  /**
   * Translated "MS Excel (.xls)".
   * 
   * @return translated "MS Excel (.xls)"
   */
  @DefaultStringValue("MS Excel (.xls)")
  @Key("msExcel")
  String msExcel();

  /**
   * Translated "Multiple choices".
   * 
   * @return translated "Multiple choices"
   */
  @DefaultStringValue("Multiple choices")
  @Key("multipleChoice")
  String multipleChoice();

  /**
   * Translated "My Settings".
   * 
   * @return translated "My Settings"
   */
  @DefaultStringValue("My Settings")
  @Key("mySettings")
  String mySettings();

  /**
   * Translated "Name".
   * 
   * @return translated "Name"
   */
  @DefaultStringValue("Name")
  @Key("name")
  String name();

  /**
   * Translated "Narrative".
   * 
   * @return translated "Narrative"
   */
  @DefaultStringValue("Narrative")
  @Key("narrative")
  String narrative();

  /**
   * Translated "New Activity".
   * 
   * @return translated "New Activity"
   */
  @DefaultStringValue("New Activity")
  @Key("newActivity")
  String newActivity();

  /**
   * Translated "New Attribute".
   * 
   * @return translated "New Attribute"
   */
  @DefaultStringValue("New Attribute")
  @Key("newAttribute")
  String newAttribute();

  /**
   * Translated "New Attribute Group".
   * 
   * @return translated "New Attribute Group"
   */
  @DefaultStringValue("New Attribute Group")
  @Key("newAttributeGroup")
  String newAttributeGroup();

  /**
   * Translated "New Database".
   * 
   * @return translated "New Database"
   */
  @DefaultStringValue("New Database")
  @Key("newDatabase")
  String newDatabase();

  /**
   * Translated "New Indicator".
   * 
   * @return translated "New Indicator"
   */
  @DefaultStringValue("New Indicator")
  @Key("newIndicator")
  String newIndicator();

  /**
   * Translated "New Indicator Group".
   * 
   * @return translated "New Indicator Group"
   */
  @DefaultStringValue("New Indicator Group")
  @Key("newIndicatorGroup")
  String newIndicatorGroup();

  /**
   * Translated "New partner".
   * 
   * @return translated "New partner"
   */
  @DefaultStringValue("New partner")
  @Key("newPartner")
  String newPartner();

  /**
   * Translated "New password".
   * 
   * @return translated "New password"
   */
  @DefaultStringValue("New password")
  @Key("newPassword")
  String newPassword();

  /**
   * Translated "New Site".
   * 
   * @return translated "New Site"
   */
  @DefaultStringValue("New Site")
  @Key("newSite")
  String newSite();

  /**
   * Translated "New".
   * 
   * @return translated "New"
   */
  @DefaultStringValue("New")
  @Key("newText")
  String newText();

  /**
   * Translated "New user".
   * 
   * @return translated "New user"
   */
  @DefaultStringValue("New user")
  @Key("newUser")
  String newUser();

  /**
   * Translated "Next".
   * 
   * @return translated "Next"
   */
  @DefaultStringValue("Next")
  @Key("next")
  String next();

  /**
   * Translated "No".
   * 
   * @return translated "No"
   */
  @DefaultStringValue("No")
  @Key("no")
  String no();

  /**
   * Translated "Coordinates must specify a hemisphere (+/-/N/S/W/E)".
   * 
   * @return translated "Coordinates must specify a hemisphere (+/-/N/S/W/E)"
   */
  @DefaultStringValue("Coordinates must specify a hemisphere (+/-/N/S/W/E)")
  @Key("noHemisphere")
  String noHemisphere();

  /**
   * Translated "Coodinates must have at least one number".
   * 
   * @return translated "Coodinates must have at least one number"
   */
  @DefaultStringValue("Coodinates must have at least one number")
  @Key("noNumber")
  String noNumber();

  /**
   * Translated "The request is missing an authorization key. You need to log in first.".
   * 
   * @return translated "The request is missing an authorization key. You need to log in first."
   */
  @DefaultStringValue("The request is missing an authorization key. You need to log in first.")
  @Key("noSessionKey")
  String noSessionKey();

  /**
   * Translated "No sites to display.".
   * 
   * @return translated "No sites to display."
   */
  @DefaultStringValue("No sites to display.")
  @Key("noSitesToDisplay")
  String noSitesToDisplay();

  /**
   * Translated "None".
   * 
   * @return translated "None"
   */
  @DefaultStringValue("None")
  @Key("none")
  String none();

  /**
   * Translated "exclude all closed projects".
   * 
   * @return translated "exclude all closed projects"
   */
  @DefaultStringValue("exclude all closed projects")
  @Key("noneFilter")
  String noneFilter();

  /**
   * Translated "N".
   * 
   * @return translated "N"
   */
  @DefaultStringValue("N")
  @Key("northHemiChars")
  String northHemiChars();

  /**
   * Translated "Not yet implemented.".
   * 
   * @return translated "Not yet implemented."
   */
  @DefaultStringValue("Not yet implemented.")
  @Key("notImplemented")
  String notImplemented();

  /**
   * Translated "Not scheduled".
   * 
   * @return translated "Not scheduled"
   */
  @DefaultStringValue("Not scheduled")
  @Key("notScheduled")
  String notScheduled();

  /**
   * Translated "1".
   * 
   * @return translated "1"
   */
  @DefaultStringValue("1")
  @Key("number_1")
  String number_1();

  /**
   * Translated "10".
   * 
   * @return translated "10"
   */
  @DefaultStringValue("10")
  @Key("number_10")
  String number_10();

  /**
   * Translated "11".
   * 
   * @return translated "11"
   */
  @DefaultStringValue("11")
  @Key("number_11")
  String number_11();

  /**
   * Translated "12".
   * 
   * @return translated "12"
   */
  @DefaultStringValue("12")
  @Key("number_12")
  String number_12();

  /**
   * Translated "13".
   * 
   * @return translated "13"
   */
  @DefaultStringValue("13")
  @Key("number_13")
  String number_13();

  /**
   * Translated "14".
   * 
   * @return translated "14"
   */
  @DefaultStringValue("14")
  @Key("number_14")
  String number_14();

  /**
   * Translated "15".
   * 
   * @return translated "15"
   */
  @DefaultStringValue("15")
  @Key("number_15")
  String number_15();

  /**
   * Translated "16".
   * 
   * @return translated "16"
   */
  @DefaultStringValue("16")
  @Key("number_16")
  String number_16();

  /**
   * Translated "17".
   * 
   * @return translated "17"
   */
  @DefaultStringValue("17")
  @Key("number_17")
  String number_17();

  /**
   * Translated "18".
   * 
   * @return translated "18"
   */
  @DefaultStringValue("18")
  @Key("number_18")
  String number_18();

  /**
   * Translated "19".
   * 
   * @return translated "19"
   */
  @DefaultStringValue("19")
  @Key("number_19")
  String number_19();

  /**
   * Translated "2".
   * 
   * @return translated "2"
   */
  @DefaultStringValue("2")
  @Key("number_2")
  String number_2();

  /**
   * Translated "20".
   * 
   * @return translated "20"
   */
  @DefaultStringValue("20")
  @Key("number_20")
  String number_20();

  /**
   * Translated "21".
   * 
   * @return translated "21"
   */
  @DefaultStringValue("21")
  @Key("number_21")
  String number_21();

  /**
   * Translated "22".
   * 
   * @return translated "22"
   */
  @DefaultStringValue("22")
  @Key("number_22")
  String number_22();

  /**
   * Translated "23".
   * 
   * @return translated "23"
   */
  @DefaultStringValue("23")
  @Key("number_23")
  String number_23();

  /**
   * Translated "24".
   * 
   * @return translated "24"
   */
  @DefaultStringValue("24")
  @Key("number_24")
  String number_24();

  /**
   * Translated "25".
   * 
   * @return translated "25"
   */
  @DefaultStringValue("25")
  @Key("number_25")
  String number_25();

  /**
   * Translated "26".
   * 
   * @return translated "26"
   */
  @DefaultStringValue("26")
  @Key("number_26")
  String number_26();

  /**
   * Translated "27".
   * 
   * @return translated "27"
   */
  @DefaultStringValue("27")
  @Key("number_27")
  String number_27();

  /**
   * Translated "28".
   * 
   * @return translated "28"
   */
  @DefaultStringValue("28")
  @Key("number_28")
  String number_28();

  /**
   * Translated "3".
   * 
   * @return translated "3"
   */
  @DefaultStringValue("3")
  @Key("number_3")
  String number_3();

  /**
   * Translated "4".
   * 
   * @return translated "4"
   */
  @DefaultStringValue("4")
  @Key("number_4")
  String number_4();

  /**
   * Translated "5".
   * 
   * @return translated "5"
   */
  @DefaultStringValue("5")
  @Key("number_5")
  String number_5();

  /**
   * Translated "6".
   * 
   * @return translated "6"
   */
  @DefaultStringValue("6")
  @Key("number_6")
  String number_6();

  /**
   * Translated "7".
   * 
   * @return translated "7"
   */
  @DefaultStringValue("7")
  @Key("number_7")
  String number_7();

  /**
   * Translated "8".
   * 
   * @return translated "8"
   */
  @DefaultStringValue("8")
  @Key("number_8")
  String number_8();

  /**
   * Translated "9".
   * 
   * @return translated "9"
   */
  @DefaultStringValue("9")
  @Key("number_9")
  String number_9();

  /**
   * Translated "OpenDocumentSpreadsheet".
   * 
   * @return translated "OpenDocumentSpreadsheet"
   */
  @DefaultStringValue("OpenDocumentSpreadsheet")
  @Key("ods")
  String ods();

  /**
   * Translated "OK".
   * 
   * @return translated "OK"
   */
  @DefaultStringValue("OK")
  @Key("ok")
  String ok();

  /**
   * Translated "Online".
   * 
   * @return translated "Online"
   */
  @DefaultStringValue("Online")
  @Key("online")
  String online();

  /**
   * Translated "Open document spreadsheet (.ods)".
   * 
   * @return translated "Open document spreadsheet (.ods)"
   */
  @DefaultStringValue("Open document spreadsheet (.ods)")
  @Key("openDocumentSpreadsheet")
  String openDocumentSpreadsheet();

  /**
   * Translated "Operational".
   * 
   * @return translated "Operational"
   */
  @DefaultStringValue("Operational")
  @Key("operational")
  String operational();

  /**
   * Translated "Organisational unit synthesis".
   * 
   * @return translated "Organisational unit synthesis"
   */
  @DefaultStringValue("Organisational unit synthesis")
  @Key("orgUnitSynthesis")
  String orgUnitSynthesis();

  /**
   * Translated "Informations".
   * 
   * @return translated "Informations"
   */
  @DefaultStringValue("Informations")
  @Key("orgUnitTabInformations")
  String orgUnitTabInformations();

  /**
   * Translated "Overview".
   * 
   * @return translated "Overview"
   */
  @DefaultStringValue("Overview")
  @Key("orgUnitTabOverview")
  String orgUnitTabOverview();

  /**
   * Translated "Actual logo :".
   * 
   * @return translated "Actual logo :"
   */
  @DefaultStringValue("Actual logo :")
  @Key("organizationManagementActualLogo")
  String organizationManagementActualLogo();

  /**
   * Translated "The organization name cannot be empty".
   * 
   * @return translated "The organization name cannot be empty"
   */
  @DefaultStringValue("The organization name cannot be empty")
  @Key("organizationManagementBlankNameNotificationError")
  String organizationManagementBlankNameNotificationError();

  /**
   * Translated "Error with the local cache".
   * 
   * @return translated "Error with the local cache"
   */
  @DefaultStringValue("Error with the local cache")
  @Key("organizationManagementLocalCacheNotificationError")
  String organizationManagementLocalCacheNotificationError();

  /**
   * Translated "Your new logo is correctly saved !".
   * 
   * @return translated "Your new logo is correctly saved !"
   */
  @DefaultStringValue("Your new logo is correctly saved !")
  @Key("organizationManagementLogoNotificationMessage")
  String organizationManagementLogoNotificationMessage();

  /**
   * Translated "Logo changed".
   * 
   * @return translated "Logo changed"
   */
  @DefaultStringValue("Logo changed")
  @Key("organizationManagementLogoNotificationTitle")
  String organizationManagementLogoNotificationTitle();

  /**
   * Translated "Logo of the organization :".
   * 
   * @return translated "Logo of the organization :"
   */
  @DefaultStringValue("Logo of the organization :")
  @Key("organizationManagementLogoUpload")
  String organizationManagementLogoUpload();

  /**
   * Translated "Name of the organization :".
   * 
   * @return translated "Name of the organization :"
   */
  @DefaultStringValue("Name of the organization :")
  @Key("organizationManagementOrganizationName")
  String organizationManagementOrganizationName();

  /**
   * Translated "Save changes".
   * 
   * @return translated "Save changes"
   */
  @DefaultStringValue("Save changes")
  @Key("organizationManagementSaveChanges")
  String organizationManagementSaveChanges();

  /**
   * Translated "Your changes are correctly saved !".
   * 
   * @return translated "Your changes are correctly saved !"
   */
  @DefaultStringValue("Your changes are correctly saved !")
  @Key("organizationManagementSaveChangesNotificationMessage")
  String organizationManagementSaveChangesNotificationMessage();

  /**
   * Translated "Changes saved".
   * 
   * @return translated "Changes saved"
   */
  @DefaultStringValue("Changes saved")
  @Key("organizationManagementSaveChangesNotificationTitle")
  String organizationManagementSaveChangesNotificationTitle();

  /**
   * Translated "Organization management".
   * 
   * @return translated "Organization management"
   */
  @DefaultStringValue("Organization management")
  @Key("organizationManagementTitle")
  String organizationManagementTitle();

  /**
   * Translated "Error during the communication with the server".
   * 
   * @return translated "Error during the communication with the server"
   */
  @DefaultStringValue("Error during the communication with the server")
  @Key("organizationManagementWebServiceNotificationError")
  String organizationManagementWebServiceNotificationError();

  /**
   * Translated "Organizations".
   * 
   * @return translated "Organizations"
   */
  @DefaultStringValue("Organizations")
  @Key("organizations")
  String organizations();

  /**
   * Translated "Organizational unit".
   * 
   * @return translated "Organizational unit"
   */
  @DefaultStringValue("Organizational unit")
  @Key("orgunit")
  String orgunit();

  /**
   * Translated "Name".
   * 
   * @return translated "Name"
   */
  @DefaultStringValue("Name")
  @Key("orgunitCompleteName")
  String orgunitCompleteName();

  /**
   * Translated "Select an organizational unit...".
   * 
   * @return translated "Select an organizational unit..."
   */
  @DefaultStringValue("Select an organizational unit...")
  @Key("orgunitEmptyChoice")
  String orgunitEmptyChoice();

  /**
   * Translated "Organization chart".
   * 
   * @return translated "Organization chart"
   */
  @DefaultStringValue("Organization chart")
  @Key("orgunitTree")
  String orgunitTree();

  /**
   * Translated "Double-click on the name of an organizational unit to open its page.".
   * 
   * @return translated "Double-click on the name of an organizational unit to open its page."
   */
  @DefaultStringValue("Double-click on the name of an organizational unit to open its page.")
  @Key("orgunitTreeOpen")
  String orgunitTreeOpen();

  /**
   * Translated "Consult".
   * 
   * @return translated "Consult"
   */
  @DefaultStringValue("Consult")
  @Key("orgunitVisit")
  String orgunitVisit();

  /**
   * Translated "Email".
   * 
   * @return translated "Email"
   */
  @DefaultStringValue("Email")
  @Key("ownerEmail")
  String ownerEmail();

  /**
   * Translated "Administrator".
   * 
   * @return translated "Administrator"
   */
  @DefaultStringValue("Administrator")
  @Key("ownerName")
  String ownerName();

  /**
   * Translated "Page Layout".
   * 
   * @return translated "Page Layout"
   */
  @DefaultStringValue("Page Layout")
  @Key("pageLayout")
  String pageLayout();

  /**
   * Translated "Page Size".
   * 
   * @return translated "Page Size"
   */
  @DefaultStringValue("Page Size")
  @Key("pageSize")
  String pageSize();

  /**
   * Translated "Partner".
   * 
   * @return translated "Partner"
   */
  @DefaultStringValue("Partner")
  @Key("partner")
  String partner();

  /**
   * Translated "Define the partner organisations who participate in this database.".
   * 
   * @return translated "Define the partner organisations who participate in this database."
   */
  @DefaultStringValue("Define the partner organisations who participate in this database.")
  @Key("partnerEditorDescription")
  String partnerEditorDescription();

  /**
   * Translated "Partners".
   * 
   * @return translated "Partners"
   */
  @DefaultStringValue("Partners")
  @Key("partners")
  String partners();

  /**
   * Translated "Password".
   * 
   * @return translated "Password"
   */
  @DefaultStringValue("Password")
  @Key("password")
  String password();

  /**
   * Translated "Passwords are not matched".
   * 
   * @return translated "Passwords are not matched"
   */
  @DefaultStringValue("Passwords are not matched")
  @Key("passwordNotMatch")
  String passwordNotMatch();

  /**
   * Translated "Password Reset".
   * 
   * @return translated "Password Reset"
   */
  @DefaultStringValue("Password Reset")
  @Key("passwordReset")
  String passwordReset();

  /**
   * Translated "Your password is successfully updated. You will be redirected to a login page.".
   * 
   * @return translated "Your password is successfully updated. You will be redirected to a login page."
   */
  @DefaultStringValue("Your password is successfully updated. You will be redirected to a login page.")
  @Key("passwordUpdated")
  String passwordUpdated();

  /**
   * Translated "Paste".
   * 
   * @return translated "Paste"
   */
  @DefaultStringValue("Paste")
  @Key("paste")
  String paste();

  /**
   * Translated "PDF".
   * 
   * @return translated "PDF"
   */
  @DefaultStringValue("PDF")
  @Key("pdf")
  String pdf();

  /**
   * Translated "Sorry, you don't have administration permissions.".
   * 
   * @return translated "Sorry, you don't have administration permissions."
   */
  @DefaultStringValue("Sorry, you don't have administration permissions.")
  @Key("permAdminInsufficient")
  String permAdminInsufficient();

  /**
   * Translated "Sorry, you don't have sufficient permissions to edit this indicator.".
   * 
   * @return translated "Sorry, you don't have sufficient permissions to edit this indicator."
   */
  @DefaultStringValue("Sorry, you don't have sufficient permissions to edit this indicator.")
  @Key("permEditIndicatorInsufficient")
  String permEditIndicatorInsufficient();

  /**
   * Translated "Sorry, you don't have sufficient permissions to manage this indicator.".
   * 
   * @return translated "Sorry, you don't have sufficient permissions to manage this indicator."
   */
  @DefaultStringValue("Sorry, you don't have sufficient permissions to manage this indicator.")
  @Key("permManageIndicatorInsufficient")
  String permManageIndicatorInsufficient();

  /**
   * Translated "Sorry, you don't have sufficient permissions to administer users.".
   * 
   * @return translated "Sorry, you don't have sufficient permissions to administer users."
   */
  @DefaultStringValue("Sorry, you don't have sufficient permissions to administer users.")
  @Key("permManageUsersInsufficient")
  String permManageUsersInsufficient();

  /**
   * Translated "Sorry, you don't have sufficient permissions to view this organizational unit.".
   * 
   * @return translated "Sorry, you don't have sufficient permissions to view this organizational unit."
   */
  @DefaultStringValue("Sorry, you don't have sufficient permissions to view this organizational unit.")
  @Key("permViewOrgUnitInsufficient")
  String permViewOrgUnitInsufficient();

  /**
   * Translated "Sorry, you don't have sufficient permissions to view this project.".
   * 
   * @return translated "Sorry, you don't have sufficient permissions to view this project."
   */
  @DefaultStringValue("Sorry, you don't have sufficient permissions to view this project.")
  @Key("permViewProjectInsufficient")
  String permViewProjectInsufficient();

  /**
   * Translated "Sorry, you don't have sufficient permissions to view projects.".
   * 
   * @return translated "Sorry, you don't have sufficient permissions to view projects."
   */
  @DefaultStringValue("Sorry, you don't have sufficient permissions to view projects.")
  @Key("permViewProjectsInsufficient")
  String permViewProjectsInsufficient();

  /**
   * Translated "Permissions".
   * 
   * @return translated "Permissions"
   */
  @DefaultStringValue("Permissions")
  @Key("permissions")
  String permissions();

  /**
   * Translated "Phase's details".
   * 
   * @return translated "Phase's details"
   */
  @DefaultStringValue("Phase's details")
  @Key("phaseDetails")
  String phaseDetails();

  /**
   * Translated "Cross all of the dimensions of your results, including by activity, time period, partner, or geography".
   * 
   * @return translated "Cross all of the dimensions of your results, including by activity, time period, partner, or geography"
   */
  @DefaultStringValue("Cross all of the dimensions of your results, including by activity, time period, partner, or geography")
  @Key("pivotTableDescription")
  String pivotTableDescription();

  /**
   * Translated "Pivot Tables".
   * 
   * @return translated "Pivot Tables"
   */
  @DefaultStringValue("Pivot Tables")
  @Key("pivotTables")
  String pivotTables();

  /**
   * Translated "Planned".
   * 
   * @return translated "Planned"
   */
  @DefaultStringValue("Planned")
  @Key("planned")
  String planned();

  /**
   * Translated "Please complete the form correctly before continuing.".
   * 
   * @return translated "Please complete the form correctly before continuing."
   */
  @DefaultStringValue("Please complete the form correctly before continuing.")
  @Key("pleaseCompleteForm")
  String pleaseCompleteForm();

  /**
   * Translated "Please select the indicator".
   * 
   * @return translated "Please select the indicator"
   */
  @DefaultStringValue("Please select the indicator")
  @Key("pleaseSelectIndicator")
  String pleaseSelectIndicator();

  /**
   * Translated "Please wait...".
   * 
   * @return translated "Please wait..."
   */
  @DefaultStringValue("Please wait...")
  @Key("pleaseWait")
  String pleaseWait();

  /**
   * Translated "Possible values".
   * 
   * @return translated "Possible values"
   */
  @DefaultStringValue("Possible values")
  @Key("possibleValues")
  String possibleValues();

  /**
   * Translated "PowerPoint".
   * 
   * @return translated "PowerPoint"
   */
  @DefaultStringValue("PowerPoint")
  @Key("powerPoint")
  String powerPoint();

  /**
   * Translated "Preview".
   * 
   * @return translated "Preview"
   */
  @DefaultStringValue("Preview")
  @Key("preview")
  String preview();

  /**
   * Translated "Previous".
   * 
   * @return translated "Previous"
   */
  @DefaultStringValue("Previous")
  @Key("previous")
  String previous();

  /**
   * Translated "Program".
   * 
   * @return translated "Program"
   */
  @DefaultStringValue("Program")
  @Key("program")
  String program();

  /**
   * Translated "project".
   * 
   * @return translated "project"
   */
  @DefaultStringValue("project")
  @Key("project")
  String project();

  /**
   * Translated "Activate this phase".
   * 
   * @return translated "Activate this phase"
   */
  @DefaultStringValue("Activate this phase")
  @Key("projectActivatePhaseButton")
  String projectActivatePhaseButton();

  /**
   * Translated "Error during phase activation".
   * 
   * @return translated "Error during phase activation"
   */
  @DefaultStringValue("Error during phase activation")
  @Key("projectActivatePhaseError")
  String projectActivatePhaseError();

  /**
   * Translated "An error occurred during the phase activation.".
   * 
   * @return translated "An error occurred during the phase activation."
   */
  @DefaultStringValue("An error occurred during the phase activation.")
  @Key("projectActivatePhaseErrorDetails")
  String projectActivatePhaseErrorDetails();

  /**
   * Translated "Active phase".
   * 
   * @return translated "Active phase"
   */
  @DefaultStringValue("Active phase")
  @Key("projectActivePhase")
  String projectActivePhase();

  /**
   * Translated "Budget (spent / planned)".
   * 
   * @return translated "Budget (spent / planned)"
   */
  @DefaultStringValue("Budget (spent / planned)")
  @Key("projectBannerBudget")
  String projectBannerBudget();

  /**
   * Translated "Budget".
   * 
   * @return translated "Budget"
   */
  @DefaultStringValue("Budget")
  @Key("projectBudget")
  String projectBudget();

  /**
   * Translated "All the required fields of the active phase must be filled and saved before activating this phase.".
   * 
   * @return translated "All the required fields of the active phase must be filled and saved before activating this phase."
   */
  @DefaultStringValue("All the required fields of the active phase must be filled and saved before activating this phase.")
  @Key("projectCannotActivate")
  String projectCannotActivate();

  /**
   * Translated "All the required fields of this phase must be filled and saved before closing it.".
   * 
   * @return translated "All the required fields of this phase must be filled and saved before closing it."
   */
  @DefaultStringValue("All the required fields of this phase must be filled and saved before closing it.")
  @Key("projectCannotClose")
  String projectCannotClose();

  /**
   * Translated "Changing model's status".
   * 
   * @return translated "Changing model's status"
   */
  @DefaultStringValue("Changing model's status")
  @Key("projectChangeStatus")
  String projectChangeStatus();

  /**
   * Translated "Close the active phase".
   * 
   * @return translated "Close the active phase"
   */
  @DefaultStringValue("Close the active phase")
  @Key("projectCloseAndActivate")
  String projectCloseAndActivate();

  /**
   * Translated "Close this phase".
   * 
   * @return translated "Close this phase"
   */
  @DefaultStringValue("Close this phase")
  @Key("projectClosePhaseButton")
  String projectClosePhaseButton();

  /**
   * Translated "Closed date".
   * 
   * @return translated "Closed date"
   */
  @DefaultStringValue("Closed date")
  @Key("projectClosedDate")
  String projectClosedDate();

  /**
   * Translated "Closed".
   * 
   * @return translated "Closed"
   */
  @DefaultStringValue("Closed")
  @Key("projectClosedLabel")
  String projectClosedLabel();

  /**
   * Translated "Country".
   * 
   * @return translated "Country"
   */
  @DefaultStringValue("Country")
  @Key("projectCountry")
  String projectCountry();

  /**
   * Translated "Details".
   * 
   * @return translated "Details"
   */
  @DefaultStringValue("Details")
  @Key("projectDetails")
  String projectDetails();

  /**
   * Translated "No detail...".
   * 
   * @return translated "No detail..."
   */
  @DefaultStringValue("No detail...")
  @Key("projectDetailsNoDetails")
  String projectDetailsNoDetails();

  /**
   * Translated "End the project".
   * 
   * @return translated "End the project"
   */
  @DefaultStringValue("End the project")
  @Key("projectEnd")
  String projectEnd();

  /**
   * Translated "End date".
   * 
   * @return translated "End date"
   */
  @DefaultStringValue("End date")
  @Key("projectEndDate")
  String projectEndDate();

  /**
   * Translated "Error during project closing".
   * 
   * @return translated "Error during project closing"
   */
  @DefaultStringValue("Error during project closing")
  @Key("projectEndError")
  String projectEndError();

  /**
   * Translated "An error occurred during the project closing.".
   * 
   * @return translated "An error occurred during the project closing."
   */
  @DefaultStringValue("An error occurred during the project closing.")
  @Key("projectEndErrorDetails")
  String projectEndErrorDetails();

  /**
   * Translated "Finances your project at".
   * 
   * @return translated "Finances your project at"
   */
  @DefaultStringValue("Finances your project at")
  @Key("projectFinances")
  String projectFinances();

  /**
   * Translated "Your project finances this project in the amount of".
   * 
   * @return translated "Your project finances this project in the amount of"
   */
  @DefaultStringValue("Your project finances this project in the amount of")
  @Key("projectFinancesDetails")
  String projectFinancesDetails();

  /**
   * Translated "Funding sources".
   * 
   * @return translated "Funding sources"
   */
  @DefaultStringValue("Funding sources")
  @Key("projectFinancialProjectsHeader")
  String projectFinancialProjectsHeader();

  /**
   * Translated "Title".
   * 
   * @return translated "Title"
   */
  @DefaultStringValue("Title")
  @Key("projectFullName")
  String projectFullName();

  /**
   * Translated "Is funded by your project at".
   * 
   * @return translated "Is funded by your project at"
   */
  @DefaultStringValue("Is funded by your project at")
  @Key("projectFundedBy")
  String projectFundedBy();

  /**
   * Translated "This project finances your project in the amount of".
   * 
   * @return translated "This project finances your project in the amount of"
   */
  @DefaultStringValue("This project finances your project in the amount of")
  @Key("projectFundedByDetails")
  String projectFundedByDetails();

  /**
   * Translated "general information".
   * 
   * @return translated "general information"
   */
  @DefaultStringValue("general information")
  @Key("projectInfos")
  String projectInfos();

  /**
   * Translated "Linked projects".
   * 
   * @return translated "Linked projects"
   */
  @DefaultStringValue("Linked projects")
  @Key("projectLinkedProjects")
  String projectLinkedProjects();

  /**
   * Translated "Funded projects".
   * 
   * @return translated "Funded projects"
   */
  @DefaultStringValue("Funded projects")
  @Key("projectLocalPartnerProjectsHeader")
  String projectLocalPartnerProjectsHeader();

  /**
   * Translated "Project".
   * 
   * @return translated "Project"
   */
  @DefaultStringValue("Project")
  @Key("projectMainTabTitle")
  String projectMainTabTitle();

  /**
   * Translated "Manager".
   * 
   * @return translated "Manager"
   */
  @DefaultStringValue("Manager")
  @Key("projectManager")
  String projectManager();

  /**
   * Translated "Model".
   * 
   * @return translated "Model"
   */
  @DefaultStringValue("Model")
  @Key("projectModel")
  String projectModel();

  /**
   * Translated "Select a model...".
   * 
   * @return translated "Select a model..."
   */
  @DefaultStringValue("Select a model...")
  @Key("projectModelEmptyChoice")
  String projectModelEmptyChoice();

  /**
   * Translated "Code".
   * 
   * @return translated "Code"
   */
  @DefaultStringValue("Code")
  @Key("projectName")
  String projectName();

  /**
   * Translated "Owner".
   * 
   * @return translated "Owner"
   */
  @DefaultStringValue("Owner")
  @Key("projectOwner")
  String projectOwner();

  /**
   * Translated "Activation error.".
   * 
   * @return translated "Activation error."
   */
  @DefaultStringValue("Activation error.")
  @Key("projectPhaseActivationError")
  String projectPhaseActivationError();

  /**
   * Translated "Please, fill in all the active phase's required elements to activate this new phase.".
   * 
   * @return translated "Please, fill in all the active phase's required elements to activate this new phase."
   */
  @DefaultStringValue("Please, fill in all the active phase's required elements to activate this new phase.")
  @Key("projectPhaseActivationErrorDetails")
  String projectPhaseActivationErrorDetails();

  /**
   * Translated "Unsaved elements".
   * 
   * @return translated "Unsaved elements"
   */
  @DefaultStringValue("Unsaved elements")
  @Key("projectPhaseChangeAlert")
  String projectPhaseChangeAlert();

  /**
   * Translated "Some elements have been modified, do you want to save them before switching phase ?".
   * 
   * @return translated "Some elements have been modified, do you want to save them before switching phase ?"
   */
  @DefaultStringValue("Some elements have been modified, do you want to save them before switching phase ?")
  @Key("projectPhaseChangeAlertDetails")
  String projectPhaseChangeAlertDetails();

  /**
   * Translated "Phase guide".
   * 
   * @return translated "Phase guide"
   */
  @DefaultStringValue("Phase guide")
  @Key("projectPhaseGuideHeader")
  String projectPhaseGuideHeader();

  /**
   * Translated "No guide available for this phase.".
   * 
   * @return translated "No guide available for this phase."
   */
  @DefaultStringValue("No guide available for this phase.")
  @Key("projectPhaseGuideUnavailable")
  String projectPhaseGuideUnavailable();

  /**
   * Translated "Planned budget".
   * 
   * @return translated "Planned budget"
   */
  @DefaultStringValue("Planned budget")
  @Key("projectPlannedBudget")
  String projectPlannedBudget();

  /**
   * Translated "Received budget".
   * 
   * @return translated "Received budget"
   */
  @DefaultStringValue("Received budget")
  @Key("projectReceivedBudget")
  String projectReceivedBudget();

  /**
   * Translated "If you've opened this document with MS Word, select this field and press F9 to generate the table of contents.".
   * 
   * @return translated "If you've opened this document with MS Word, select this field and press F9 to generate the table of contents."
   */
  @DefaultStringValue("If you've opened this document with MS Word, select this field and press F9 to generate the table of contents.")
  @Key("projectReportTableOfContents")
  String projectReportTableOfContents();

  /**
   * Translated "Required fields".
   * 
   * @return translated "Required fields"
   */
  @DefaultStringValue("Required fields")
  @Key("projectRequiredElements")
  String projectRequiredElements();

  /**
   * Translated "Type".
   * 
   * @return translated "Type"
   */
  @DefaultStringValue("Type")
  @Key("projectRequiredElementsElementType")
  String projectRequiredElementsElementType();

  /**
   * Translated "Filled In ?".
   * 
   * @return translated "Filled In ?"
   */
  @DefaultStringValue("Filled In ?")
  @Key("projectRequiredElementsGridChecked")
  String projectRequiredElementsGridChecked();

  /**
   * Translated "Element name".
   * 
   * @return translated "Element name"
   */
  @DefaultStringValue("Element name")
  @Key("projectRequiredElementsGridLabel")
  String projectRequiredElementsGridLabel();

  /**
   * Translated "Save modifications".
   * 
   * @return translated "Save modifications"
   */
  @DefaultStringValue("Save modifications")
  @Key("projectSavePhaseButton")
  String projectSavePhaseButton();

  /**
   * Translated "Spent budget".
   * 
   * @return translated "Spent budget"
   */
  @DefaultStringValue("Spent budget")
  @Key("projectSpendBudget")
  String projectSpendBudget();

  /**
   * Translated "This project's favorite tag has been changed.".
   * 
   * @return translated "This project's favorite tag has been changed."
   */
  @DefaultStringValue("This project's favorite tag has been changed.")
  @Key("projectStarred")
  String projectStarred();

  /**
   * Translated "Save error".
   * 
   * @return translated "Save error"
   */
  @DefaultStringValue("Save error")
  @Key("projectStarredError")
  String projectStarredError();

  /**
   * Translated "An error occurred during the modification of the project.".
   * 
   * @return translated "An error occurred during the modification of the project."
   */
  @DefaultStringValue("An error occurred during the modification of the project.")
  @Key("projectStarredErrorDetails")
  String projectStarredErrorDetails();

  /**
   * Translated "Start date".
   * 
   * @return translated "Start date"
   */
  @DefaultStringValue("Start date")
  @Key("projectStartDate")
  String projectStartDate();

  /**
   * Translated "Project synthesis".
   * 
   * @return translated "Project synthesis"
   */
  @DefaultStringValue("Project synthesis")
  @Key("projectSynthesis")
  String projectSynthesis();

  /**
   * Translated "Calendar".
   * 
   * @return translated "Calendar"
   */
  @DefaultStringValue("Calendar")
  @Key("projectTabCalendar")
  String projectTabCalendar();

  /**
   * Translated "Management board".
   * 
   * @return translated "Management board"
   */
  @DefaultStringValue("Management board")
  @Key("projectTabDashboard")
  String projectTabDashboard();

  /**
   * Translated "Indicator Data Entry".
   * 
   * @return translated "Indicator Data Entry"
   */
  @DefaultStringValue("Indicator Data Entry")
  @Key("projectTabDataEntry")
  String projectTabDataEntry();

  /**
   * Translated "Manage Indicators".
   * 
   * @return translated "Manage Indicators"
   */
  @DefaultStringValue("Manage Indicators")
  @Key("projectTabIndicators")
  String projectTabIndicators();

  /**
   * Translated "Log frame".
   * 
   * @return translated "Log frame"
   */
  @DefaultStringValue("Log frame")
  @Key("projectTabLogFrame")
  String projectTabLogFrame();

  /**
   * Translated "Reports & documents".
   * 
   * @return translated "Reports & documents"
   */
  @DefaultStringValue("Reports & documents")
  @Key("projectTabReports")
  String projectTabReports();

  /**
   * Translated "Security incidents".
   * 
   * @return translated "Security incidents"
   */
  @DefaultStringValue("Security incidents")
  @Key("projectTabSecurityIncident")
  String projectTabSecurityIncident();

  /**
   * Translated "Time".
   * 
   * @return translated "Time"
   */
  @DefaultStringValue("Time")
  @Key("projectTime")
  String projectTime();

  /**
   * Translated "Group by".
   * 
   * @return translated "Group by"
   */
  @DefaultStringValue("Group by")
  @Key("projectTypeFilter")
  String projectTypeFilter();

  /**
   * Translated "View projects".
   * 
   * @return translated "View projects"
   */
  @DefaultStringValue("View projects")
  @Key("projectViewAll")
  String projectViewAll();

  /**
   * Translated "You must create at least one site in the \"Map\" sub-tab before being able to enter values for your indicators.".
   * 
   * @return translated "You must create at least one site in the \"Map\" sub-tab before being able to enter values for your indicators."
   */
  @DefaultStringValue("You must create at least one site in the \"Map\" sub-tab before being able to enter values for your indicators.")
  @Key("projectWithNoSitesWarning")
  String projectWithNoSitesWarning();

  /**
   * Translated "Projects".
   * 
   * @return translated "Projects"
   */
  @DefaultStringValue("Projects")
  @Key("projects")
  String projects();

  /**
   * Translated "You have unsaved changes. Do you want to save before continuing?".
   * 
   * @return translated "You have unsaved changes. Do you want to save before continuing?"
   */
  @DefaultStringValue("You have unsaved changes. Do you want to save before continuing?")
  @Key("promptSave")
  String promptSave();

  /**
   * Translated "Qualitative".
   * 
   * @return translated "Qualitative"
   */
  @DefaultStringValue("Qualitative")
  @Key("qualitative")
  String qualitative();

  /**
   * Translated "Quantitative".
   * 
   * @return translated "Quantitative"
   */
  @DefaultStringValue("Quantitative")
  @Key("quantitative")
  String quantitative();

  /**
   * Translated "Number".
   * 
   * @return translated "Number"
   */
  @DefaultStringValue("Number")
  @Key("quantityType")
  String quantityType();

  /**
   * Translated "Quarter".
   * 
   * @return translated "Quarter"
   */
  @DefaultStringValue("Quarter")
  @Key("quarter")
  String quarter();

  /**
   * Translated "Maximum Radius".
   * 
   * @return translated "Maximum Radius"
   */
  @DefaultStringValue("Maximum Radius")
  @Key("radiusMaximum")
  String radiusMaximum();

  /**
   * Translated "Minimum radius".
   * 
   * @return translated "Minimum radius"
   */
  @DefaultStringValue("Minimum radius")
  @Key("radiusMinimum")
  String radiusMinimum();

  /**
   * Translated "#Error: A '\"Ready\"' model can be only shifted to status '\"Draft\"' or '\"unavailable\"' !".
   * 
   * @return translated "#Error: A '\"Ready\"' model can be only shifted to status '\"Draft\"' or '\"unavailable\"' !"
   */
  @DefaultStringValue("#Error: A '\"Ready\"' model can be only shifted to status '\"Draft\"' or '\"unavailable\"' !")
  @Key("readyModelStatusChangeError")
  String readyModelStatusChangeError();

  /**
   * Translated "Refresh".
   * 
   * @return translated "Refresh"
   */
  @DefaultStringValue("Refresh")
  @Key("refresh")
  String refresh();

  /**
   * Translated "Refresh Preview".
   * 
   * @return translated "Refresh Preview"
   */
  @DefaultStringValue("Refresh Preview")
  @Key("refreshPreview")
  String refreshPreview();

  /**
   * Translated "Reload project".
   * 
   * @return translated "Reload project"
   */
  @DefaultStringValue("Reload project")
  @Key("refreshProjectList")
  String refreshProjectList();

  /**
   * Translated "The list below doesn't refresh automatically. Consider on clicking on this button to refresh your dashboard.".
   * 
   * @return translated "The list below doesn't refresh automatically. Consider on clicking on this button to refresh your dashboard."
   */
  @DefaultStringValue("The list below doesn't refresh automatically. Consider on clicking on this button to refresh your dashboard.")
  @Key("refreshProjectListDetails")
  String refreshProjectListDetails();

  /**
   * Translated "An error occurred while retrieving projects. All the projects could not be retrieved correctly.".
   * 
   * @return translated "An error occurred while retrieving projects. All the projects could not be retrieved correctly."
   */
  @DefaultStringValue("An error occurred while retrieving projects. All the projects could not be retrieved correctly.")
  @Key("refreshProjectListError")
  String refreshProjectListError();

  /**
   * Translated "projects loaded".
   * 
   * @return translated "projects loaded"
   */
  @DefaultStringValue("projects loaded")
  @Key("refreshProjectListProjectLoaded")
  String refreshProjectListProjectLoaded();

  /**
   * Translated "Reinstall offline mode".
   * 
   * @return translated "Reinstall offline mode"
   */
  @DefaultStringValue("Reinstall offline mode")
  @Key("reinstallOfflineMode")
  String reinstallOfflineMode();

  /**
   * Translated "Remember me".
   * 
   * @return translated "Remember me"
   */
  @DefaultStringValue("Remember me")
  @Key("rememberMe")
  String rememberMe();

  /**
   * Translated "Add a todo".
   * 
   * @return translated "Add a todo"
   */
  @DefaultStringValue("Add a todo")
  @Key("reminderAdd")
  String reminderAdd();

  /**
   * Translated "The new todo has been correctly created.".
   * 
   * @return translated "The new todo has been correctly created."
   */
  @DefaultStringValue("The new todo has been correctly created.")
  @Key("reminderAddConfirm")
  String reminderAddConfirm();

  /**
   * Translated "Fill the properties of this todo.".
   * 
   * @return translated "Fill the properties of this todo."
   */
  @DefaultStringValue("Fill the properties of this todo.")
  @Key("reminderAddDetails")
  String reminderAddDetails();

  /**
   * Translated "An error occurred during the creation of the todo.".
   * 
   * @return translated "An error occurred during the creation of the todo."
   */
  @DefaultStringValue("An error occurred during the creation of the todo.")
  @Key("reminderAddErrorDetails")
  String reminderAddErrorDetails();

  /**
   * Translated "The todo reminder has been correctly deleted.".
   * 
   * @return translated "The todo reminder has been correctly deleted."
   */
  @DefaultStringValue("The todo reminder has been correctly deleted.")
  @Key("reminderDeletionConfirm")
  String reminderDeletionConfirm();

  /**
   * Translated "An error occurred while deleting the todo reminder.".
   * 
   * @return translated "An error occurred while deleting the todo reminder."
   */
  @DefaultStringValue("An error occurred while deleting the todo reminder.")
  @Key("reminderDeletionErrorDetails")
  String reminderDeletionErrorDetails();

  /**
   * Translated "Todo".
   * 
   * @return translated "Todo"
   */
  @DefaultStringValue("Todo")
  @Key("reminderPoint")
  String reminderPoint();

  /**
   * Translated "Todo".
   * 
   * @return translated "Todo"
   */
  @DefaultStringValue("Todo")
  @Key("reminderPoints")
  String reminderPoints();

  /**
   * Translated "Update a todo.".
   * 
   * @return translated "Update a todo."
   */
  @DefaultStringValue("Update a todo.")
  @Key("reminderUpdate")
  String reminderUpdate();

  /**
   * Translated "The todo reminder has been correctly modified.".
   * 
   * @return translated "The todo reminder has been correctly modified."
   */
  @DefaultStringValue("The todo reminder has been correctly modified.")
  @Key("reminderUpdateConfirm")
  String reminderUpdateConfirm();

  /**
   * Translated "Update the properties of this todo.".
   * 
   * @return translated "Update the properties of this todo."
   */
  @DefaultStringValue("Update the properties of this todo.")
  @Key("reminderUpdateDetails")
  String reminderUpdateDetails();

  /**
   * Translated "Modification error.".
   * 
   * @return translated "Modification error."
   */
  @DefaultStringValue("Modification error.")
  @Key("reminderUpdateError")
  String reminderUpdateError();

  /**
   * Translated "An error occurred during the modification of the todo reminder.".
   * 
   * @return translated "An error occurred during the modification of the todo reminder."
   */
  @DefaultStringValue("An error occurred during the modification of the todo reminder.")
  @Key("reminderUpdateErrorDetails")
  String reminderUpdateErrorDetails();

  /**
   * Translated "Reminders".
   * 
   * @return translated "Reminders"
   */
  @DefaultStringValue("Reminders")
  @Key("reminders")
  String reminders();

  /**
   * Translated "Remove".
   * 
   * @return translated "Remove"
   */
  @DefaultStringValue("Remove")
  @Key("remove")
  String remove();

  /**
   * Translated "Remove".
   * 
   * @return translated "Remove"
   */
  @DefaultStringValue("Remove")
  @Key("removeItem")
  String removeItem();

  /**
   * Translated "Report".
   * 
   * @return translated "Report"
   */
  @DefaultStringValue("Report")
  @Key("report")
  String report();

  /**
   * Translated "Add Image".
   * 
   * @return translated "Add Image"
   */
  @DefaultStringValue("Add Image")
  @Key("reportAddImageDialogTitle")
  String reportAddImageDialogTitle();

  /**
   * Translated "This report is already opened.".
   * 
   * @return translated "This report is already opened."
   */
  @DefaultStringValue("This report is already opened.")
  @Key("reportAlreadyOpened")
  String reportAlreadyOpened();

  /**
   * Translated "Error, the report have not been created.".
   * 
   * @return translated "Error, the report have not been created."
   */
  @DefaultStringValue("Error, the report have not been created.")
  @Key("reportCreateError")
  String reportCreateError();

  /**
   * Translated "New Report".
   * 
   * @return translated "New Report"
   */
  @DefaultStringValue("New Report")
  @Key("reportCreateReport")
  String reportCreateReport();

  /**
   * Translated "The report has been created successfully.".
   * 
   * @return translated "The report has been created successfully."
   */
  @DefaultStringValue("The report has been created successfully.")
  @Key("reportCreateSuccess")
  String reportCreateSuccess();

  /**
   * Translated "Report Definitions".
   * 
   * @return translated "Report Definitions"
   */
  @DefaultStringValue("Report Definitions")
  @Key("reportDefinitions")
  String reportDefinitions();

  /**
   * Translated "The draft has been successfully deleted.".
   * 
   * @return translated "The draft has been successfully deleted."
   */
  @DefaultStringValue("The draft has been successfully deleted.")
  @Key("reportEditCancelSuccess")
  String reportEditCancelSuccess();

  /**
   * Translated "Error, you cannot edit this report.".
   * 
   * @return translated "Error, you cannot edit this report."
   */
  @DefaultStringValue("Error, you cannot edit this report.")
  @Key("reportEditError")
  String reportEditError();

  /**
   * Translated "Edited By".
   * 
   * @return translated "Edited By"
   */
  @DefaultStringValue("Edited By")
  @Key("reportEditor")
  String reportEditor();

  /**
   * Translated "Select a model...".
   * 
   * @return translated "Select a model..."
   */
  @DefaultStringValue("Select a model...")
  @Key("reportEmptyChoice")
  String reportEmptyChoice();

  /**
   * Translated "This section is empty.".
   * 
   * @return translated "This section is empty."
   */
  @DefaultStringValue("This section is empty.")
  @Key("reportEmptySection")
  String reportEmptySection();

  /**
   * Translated "Frequency".
   * 
   * @return translated "Frequency"
   */
  @DefaultStringValue("Frequency")
  @Key("reportFrequency")
  String reportFrequency();

  /**
   * Translated "Full Mode".
   * 
   * @return translated "Full Mode"
   */
  @DefaultStringValue("Full Mode")
  @Key("reportFullMode")
  String reportFullMode();

  /**
   * Translated "Image URL".
   * 
   * @return translated "Image URL"
   */
  @DefaultStringValue("Image URL")
  @Key("reportImageURL")
  String reportImageURL();

  /**
   * Translated "Date".
   * 
   * @return translated "Date"
   */
  @DefaultStringValue("Date")
  @Key("reportLastEditDate")
  String reportLastEditDate();

  /**
   * Translated "Template".
   * 
   * @return translated "Template"
   */
  @DefaultStringValue("Template")
  @Key("reportModel")
  String reportModel();

  /**
   * Translated "Title".
   * 
   * @return translated "Title"
   */
  @DefaultStringValue("Title")
  @Key("reportName")
  String reportName();

  /**
   * Translated "A report is already attached to this element.".
   * 
   * @return translated "A report is already attached to this element."
   */
  @DefaultStringValue("A report is already attached to this element.")
  @Key("reportNoCreate")
  String reportNoCreate();

  /**
   * Translated "Once".
   * 
   * @return translated "Once"
   */
  @DefaultStringValue("Once")
  @Key("reportOnce")
  String reportOnce();

  /**
   * Translated "Open Report".
   * 
   * @return translated "Open Report"
   */
  @DefaultStringValue("Open Report")
  @Key("reportOpenReport")
  String reportOpenReport();

  /**
   * Translated "Overview Mode".
   * 
   * @return translated "Overview Mode"
   */
  @DefaultStringValue("Overview Mode")
  @Key("reportOverviewMode")
  String reportOverviewMode();

  /**
   * Translated "Report parameters".
   * 
   * @return translated "Report parameters"
   */
  @DefaultStringValue("Report parameters")
  @Key("reportParameters")
  String reportParameters();

  /**
   * Translated "Phase".
   * 
   * @return translated "Phase"
   */
  @DefaultStringValue("Phase")
  @Key("reportPhase")
  String reportPhase();

  /**
   * Translated "Error, your modifications could not be saved.".
   * 
   * @return translated "Error, your modifications could not be saved."
   */
  @DefaultStringValue("Error, your modifications could not be saved.")
  @Key("reportSaveError")
  String reportSaveError();

  /**
   * Translated "Modifications have been saved.".
   * 
   * @return translated "Modifications have been saved."
   */
  @DefaultStringValue("Modifications have been saved.")
  @Key("reportSaveSuccess")
  String reportSaveSuccess();

  /**
   * Translated "Type".
   * 
   * @return translated "Type"
   */
  @DefaultStringValue("Type")
  @Key("reportType")
  String reportType();

  /**
   * Translated "Reporting Frequency".
   * 
   * @return translated "Reporting Frequency"
   */
  @DefaultStringValue("Reporting Frequency")
  @Key("reportingFrequency")
  String reportingFrequency();

  /**
   * Translated "Reports".
   * 
   * @return translated "Reports"
   */
  @DefaultStringValue("Reports")
  @Key("reports")
  String reports();

  /**
   * Translated "Retry".
   * 
   * @return translated "Retry"
   */
  @DefaultStringValue("Retry")
  @Key("retry")
  String retry();

  /**
   * Translated "Retrying...".
   * 
   * @return translated "Retrying..."
   */
  @DefaultStringValue("Retrying...")
  @Key("retrying")
  String retrying();

  /**
   * Translated "Return to the grid".
   * 
   * @return translated "Return to the grid"
   */
  @DefaultStringValue("Return to the grid")
  @Key("returnToGrid")
  String returnToGrid();

  /**
   * Translated "Rows".
   * 
   * @return translated "Rows"
   */
  @DefaultStringValue("Rows")
  @Key("rows")
  String rows();

  /**
   * Translated "Save".
   * 
   * @return translated "Save"
   */
  @DefaultStringValue("Save")
  @Key("save")
  String save();

  /**
   * Translated "Your data has been correctly saved.".
   * 
   * @return translated "Your data has been correctly saved."
   */
  @DefaultStringValue("Your data has been correctly saved.")
  @Key("saveConfirm")
  String saveConfirm();

  /**
   * Translated "An error occurred during your data saving.".
   * 
   * @return translated "An error occurred during your data saving."
   */
  @DefaultStringValue("An error occurred during your data saving.")
  @Key("saveError")
  String saveError();

  /**
   * Translated "Save configuration".
   * 
   * @return translated "Save configuration"
   */
  @DefaultStringValue("Save configuration")
  @Key("saveExportConfiguration")
  String saveExportConfiguration();

  /**
   * Translated "Saved".
   * 
   * @return translated "Saved"
   */
  @DefaultStringValue("Saved")
  @Key("saved")
  String saved();

  /**
   * Translated "M.d.yyyy, h a".
   * 
   * @return translated "M.d.yyyy, h a"
   */
  @DefaultStringValue("M.d.yyyy, h a")
  @Key("savedDateExportFormat")
  String savedDateExportFormat();

  /**
   * Translated "Saving changes...".
   * 
   * @return translated "Saving changes..."
   */
  @DefaultStringValue("Saving changes...")
  @Key("saving")
  String saving();

  /**
   * Translated "Schema".
   * 
   * @return translated "Schema"
   */
  @DefaultStringValue("Schema")
  @Key("schema")
  String schema();

  /**
   * Translated "Search".
   * 
   * @return translated "Search"
   */
  @DefaultStringValue("Search")
  @Key("search")
  String search();

  /**
   * Translated "2. Select export backup to export".
   * 
   * @return translated "2. Select export backup to export"
   */
  @DefaultStringValue("2. Select export backup to export")
  @Key("selectBackupToExport")
  String selectBackupToExport();

  /**
   * Translated "Select a page from the tabs above, or browse all of the available features below.".
   * 
   * @return translated "Select a page from the tabs above, or browse all of the available features below."
   */
  @DefaultStringValue("Select a page from the tabs above, or browse all of the available features below.")
  @Key("selectCategory")
  String selectCategory();

  /**
   * Translated "Please select at least one category element !".
   * 
   * @return translated "Please select at least one category element !"
   */
  @DefaultStringValue("Please select at least one category element !")
  @Key("selectCategoryElementToDelete")
  String selectCategoryElementToDelete();

  /**
   * Translated "Please select at least one category !".
   * 
   * @return translated "Please select at least one category !"
   */
  @DefaultStringValue("Please select at least one category !")
  @Key("selectCategoryToDelete")
  String selectCategoryToDelete();

  /**
   * Translated "Select the symbol".
   * 
   * @return translated "Select the symbol"
   */
  @DefaultStringValue("Select the symbol")
  @Key("selectTheSymbol")
  String selectTheSymbol();

  /**
   * Translated "Choice".
   * 
   * @return translated "Choice"
   */
  @DefaultStringValue("Choice")
  @Key("selectType")
  String selectType();

  /**
   * Translated "Share".
   * 
   * @return translated "Share"
   */
  @DefaultStringValue("Share")
  @Key("sendReportDraft")
  String sendReportDraft();

  /**
   * Translated "Load failed due to error on the server.".
   * 
   * @return translated "Load failed due to error on the server."
   */
  @DefaultStringValue("Load failed due to error on the server.")
  @Key("serverError")
  String serverError();

  /**
   * Translated "Settings".
   * 
   * @return translated "Settings"
   */
  @DefaultStringValue("Settings")
  @Key("settings")
  String settings();

  /**
   * Translated "Setup".
   * 
   * @return translated "Setup"
   */
  @DefaultStringValue("Setup")
  @Key("setup")
  String setup();

  /**
   * Translated "A4 Sheet (Landscape)".
   * 
   * @return translated "A4 Sheet (Landscape)"
   */
  @DefaultStringValue("A4 Sheet (Landscape)")
  @Key("sheetA4Landscape")
  String sheetA4Landscape();

  /**
   * Translated "A4 Sheet (Portrait)".
   * 
   * @return translated "A4 Sheet (Portrait)"
   */
  @DefaultStringValue("A4 Sheet (Portrait)")
  @Key("sheetA4Portrait")
  String sheetA4Portrait();

  /**
   * Translated "Show in the grid".
   * 
   * @return translated "Show in the grid"
   */
  @DefaultStringValue("Show in the grid")
  @Key("showInGrid")
  String showInGrid();

  /**
   * Translated "Site/Month".
   * 
   * @return translated "Site/Month"
   */
  @DefaultStringValue("Site/Month")
  @Key("sideAndMonth")
  String sideAndMonth();

  /**
   * Translated "Contributors".
   * 
   * @return translated "Contributors"
   */
  @DefaultStringValue("Contributors")
  @Key("sigmah.contributors")
  String sigmah_contributors();

  /**
   * Translated "Financial partners".
   * 
   * @return translated "Financial partners"
   */
  @DefaultStringValue("Financial partners")
  @Key("sigmah.credits")
  String sigmah_credits();

  /**
   * Translated "Developers".
   * 
   * @return translated "Developers"
   */
  @DefaultStringValue("Developers")
  @Key("sigmah.developers")
  String sigmah_developers();

  /**
   * Translated "Managers".
   * 
   * @return translated "Managers"
   */
  @DefaultStringValue("Managers")
  @Key("sigmah.managers")
  String sigmah_managers();

  /**
   * Translated "Technical partners".
   * 
   * @return translated "Technical partners"
   */
  @DefaultStringValue("Technical partners")
  @Key("sigmah.partners")
  String sigmah_partners();

  /**
   * Translated "design".
   * 
   * @return translated "design"
   */
  @DefaultStringValue("design")
  @Key("sigmah.partners.role.design")
  String sigmah_partners_role_design();

  /**
   * Translated "development".
   * 
   * @return translated "development"
   */
  @DefaultStringValue("development")
  @Key("sigmah.partners.role.development")
  String sigmah_partners_role_development();

  /**
   * Translated "graphic".
   * 
   * @return translated "graphic"
   */
  @DefaultStringValue("graphic")
  @Key("sigmah.partners.role.graphic")
  String sigmah_partners_role_graphic();

  /**
   * Translated "Access denied".
   * 
   * @return translated "Access denied"
   */
  @DefaultStringValue("Access denied")
  @Key("sigmahOfflineDenied")
  String sigmahOfflineDenied();

  /**
   * Translated "Sigmah needs your permission to store your datas on this computer.".
   * 
   * @return translated "Sigmah needs your permission to store your datas on this computer."
   */
  @DefaultStringValue("Sigmah needs your permission to store your datas on this computer.")
  @Key("sigmahOfflineDescription")
  String sigmahOfflineDescription();

  /**
   * Translated "Offline Mode".
   * 
   * @return translated "Offline Mode"
   */
  @DefaultStringValue("Offline Mode")
  @Key("sigmahOfflineOfflineMode")
  String sigmahOfflineOfflineMode();

  /**
   * Translated "Online Mode".
   * 
   * @return translated "Online Mode"
   */
  @DefaultStringValue("Online Mode")
  @Key("sigmahOfflineOnlineMode")
  String sigmahOfflineOnlineMode();

  /**
   * Translated "Command unavailable".
   * 
   * @return translated "Command unavailable"
   */
  @DefaultStringValue("Command unavailable")
  @Key("sigmahOfflineUnavailable")
  String sigmahOfflineUnavailable();

  /**
   * Translated "Single selection".
   * 
   * @return translated "Single selection"
   */
  @DefaultStringValue("Single selection")
  @Key("singleChoice")
  String singleChoice();

  /**
   * Translated "Site".
   * 
   * @return translated "Site"
   */
  @DefaultStringValue("Site")
  @Key("site")
  String site();

  /**
   * Translated "Count".
   * 
   * @return translated "Count"
   */
  @DefaultStringValue("Count")
  @Key("siteCount")
  String siteCount();

  /**
   * Translated "site(s) are missing geographic coordinates".
   * 
   * @return translated "site(s) are missing geographic coordinates"
   */
  @DefaultStringValue("site(s) are missing geographic coordinates")
  @Key("siteLackCoordiantes")
  String siteLackCoordiantes();

  /**
   * Translated "Lists of Activities".
   * 
   * @return translated "Lists of Activities"
   */
  @DefaultStringValue("Lists of Activities")
  @Key("siteLists")
  String siteLists();

  /**
   * Translated "Browse the lists of activity sites with an Excel-like interface. Sort, filter, search and map.".
   * 
   * @return translated "Browse the lists of activity sites with an Excel-like interface. Sort, filter, search and map."
   */
  @DefaultStringValue("Browse the lists of activity sites with an Excel-like interface. Sort, filter, search and map.")
  @Key("siteListsDescriptions")
  String siteListsDescriptions();

  /**
   * Translated "Sites".
   * 
   * @return translated "Sites"
   */
  @DefaultStringValue("Sites")
  @Key("sites")
  String sites();

  /**
   * Translated "include last 6 months".
   * 
   * @return translated "include last 6 months"
   */
  @DefaultStringValue("include last 6 months")
  @Key("sixMonthsFilter")
  String sixMonthsFilter();

  /**
   * Translated "PowerPoint Slide".
   * 
   * @return translated "PowerPoint Slide"
   */
  @DefaultStringValue("PowerPoint Slide")
  @Key("slidePowerPoint")
  String slidePowerPoint();

  /**
   * Translated "OK, software is loaded, retrieving programming definitions...".
   * 
   * @return translated "OK, software is loaded, retrieving programming definitions..."
   */
  @DefaultStringValue("OK, software is loaded, retrieving programming definitions...")
  @Key("softwareLoaded")
  String softwareLoaded();

  /**
   * Translated "Sort Order".
   * 
   * @return translated "Sort Order"
   */
  @DefaultStringValue("Sort Order")
  @Key("sortOrder")
  String sortOrder();

  /**
   * Translated "Source of Verification".
   * 
   * @return translated "Source of Verification"
   */
  @DefaultStringValue("Source of Verification")
  @Key("sourceOfVerification")
  String sourceOfVerification();

  /**
   * Translated "S".
   * 
   * @return translated "S"
   */
  @DefaultStringValue("S")
  @Key("southHemiChars")
  String southHemiChars();

  /**
   * Translated "1. Specify period to search for a backup".
   * 
   * @return translated "1. Specify period to search for a backup"
   */
  @DefaultStringValue("1. Specify period to search for a backup")
  @Key("specifyPeriodForBackup")
  String specifyPeriodForBackup();

  /**
   * Translated "Start Date".
   * 
   * @return translated "Start Date"
   */
  @DefaultStringValue("Start Date")
  @Key("startDate")
  String startDate();

  /**
   * Translated "Status".
   * 
   * @return translated "Status"
   */
  @DefaultStringValue("Status")
  @Key("status")
  String status();

  /**
   * Translated "Offline mode status".
   * 
   * @return translated "Offline mode status"
   */
  @DefaultStringValue("Offline mode status")
  @Key("statusOfflineMode")
  String statusOfflineMode();

  /**
   * Translated "Email Subscription".
   * 
   * @return translated "Email Subscription"
   */
  @DefaultStringValue("Email Subscription")
  @Key("subscribed")
  String subscribed();

  /**
   * Translated "Sum".
   * 
   * @return translated "Sum"
   */
  @DefaultStringValue("Sum")
  @Key("sum")
  String sum();

  /**
   * Translated "Switch to Offline Mode".
   * 
   * @return translated "Switch to Offline Mode"
   */
  @DefaultStringValue("Switch to Offline Mode")
  @Key("switchToOffline")
  String switchToOffline();

  /**
   * Translated "Switch to Online Mode".
   * 
   * @return translated "Switch to Online Mode"
   */
  @DefaultStringValue("Switch to Online Mode")
  @Key("switchToOnline")
  String switchToOnline();

  /**
   * Translated "Synchronize Now".
   * 
   * @return translated "Synchronize Now"
   */
  @DefaultStringValue("Synchronize Now")
  @Key("syncNow")
  String syncNow();

  /**
   * Translated "Downloading Sigmah...".
   * 
   * @return translated "Downloading Sigmah..."
   */
  @DefaultStringValue("Downloading Sigmah...")
  @Key("synchronizerApplicationDownload.0")
  String synchronizerApplicationDownload_0();

  /**
   * Translated "An error occured while downloading Sigmah:".
   * 
   * @return translated "An error occured while downloading Sigmah:"
   */
  @DefaultStringValue("An error occured while downloading Sigmah:")
  @Key("synchronizerApplicationDownload.0.failed")
  String synchronizerApplicationDownload_0_failed();

  /**
   * Translated "Cleaning downloaded Sigmah content...".
   * 
   * @return translated "Cleaning downloaded Sigmah content..."
   */
  @DefaultStringValue("Cleaning downloaded Sigmah content...")
  @Key("synchronizerApplicationUpload.0")
  String synchronizerApplicationUpload_0();

  /**
   * Translated "An error occured while cleaning the cached content:".
   * 
   * @return translated "An error occured while cleaning the cached content:"
   */
  @DefaultStringValue("An error occured while cleaning the cached content:")
  @Key("synchronizerApplicationUpload.0.failed")
  String synchronizerApplicationUpload_0_failed();

  /**
   * Translated "Persisting your session...".
   * 
   * @return translated "Persisting your session..."
   */
  @DefaultStringValue("Persisting your session...")
  @Key("synchronizerAuthTokenDownload.0")
  String synchronizerAuthTokenDownload_0();

  /**
   * Translated "An error occured while persisting your session:".
   * 
   * @return translated "An error occured while persisting your session:"
   */
  @DefaultStringValue("An error occured while persisting your session:")
  @Key("synchronizerAuthTokenDownload.0.failed")
  String synchronizerAuthTokenDownload_0_failed();

  /**
   * Translated "Cleaning your session...".
   * 
   * @return translated "Cleaning your session..."
   */
  @DefaultStringValue("Cleaning your session...")
  @Key("synchronizerAuthTokenUpload.0")
  String synchronizerAuthTokenUpload_0();

  /**
   * Translated "An error occured while cleaning your session:".
   * 
   * @return translated "An error occured while cleaning your session:"
   */
  @DefaultStringValue("An error occured while cleaning your session:")
  @Key("synchronizerAuthTokenUpload.0.failed")
  String synchronizerAuthTokenUpload_0_failed();

  /**
   * Translated "Checking the connection...".
   * 
   * @return translated "Checking the connection..."
   */
  @DefaultStringValue("Checking the connection...")
  @Key("synchronizerAvailabilityUpload.0")
  String synchronizerAvailabilityUpload_0();

  /**
   * Translated "Server unreachable. Please check your internet connection.".
   * 
   * @return translated "Server unreachable. Please check your internet connection."
   */
  @DefaultStringValue("Server unreachable. Please check your internet connection.")
  @Key("synchronizerAvailabilityUpload.0.failed")
  String synchronizerAvailabilityUpload_0_failed();

  /**
   * Translated "Sync error".
   * 
   * @return translated "Sync error"
   */
  @DefaultStringValue("Sync error")
  @Key("synchronizerError")
  String synchronizerError();

  /**
   * Translated "Operation complete.".
   * 
   * @return translated "Operation complete."
   */
  @DefaultStringValue("Operation complete.")
  @Key("synchronizerFinished")
  String synchronizerFinished();

  /**
   * Translated "Finalizing...".
   * 
   * @return translated "Finalizing..."
   */
  @DefaultStringValue("Finalizing...")
  @Key("synchronizerFinishing")
  String synchronizerFinishing();

  /**
   * Translated "Preparing the caching of informations about your organization...".
   * 
   * @return translated "Preparing the caching of informations about your organization..."
   */
  @DefaultStringValue("Preparing the caching of informations about your organization...")
  @Key("synchronizerOrganizationDownload.0")
  String synchronizerOrganizationDownload_0();

  /**
   * Translated "An error occured while caching informations about your organization:".
   * 
   * @return translated "An error occured while caching informations about your organization:"
   */
  @DefaultStringValue("An error occured while caching informations about your organization:")
  @Key("synchronizerOrganizationDownload.0.failed")
  String synchronizerOrganizationDownload_0_failed();

  /**
   * Translated "Creating databases...".
   * 
   * @return translated "Creating databases..."
   */
  @DefaultStringValue("Creating databases...")
  @Key("synchronizerOrganizationDownload.1")
  String synchronizerOrganizationDownload_1();

  /**
   * Translated "Downloading informations...".
   * 
   * @return translated "Downloading informations..."
   */
  @DefaultStringValue("Downloading informations...")
  @Key("synchronizerOrganizationDownload.2")
  String synchronizerOrganizationDownload_2();

  /**
   * Translated "Downloading the logo of your organization...".
   * 
   * @return translated "Downloading the logo of your organization..."
   */
  @DefaultStringValue("Downloading the logo of your organization...")
  @Key("synchronizerOrganizationDownload.3")
  String synchronizerOrganizationDownload_3();

  /**
   * Translated "Cleaning the cached data about your organization...".
   * 
   * @return translated "Cleaning the cached data about your organization..."
   */
  @DefaultStringValue("Cleaning the cached data about your organization...")
  @Key("synchronizerOrganizationUpload.0")
  String synchronizerOrganizationUpload_0();

  /**
   * Translated "An error occured while cleaning your organization data:".
   * 
   * @return translated "An error occured while cleaning your organization data:"
   */
  @DefaultStringValue("An error occured while cleaning your organization data:")
  @Key("synchronizerOrganizationUpload.0.failed")
  String synchronizerOrganizationUpload_0_failed();

  /**
   * Translated "Please wait...".
   * 
   * @return translated "Please wait..."
   */
  @DefaultStringValue("Please wait...")
  @Key("synchronizerTitle")
  String synchronizerTitle();

  /**
   * Translated "Synchronizing...".
   * 
   * @return translated "Synchronizing..."
   */
  @DefaultStringValue("Synchronizing...")
  @Key("synchronizing")
  String synchronizing();

  /**
   * Translated "Tableaux".
   * 
   * @return translated "Tableaux"
   */
  @DefaultStringValue("Tableaux")
  @Key("tableaux")
  String tableaux();

  /**
   * Translated "Tables".
   * 
   * @return translated "Tables"
   */
  @DefaultStringValue("Tables")
  @Key("tables")
  String tables();

  /**
   * Translated "Target value".
   * 
   * @return translated "Target value"
   */
  @DefaultStringValue("Target value")
  @Key("targetValue")
  String targetValue();

  /**
   * Translated "Tasks".
   * 
   * @return translated "Tasks"
   */
  @DefaultStringValue("Tasks")
  @Key("tasks")
  String tasks();

  /**
   * Translated "Time".
   * 
   * @return translated "Time"
   */
  @DefaultStringValue("Time")
  @Key("time")
  String time();

  /**
   * Translated "Time Period".
   * 
   * @return translated "Time Period"
   */
  @DefaultStringValue("Time Period")
  @Key("timePeriod")
  String timePeriod();

  /**
   * Translated "Sorry, your session has expired. Please login again.".
   * 
   * @return translated "Sorry, your session has expired. Please login again."
   */
  @DefaultStringValue("Sorry, your session has expired. Please login again.")
  @Key("timeout")
  String timeout();

  /**
   * Translated "Title".
   * 
   * @return translated "Title"
   */
  @DefaultStringValue("Title")
  @Key("title")
  String title();

  /**
   * Translated "to".
   * 
   * @return translated "to"
   */
  @DefaultStringValue("to")
  @Key("toDate")
  String toDate();

  /**
   * Translated "Today".
   * 
   * @return translated "Today"
   */
  @DefaultStringValue("Today")
  @Key("today")
  String today();

  /**
   * Translated "Coordinates may have up to 3 numbers".
   * 
   * @return translated "Coordinates may have up to 3 numbers"
   */
  @DefaultStringValue("Coordinates may have up to 3 numbers")
  @Key("tooManyNumbers")
  String tooManyNumbers();

  /**
   * Translated "Totals".
   * 
   * @return translated "Totals"
   */
  @DefaultStringValue("Totals")
  @Key("totals")
  String totals();

  /**
   * Translated "Try to connect".
   * 
   * @return translated "Try to connect"
   */
  @DefaultStringValue("Try to connect")
  @Key("tryToConnect")
  String tryToConnect();

  /**
   * Translated "include last 12 months".
   * 
   * @return translated "include last 12 months"
   */
  @DefaultStringValue("include last 12 months")
  @Key("twelveMonthsFilter")
  String twelveMonthsFilter();

  /**
   * Translated "Type".
   * 
   * @return translated "Type"
   */
  @DefaultStringValue("Type")
  @Key("type")
  String type();

  /**
   * Translated "Ungrouped".
   * 
   * @return translated "Ungrouped"
   */
  @DefaultStringValue("Ungrouped")
  @Key("unGrouped")
  String unGrouped();

  /**
   * Translated "#Error: This '\"unavailable\"' model is not being used. You can only shift it to status '\"Ready\"' !".
   * 
   * @return translated "#Error: This '\"unavailable\"' model is not being used. You can only shift it to status '\"Ready\"' !"
   */
  @DefaultStringValue("#Error: This '\"unavailable\"' model is not being used. You can only shift it to status '\"Ready\"' !")
  @Key("unavailableNotUsedModelStatusChangeError")
  String unavailableNotUsedModelStatusChangeError();

  /**
   * Translated "#Error: This '\"unavailable\"' model is being used . You can only shift it to status '\"Used\"' !".
   * 
   * @return translated "#Error: This '\"unavailable\"' model is being used . You can only shift it to status '\"Used\"' !"
   */
  @DefaultStringValue("#Error: This '\"unavailable\"' model is being used . You can only shift it to status '\"Used\"' !")
  @Key("unavailableUsedModelStatusChangeError")
  String unavailableUsedModelStatusChangeError();

  /**
   * Translated "Uninstall offline mode".
   * 
   * @return translated "Uninstall offline mode"
   */
  @DefaultStringValue("Uninstall offline mode")
  @Key("uninstallOfflineMode")
  String uninstallOfflineMode();

  /**
   * Translated "Units".
   * 
   * @return translated "Units"
   */
  @DefaultStringValue("Units")
  @Key("units")
  String units();

  /**
   * Translated "You have unsaved changes".
   * 
   * @return translated "You have unsaved changes"
   */
  @DefaultStringValue("You have unsaved changes")
  @Key("unsavedChanges")
  String unsavedChanges();

  /**
   * Translated "You have unsaved changes. If you leave this page or close your browser, these changes will be lost.".
   * 
   * @return translated "You have unsaved changes. If you leave this page or close your browser, these changes will be lost."
   */
  @DefaultStringValue("You have unsaved changes. If you leave this page or close your browser, these changes will be lost.")
  @Key("unsavedChangesWarning")
  String unsavedChangesWarning();

  /**
   * Translated "Your data is not saved. Do you really want to exit ?".
   * 
   * @return translated "Your data is not saved. Do you really want to exit ?"
   */
  @DefaultStringValue("Your data is not saved. Do you really want to exit ?")
  @Key("unsavedDataMessage")
  String unsavedDataMessage();

  /**
   * Translated "Unsaved data".
   * 
   * @return translated "Unsaved data"
   */
  @DefaultStringValue("Unsaved data")
  @Key("unsavedDataTitle")
  String unsavedDataTitle();

  /**
   * Translated "Update Report".
   * 
   * @return translated "Update Report"
   */
  @DefaultStringValue("Update Report")
  @Key("updateReport")
  String updateReport();

  /**
   * Translated "#Error: A '\"Used\"' model can be only shifted to status '\"unavailable\"' !".
   * 
   * @return translated "#Error: A '\"Used\"' model can be only shifted to status '\"unavailable\"' !"
   */
  @DefaultStringValue("#Error: A '\"Used\"' model can be only shifted to status '\"unavailable\"' !")
  @Key("usedModelStatusChangeError")
  String usedModelStatusChangeError();

  /**
   * Translated "User Information".
   * 
   * @return translated "User Information"
   */
  @DefaultStringValue("User Information")
  @Key("userInformation")
  String userInformation();

  /**
   * Translated "Add users or control their access level".
   * 
   * @return translated "Add users or control their access level"
   */
  @DefaultStringValue("Add users or control their access level")
  @Key("userManagerDescription")
  String userManagerDescription();

  /**
   * Translated "Users".
   * 
   * @return translated "Users"
   */
  @DefaultStringValue("Users")
  @Key("users")
  String users();

  /**
   * Translated "Value".
   * 
   * @return translated "Value"
   */
  @DefaultStringValue("Value")
  @Key("value")
  String value();

  /**
   * Translated "Verifying and deleting...".
   * 
   * @return translated "Verifying and deleting..."
   */
  @DefaultStringValue("Verifying and deleting...")
  @Key("verfyingAndDeleting")
  String verfyingAndDeleting();

  /**
   * Translated "Select version of data to export".
   * 
   * @return translated "Select version of data to export"
   */
  @DefaultStringValue("Select version of data to export")
  @Key("versionOfDataToExport")
  String versionOfDataToExport();

  /**
   * Translated "Vertical Position Conflict".
   * 
   * @return translated "Vertical Position Conflict"
   */
  @DefaultStringValue("Vertical Position Conflict")
  @Key("verticalPositionConflict")
  String verticalPositionConflict();

  /**
   * Translated "View".
   * 
   * @return translated "View"
   */
  @DefaultStringValue("View")
  @Key("view")
  String view();

  /**
   * Translated "Visit to a login page".
   * 
   * @return translated "Visit to a login page"
   */
  @DefaultStringValue("Visit to a login page")
  @Key("visitToLoginPage")
  String visitToLoginPage();

  /**
   * Translated "Week".
   * 
   * @return translated "Week"
   */
  @DefaultStringValue("Week")
  @Key("week")
  String week();

  /**
   * Translated "Weekly".
   * 
   * @return translated "Weekly"
   */
  @DefaultStringValue("Weekly")
  @Key("weekly")
  String weekly();

  /**
   * Translated "Welcome".
   * 
   * @return translated "Welcome"
   */
  @DefaultStringValue("Welcome")
  @Key("welcome")
  String welcome();

  /**
   * Translated "Welcome to ActivityInfo".
   * 
   * @return translated "Welcome to ActivityInfo"
   */
  @DefaultStringValue("Welcome to ActivityInfo")
  @Key("welcomeMessage")
  String welcomeMessage();

  /**
   * Translated "W".
   * 
   * @return translated "W"
   */
  @DefaultStringValue("W")
  @Key("westHemiChars")
  String westHemiChars();

  /**
   * Translated "Word".
   * 
   * @return translated "Word"
   */
  @DefaultStringValue("Word")
  @Key("word")
  String word();

  /**
   * Translated "Year".
   * 
   * @return translated "Year"
   */
  @DefaultStringValue("Year")
  @Key("year")
  String year();

  /**
   * Translated "Yes".
   * 
   * @return translated "Yes"
   */
  @DefaultStringValue("Yes")
  @Key("yes")
  String yes();

  /**
   * Translated "You".
   * 
   * @return translated "You"
   */
  @DefaultStringValue("You")
  @Key("you")
  String you();

  /**
   * Translated "Your email".
   * 
   * @return translated "Your email"
   */
  @DefaultStringValue("Your email")
  @Key("yourEmail")
  String yourEmail();

  /**
   * Translated "Your name".
   * 
   * @return translated "Your name"
   */
  @DefaultStringValue("Your name")
  @Key("yourName")
  String yourName();
}
