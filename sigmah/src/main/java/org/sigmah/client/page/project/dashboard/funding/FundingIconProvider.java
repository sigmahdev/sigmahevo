package org.sigmah.client.page.project.dashboard.funding;

import org.sigmah.shared.domain.ProjectModelType;

import com.google.gwt.user.client.ui.AbstractImagePrototype;

/**
 * Utility class to get icons for the different project type.
 * 
 * @author tmi
 * 
 */
public final class FundingIconProvider {
	   
	public static AbstractImagePrototype [] sigmah3 = new AbstractImagePrototype[] {
	    FundingIconImageBundle.ICONS.fundingSmall(),
	    FundingIconImageBundle.ICONS.fundingMediumTransparent(),
	    FundingIconImageBundle.ICONS.fundingMedium(),
		FundingIconImageBundle.ICONS.fundingLarge(),
		FundingIconImageBundle.ICONS.localPartnerSmall(),
		FundingIconImageBundle.ICONS.localPartnerMediumTransparent(),
		FundingIconImageBundle.ICONS.localPartnerMedium(),
		FundingIconImageBundle.ICONS.localPartnerLarge(),
		FundingIconImageBundle.ICONS.ngoSmall(),
		FundingIconImageBundle.ICONS.ngoMediumTransparent(),
		FundingIconImageBundle.ICONS.ngoMedium(),
		FundingIconImageBundle.ICONS.ngoLarge(),
		};
    /**
     * Provides only static methods.
     */
    private FundingIconProvider() {

    }

    /**
     * Defines the available size for these icons.
     * 
     * @author tmi
     * 
     */
    public static enum IconSize {
    	SMALL(0), SMALL_MEIDUM(1), MEDIUM(2), LARGE(3);
    	private final int currentCode;
    	
    	IconSize(int i){
    		this.currentCode = i;
    	}
    	public int getIconValue(){
    		return this.currentCode;
    	}
        }
    

    /**
     * Gets the small icon for the given type.
     * 
     * @param type
     *            The type.
     * @return the small icon.
     */
    public static AbstractImagePrototype getProjectTypeIcon(ProjectModelType type) {
//    	 IconSize.SMALL.getIconValue();
        return getProjectTypeIcon(type, IconSize.SMALL);
    }

    /**
     * Gets the icon for the given type at the given size.
     * 
     * @param type
     *            The type.
     * @param size
     *            The size.
     * @return The icon.
     */
    public static AbstractImagePrototype getProjectTypeIcon(ProjectModelType type, IconSize size) {

        if (type == null) {
            throw new IllegalArgumentException("The type must not be null.");
        }

        switch (type) {
        case FUNDING:
//        	size.getIconValue();
        
        	return sigmah3[size.getIconValue()];
        	
//            switch (size) {
//            case MEDIUM:
//                return FundingIconImageBundle.ICONS.fundingMedium();
//            case LARGE:
//                return FundingIconImageBundle.ICONS.fundingLarge();
//            case SMALL_MEIDUM:
//                return FundingIconImageBundle.ICONS.fundingMediumTransparent();
//            default:
//                return FundingIconImageBundle.ICONS.fundingSmall();
//            }
        case LOCAL_PARTNER:
        	return sigmah3[4+size.getIconValue()];
//            switch (size) {
//            case MEDIUM:
//                return FundingIconImageBundle.ICONS.localPartnerMedium();
//            case LARGE:
//                return FundingIconImageBundle.ICONS.localPartnerLarge();
//            case SMALL_MEIDUM:
//                return FundingIconImageBundle.ICONS.localPartnerMediumTransparent();
//            default:
//                return FundingIconImageBundle.ICONS.localPartnerSmall();
//            }
        case NGO:
        	return sigmah3[8+size.getIconValue()];
//            switch (size) {
//            case MEDIUM:
//                return FundingIconImageBundle.ICONS.ngoMedium();
//            case LARGE:
//                return FundingIconImageBundle.ICONS.ngoLarge();
//            case SMALL_MEIDUM:
//                return FundingIconImageBundle.ICONS.ngoMediumTransparent();
//            default:
//                return FundingIconImageBundle.ICONS.ngoSmall();
//            }
        default:
            return FundingIconImageBundle.ICONS.ngoSmall();
        }
    }
}
